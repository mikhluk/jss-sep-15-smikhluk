<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 25.03.2016
  Time: 0:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>09 Weather</title>
</head>
<body>


    <jsp:useBean id ="client" class="jss.w09_test.WeatherBean"/>
    <jsp:setProperty name="client" property="zipCode" value="90001"/>
    <jsp:getProperty name="client" property="zipCode"/>
    <h1>
        City:
        ${client.weather.city} <br>
        Temperature:
        ${client.weatherTemperature}

    </h1>
    <jsp:getProperty name="client" property="weatherTemperature"/>

</body>
</html>
