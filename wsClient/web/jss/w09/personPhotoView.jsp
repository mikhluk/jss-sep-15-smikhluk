<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 23.01.2016
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Person photo view 09</title>
</head>
<body>
<a href='/jss-wsClient/openPhoto'>back to photo View</a>
<br>

<c:set var="photoId" value='${sessionScope.photoId}' scope="page"/>
<c:set var="photo" value='${sessionScope.photo}' scope="page"/>
<c:set var="personDAO" value='${sessionScope.personDAO}' scope="page"/>
<c:set var="filter" value='${sessionScope.filter}' scope="page"/>
<c:set var="emptyFilter" value='${sessionScope.emtyFilter}' scope="page"/>

<table border=1 cellspacing=0 bordercolor="#CCCCCC" width=500>
<tr>
<th width="30">photo id</th>
<th>path</th>
<th>views</th>
<th>region</th>
</tr>

<tr>
<td>${photo.getId()}</td>
<td>${photo.getPath()}</td>
<td>${photo.getViews()}</td>
<td>${photo.getRegion().getName()}</td>
</tr>
</table>

<br>

<table border="1" cellspacing="0" width="300">
<tr>
<th width="30">person_id</th>
<th>name</th>
<th width="30">delete</th>
</tr>

<c:forEach var="person" items="${personDAO.getAsList(filter)}">
<tr>
<td>${person.getId()}</td>
<td> <font color="red">   ${person.getName()} </font></td>
<td><a href="/jss-wsClient/photoDBAction?action=deletePerson&photo_id=${photoId}&person_id=${person.getId()}"> X</a></td>
</tr>
</c:forEach>
</table>
<p/>

<h3> Add person on photo id = ${photoId}</h3>
<form action="/jss-wsClient/photoDBAction" method="post" accept-charset="UTF-8">
<table border=1 cellspacing=0 bordercolor="#CCCCCC" width=300px>
<tr>
<th>name</th>
<tr>
<td><input type="text" name="updateName" value="Arnold"></td>
</tr>
</table>
<p/>
<input type="hidden" name="action" value="addPerson">
<input type="hidden" name="photo_id" value="${photoId}">
<input type="hidden" name="pfid" value="-1">
<input type="submit" value="Add person">
<input type="reset" value="Cancel">
</form>
<br>
<img src='/jss-wsClient/photoCard?photo_id=${photoId}'>
<br>

<h3>All existing person:</h3>
<table border="1" cellspacing="0" width="300">
<tr>
<th width="30">person_id</th>
<th>name</th>
</tr>

<c:forEach var="person" items="${personDAO.getAsList(emptyFilter)}">
<tr>
<td >${person.getId()}</td>
<td > ${person.getName()} </td>
</tr>
</c:forEach>
</table>
<br>
</body>
</html>
