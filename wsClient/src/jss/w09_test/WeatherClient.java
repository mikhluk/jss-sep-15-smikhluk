package jss.w09_test;

import jss.w09_test.weather.*;

import java.util.List;

/**
 * @author Sergey Mikhluk.
 */
public class WeatherClient {
    public static void main(String[] args) {

        WeatherSoap port = new Weather().getWeatherSoap();
        List<WeatherDescription>  wInfo = port.getWeatherInformation().getWeatherDescription();

        //WeatherReturn result = port.getCityWeatherByZIP("90001");
        WeatherReturn result = port.getCityWeatherByZIP("10010");

        int weatherID = result.getWeatherID();
        result.getCity();

        WeatherDescription description = null;
        for(WeatherDescription item : wInfo) {
            if (item.getWeatherID() == weatherID){
                description = item;
            }
        }

        System.out.println(result.getCity());
        System.out.println(description.getDescription() + " : " + description.getPictureURL());
        System.out.println("Temperature: " + result.getTemperature());
        System.out.println("Pressure: " + result.getPressure());



//
//        {
//            System.out.println("Invoking getCityForecastByZIP...");
//            java.lang.String _getCityForecastByZIP_zip = "";
//            jss.w09_test.weather.ForecastReturn _getCityForecastByZIP__return = port.getCityForecastByZIP(_getCityForecastByZIP_zip);
//            System.out.println("getCityForecastByZIP.result=" + _getCityForecastByZIP__return);
//
//
//        }
    }
}