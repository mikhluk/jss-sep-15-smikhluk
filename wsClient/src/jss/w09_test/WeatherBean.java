package jss.w09_test;

import jss.w09_test.weather.Weather;
import jss.w09_test.weather.WeatherDescription;
import jss.w09_test.weather.WeatherReturn;
import jss.w09_test.weather.WeatherSoap;

import java.util.List;

/**
 * @author Sergey Mikhluk.
 */
public class WeatherBean {
    WeatherSoap port = null;
    List<WeatherDescription> wInfo;
    String zipCode = "";

    public WeatherBean() {
        port = new Weather().getWeatherSoap();
        wInfo = port.getWeatherInformation().getWeatherDescription();
    }

    public List<WeatherDescription> getwInfo() {
        return wInfo;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public WeatherReturn getWeather() {
        return port.getCityWeatherByZIP(zipCode);

    }

    public String getWeatherTemperature() {
        return port.getCityWeatherByZIP(zipCode).getTemperature();

    }


}
