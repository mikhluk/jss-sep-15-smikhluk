package jss.w09_test;

import jss.w09_test.validator.CardValidator;
import jss.w09_test.validator.CreditCard;
import jss.w09_test.validator.ICardValidator;

/**
 * @author Sergey Mikhluk.
 */
public class CVClient {
  public static void main(String[] argv) {

      CreditCard card = new CreditCard();
      ICardValidator validator = new CardValidator().getValidatorPort();
      card.setNumber("432435656441");

      boolean result = validator.validateCard(card);
      System.out.println("Card validation result = " + result);

  }
}
