package jss.w09.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Sergey Mikhluk.
 */
@WebServlet(name = "OpenPersonPhotoView", urlPatterns = {"/openPersonPhoto"})
public class OpenPersonPhotoView extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        Filter filter = new Filter();
        Filter emptyFilter = new Filter();

        IPhotoDAO photoDAO = new PhotoDAO().getPhotoDAOPort();
        IPersonDAO personDAO = new PersonDAO().getPersonDAOPort();

        String photoId = request.getParameter("photo_id");

        //MS:TODO нормально реализовал фильтр? или можно было как-то проще?
        // CK: Проще передать Map не получится, нужен класс-обертка

        Filter.FilterMap filterMap = new Filter.FilterMap();
        filterMap = addFilter("photo_id", photoId, filterMap);
        filterMap = addFilter("person_name", request.getParameter("person_name"), filterMap);
        filter.setFilterMap(filterMap);

        Photo photo = null;
        if (photoId != null && !photoId.equals("")) {
            if (photoDAO != null) {
                photo = photoDAO.getById(Long.valueOf(photoId));
            }
        }

        request.getSession().setAttribute("photoId", photoId);
        request.getSession().setAttribute("photo", photo);
        request.getSession().setAttribute("personDAO", personDAO);
        request.getSession().setAttribute("filter", filter);
        request.getSession().setAttribute("emptyFilter", emptyFilter);

        request.getRequestDispatcher("/jss/w09/personPhotoView.jsp").forward(request, response);
    }

    private Filter.FilterMap addFilter(String key, String value, Filter.FilterMap filterMap) {
        Filter.FilterMap.Entry entry = new Filter.FilterMap.Entry();
        entry.setKey(key);
        entry.setValue(value == null ? "" : value);
        filterMap.getEntry().add(entry);
        return filterMap;
    }

}