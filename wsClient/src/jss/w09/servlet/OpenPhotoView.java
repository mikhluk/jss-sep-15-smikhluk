package jss.w09.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Sergey Mikhluk.
 */
@WebServlet(name = "OpenPhotoView", urlPatterns = {"/openPhoto"})
public class OpenPhotoView extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        Filter filter = new Filter();
        IPhotoDAO photoDAO = new PhotoDAO().getPhotoDAOPort();

        //MS:TODO нормально реализовал фильтр? или можно было как-то проще?
        Filter.FilterMap filterMap = new Filter.FilterMap();
        filterMap = addFilter("photo_path", request.getParameter("filterPath"), filterMap);
        filterMap = addFilter("photo_region_name", request.getParameter("filterRegion"), filterMap);
        filterMap = addFilter("person_id", request.getParameter("person_id"), filterMap);
        filter.setFilterMap(filterMap);

        request.getSession().setAttribute("photoDAO", photoDAO);
        request.getSession().setAttribute("filter", filter);
        request.getRequestDispatcher("/jss/w09/photoView.jsp").forward(request, response);
    }

    private Filter.FilterMap addFilter(String key, String value, Filter.FilterMap filterMap) {
        Filter.FilterMap.Entry entry = new Filter.FilterMap.Entry();
        entry.setKey(key);
        entry.setValue(value == null ? "" : value);
        filterMap.getEntry().add(entry);
        return filterMap;
    }
}
