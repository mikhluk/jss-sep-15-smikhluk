package jss.w09.servlet;

import jss.w09.personDao.IPersonDAO;
import jss.w09.personDao.Person;
import jss.w09.personDao.PersonDAO;
import jss.w09.photoDao.IPhotoDAO;
import jss.w09.photoDao.Photo;
import jss.w09.photoDao.PhotoDAO;
import jss.w09.photoDao.Region;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Sergey Mikhluk.
 */

@WebServlet(name = "photoDBAction", urlPatterns = {"/photoDBAction"})
@MultipartConfig(location = "d:/temp", maxFileSize = 16 * 1024 * 1024, fileSizeThreshold = 0)
public class DBActionServlet extends HttpServlet {
    private InitialContext initialContext = null;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String action = request.getParameter("action");
        String actionStatus;

        String photoId = request.getParameter("photo_id");
        if (photoId == null || photoId.equals("")) {
            actionStatus = "Wrong parameter photo_id!";

            String refRedirect = String.format("jss/w09/error.jsp?&actionStatus=%s", actionStatus);
            response.sendRedirect(refRedirect);
            return;
        }
        Long id = new Long(photoId);

        actionStatus = "Wrong parameter action!";
        String refRedirect = String.format("jss/w09/error.jsp?&actionStatus=%s", actionStatus);
        if (action == null) {
            response.sendRedirect(refRedirect);
            return;
        }

        switch (action) {
            case "addPerson":
                actionStatus = actAddPerson(request, actionStatus);
                refRedirect = String.format("/jss-wsClient/openPersonPhoto?photo_id=%d&actionStatus=%s", id, actionStatus);
                break;

            case "deletePerson":
                Long personId = new Long(request.getParameter("person_id"));
                actionStatus = actDeletePerson(actionStatus, id, personId);
                refRedirect = String.format("/jss-wsClient/openPersonPhoto?photo_id=%d&actionStatus=%s", id, actionStatus);
                break;

            case "delete":
                actionStatus = actDelete(actionStatus, id);
                refRedirect = String.format("/jss-wsClient/openPhoto?photo_id=%d&actionStatus=%s", id, actionStatus);
                break;
            case "update":
                actionStatus = actUpdate(request, actionStatus);
                refRedirect = String.format("/jss-wsClient/openPhoto?photo_id=%d&actionStatus=%s", id, actionStatus);
                break;
            case "addPhoto":
                id = actAddPhoto(request);
                if (id > 0) {
                    actionStatus = "added";
                }
                refRedirect = String.format("/jss-wsClient/openPhoto?photo_id=%d&actionStatus=%s", id, actionStatus);
                break;
        }

        response.sendRedirect(refRedirect);
    }

    @Override
    public void init() throws ServletException {
        try {
            initialContext = new InitialContext();
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    private XMLGregorianCalendar getXmlGregorianCalendar(HttpServletRequest request) {
        Date date = null;
        GregorianCalendar gregorianCalendar;
        XMLGregorianCalendar xmlGregorianCalendar = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("updateDate"));
            gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.setTime(date);
            xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);

        } catch (ParseException | DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        return xmlGregorianCalendar;
    }

    private IPhotoDAO getPhotoDAORef() {
        return new PhotoDAO().getPhotoDAOPort();
    }

    private byte[] getImageDataFromPart(HttpServletRequest request) throws IOException, ServletException {
        Part filePart = request.getPart("file");
        InputStream inputStream = filePart.getInputStream();
        byte[] buf = new byte[1024*4];
        ByteArrayOutputStream bos = new ByteArrayOutputStream(1024*1024);
        while (inputStream.read(buf) > 0) {
            bos.write(buf);
        }
        return bos.toByteArray();
    }

    //MS:TODO нормально реализован апдейт фото? в том числе числе установка даты
    //Сейчас дата выглядит как 2016-01-01T00:00:00+02:00 , хотя раньше выглядела как 2016-01-01

    // Достаточно было бы и просто в виде строки передать
    // Но такой сапособ лучше, потому что в xml дата преобразуется в тип xsd:date, маппинг на который в сервисе еще можно дополнительно настроить
    // по умолчанию - да будут передаваться все части даты-времени

    private String actUpdate(HttpServletRequest request, String actionStatus) throws IOException, ServletException {
        Photo photo = new Photo();

        photo.setId(Long.valueOf(request.getParameter("photo_id")));
        photo.setPath(request.getParameter("updatePath"));
        photo.setDate(getXmlGregorianCalendar(request));
        photo.setViews(Integer.valueOf(request.getParameter("updateViews")));
        photo.setImageData(getImageDataFromPart(request));

        Region region = new Region();
        region.setId(-1);
        region.setName(request.getParameter("updateRegionName"));
        photo.setRegion(region);

        IPhotoDAO photoDAO = getPhotoDAORef();
        boolean success = photoDAO.update(photo);
        if (success) {
            actionStatus = "updated";
        }
        return actionStatus;
    }

    private String actDelete(String actionStatus, Long id) throws RemoteException{
        IPhotoDAO photoDAO = getPhotoDAORef();

        boolean success = photoDAO.deleteById(id);
        if (success) {
            actionStatus = "deleted";
        }
        return actionStatus;
    }

    private Long actAddPhoto(HttpServletRequest request) throws ServletException, IOException {
        //System.out.println("DBActionServlet.java case addPhoto"); //debug
        Photo photo = new Photo();

        photo.setId(Long.valueOf(request.getParameter("photo_id")));
        photo.setPath(request.getParameter("updatePath"));
        photo.setDate(getXmlGregorianCalendar(request));
        photo.setViews(Integer.valueOf(request.getParameter("updateViews")));
        photo.setImageData(getImageDataFromPart(request));

        Region region = new Region();
        region.setId(-1);
        region.setName(request.getParameter("updateRegionName"));
        photo.setRegion(region);

        IPhotoDAO photoDAO = getPhotoDAORef();
        return photoDAO.add(photo);
    }

    private String actAddPerson(HttpServletRequest request, String actionStatus) {
        //System.out.println("DBActionServlet.java case addPerson"); // debug
        IPersonDAO personDAO = new PersonDAO().getPersonDAOPort();

        Person person = new Person();
        person.setId(-1);  //MS:TODO нормально сетить тут -1 или лучше было засетить в конктрукторе без параметров

        // CK : В принципе могут быть разые стратегии назначения id, только галвное чтобы везде подход был одинаковый
        // Лучше всего наверное следовать стратегии, которой мы при работе с hibernate session пользовались
        // То есть клиенский код id вообще не назначает, т.е. его нет в конструкторе и сеттер setId() клиентом не используется

        person.setName(request.getParameter("updateName"));
        Long id = personDAO.addPersonOnPhoto(Long.valueOf(request.getParameter("photo_id")), person);

        if (id > 0) {
            actionStatus = "added";
        }
        return actionStatus;
    }

    private String actDeletePerson(String actionStatus, Long photoId, Long personId) {
        //System.out.println("DBActionServlet.java case deletePerson");  //debug
        boolean success;

        IPersonDAO personDAO = new PersonDAO().getPersonDAOPort();
        success = personDAO.deletePersonFromPhoto(photoId, personId);

        if (success) {
            actionStatus = "deleted";
        }
        return actionStatus;
    }
}
