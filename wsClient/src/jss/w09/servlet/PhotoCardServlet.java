package jss.w09.servlet;


import jss.w09.photoDao.IPhotoDAO;
import jss.w09.photoDao.Photo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Sergey Mikhluk.
 */

@WebServlet(name = "PhotoCardServlet", urlPatterns = {"/photoCard"})
public class PhotoCardServlet extends HttpServlet {
    public static final int BUF_SIZE = 1024;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("image/jpeg");
        long id = Long.parseLong(request.getParameter("photo_id"));

        OutputStream binaryOut = response.getOutputStream();
        IPhotoDAO photoDAO = (IPhotoDAO) request.getSession().getAttribute("photoDAO");

        if (photoDAO != null) {
            Photo photo = photoDAO.getById(id);
            InputStream inputStream = new ByteArrayInputStream(photo.getImageData());
            byte[] buf = new byte[BUF_SIZE];

            while (inputStream.read(buf) > 0) {
                binaryOut.write(buf);
            }

        } else {
            // error image
        }
    }
}
