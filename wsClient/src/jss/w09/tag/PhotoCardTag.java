package jss.w09.tag;


import jss.w09.photoDao.Filter;
import jss.w09.photoDao.Photo;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * @author Sergey Mikhluk.
 */
public class PhotoCardTag extends SimpleTagSupport {
    Long id = 0L;
    Photo photo;
    Filter filter;

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();

        out.print("<tr>");
        out.print("<td>" + photo.getId() + " </td>");
        out.print("<td align='center'> <img src='/jss-wsClient/photoCard?photo_id=" + photo.getId() +"' height=40>");
        out.print("<td> <a href='/jss-wsClient/openPersonPhoto?photo_id=" + photo.getId() + "'>" + photo.getPath() + "</a></td>");
        out.print("<td>" + photo.getRegion().getName() + " </td>");
        out.print("<td>" + photo.getDate() + " </td>");
        out.print("<td>" + photo.getViews() + " </td>");
        out.print("<td> <a href='/jss-wsClient/photoDBAction?action=delete&photo_id=" + photo.getId() + "'> X</a></td>");
        out.print("<td> <a href='?action=update&photo_id=" + photo.getId() + "'> Update</a></td>");

        out.print("</td>");
        out.print("</tr>");
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }
}


