package jss.w09.photoDao;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.1.5
 * 2016-04-15T17:29:26.634+03:00
 * Generated source version: 3.1.5
 * 
 */
@WebServiceClient(name = "PhotoDAO", 
                  wsdlLocation = "file:/D:/javajoy/jss-sep-15-smikhluk/wsClient/src/jss/w09/photoDao/photoDAO.wsdl",
                  targetNamespace = "http://dao.w09.homework.jss.javajoy.net/") 
public class PhotoDAO extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://dao.w09.homework.jss.javajoy.net/", "PhotoDAO");
    public final static QName PhotoDAOPort = new QName("http://dao.w09.homework.jss.javajoy.net/", "PhotoDAOPort");
    static {
        URL url = null;
        try {
            url = new URL("file:/D:/javajoy/jss-sep-15-smikhluk/wsClient/src/jss/w09/photoDao/photoDAO.wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(PhotoDAO.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "file:/D:/javajoy/jss-sep-15-smikhluk/wsClient/src/jss/w09/photoDao/photoDAO.wsdl");
        }
        WSDL_LOCATION = url;
    }

    public PhotoDAO(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public PhotoDAO(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public PhotoDAO() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    public PhotoDAO(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public PhotoDAO(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public PhotoDAO(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }    




    /**
     *
     * @return
     *     returns IPhotoDAO
     */
    @WebEndpoint(name = "PhotoDAOPort")
    public IPhotoDAO getPhotoDAOPort() {
        return super.getPort(PhotoDAOPort, IPhotoDAO.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns IPhotoDAO
     */
    @WebEndpoint(name = "PhotoDAOPort")
    public IPhotoDAO getPhotoDAOPort(WebServiceFeature... features) {
        return super.getPort(PhotoDAOPort, IPhotoDAO.class, features);
    }

}
