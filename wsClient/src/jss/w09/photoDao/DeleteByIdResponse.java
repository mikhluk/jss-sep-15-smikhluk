
package jss.w09.photoDao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteByIdResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteByIdResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deleteByIdSuccess" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteByIdResponse", propOrder = {
    "deleteByIdSuccess"
})
public class DeleteByIdResponse {

    protected boolean deleteByIdSuccess;

    /**
     * Gets the value of the deleteByIdSuccess property.
     * 
     */
    public boolean isDeleteByIdSuccess() {
        return deleteByIdSuccess;
    }

    /**
     * Sets the value of the deleteByIdSuccess property.
     * 
     */
    public void setDeleteByIdSuccess(boolean value) {
        this.deleteByIdSuccess = value;
    }

}
