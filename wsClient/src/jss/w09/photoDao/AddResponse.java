
package jss.w09.photoDao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="addedPhotoId" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addResponse", propOrder = {
    "addedPhotoId"
})
public class AddResponse {

    protected long addedPhotoId;

    /**
     * Gets the value of the addedPhotoId property.
     * 
     */
    public long getAddedPhotoId() {
        return addedPhotoId;
    }

    /**
     * Sets the value of the addedPhotoId property.
     * 
     */
    public void setAddedPhotoId(long value) {
        this.addedPhotoId = value;
    }

}
