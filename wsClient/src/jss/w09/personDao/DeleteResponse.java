
package jss.w09.personDao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deleteSuccess" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteResponse", propOrder = {
    "deleteSuccess"
})
public class DeleteResponse {

    protected boolean deleteSuccess;

    /**
     * Gets the value of the deleteSuccess property.
     * 
     */
    public boolean isDeleteSuccess() {
        return deleteSuccess;
    }

    /**
     * Sets the value of the deleteSuccess property.
     * 
     */
    public void setDeleteSuccess(boolean value) {
        this.deleteSuccess = value;
    }

}
