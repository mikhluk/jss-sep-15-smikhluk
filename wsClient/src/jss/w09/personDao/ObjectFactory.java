
package jss.w09.personDao;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the jss.w09.personDao package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Add_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "add");
    private final static QName _AddPersonOnPhoto_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "addPersonOnPhoto");
    private final static QName _AddPersonOnPhotoResponse_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "addPersonOnPhotoResponse");
    private final static QName _AddResponse_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "addResponse");
    private final static QName _Delete_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "delete");
    private final static QName _DeleteById_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "deleteById");
    private final static QName _DeleteByIdResponse_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "deleteByIdResponse");
    private final static QName _DeletePersonFromPhoto_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "deletePersonFromPhoto");
    private final static QName _DeletePersonFromPhotoResponse_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "deletePersonFromPhotoResponse");
    private final static QName _DeleteResponse_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "deleteResponse");
    private final static QName _Filter_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "filter");
    private final static QName _GetAsList_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "getAsList");
    private final static QName _GetAsListResponse_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "getAsListResponse");
    private final static QName _GetById_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "getById");
    private final static QName _GetByIdResponse_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "getByIdResponse");
    private final static QName _Person_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "person");
    private final static QName _Update_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "update");
    private final static QName _UpdateResponse_QNAME = new QName("http://dao.w09.homework.jss.javajoy.net/", "updateResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: jss.w09.personDao
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Filter }
     * 
     */
    public Filter createFilter() {
        return new Filter();
    }

    /**
     * Create an instance of {@link Filter.FilterMap }
     * 
     */
    public Filter.FilterMap createFilterFilterMap() {
        return new Filter.FilterMap();
    }

    /**
     * Create an instance of {@link Add }
     * 
     */
    public Add createAdd() {
        return new Add();
    }

    /**
     * Create an instance of {@link AddPersonOnPhoto }
     * 
     */
    public AddPersonOnPhoto createAddPersonOnPhoto() {
        return new AddPersonOnPhoto();
    }

    /**
     * Create an instance of {@link AddPersonOnPhotoResponse }
     * 
     */
    public AddPersonOnPhotoResponse createAddPersonOnPhotoResponse() {
        return new AddPersonOnPhotoResponse();
    }

    /**
     * Create an instance of {@link AddResponse }
     * 
     */
    public AddResponse createAddResponse() {
        return new AddResponse();
    }

    /**
     * Create an instance of {@link Delete }
     * 
     */
    public Delete createDelete() {
        return new Delete();
    }

    /**
     * Create an instance of {@link DeleteById }
     * 
     */
    public DeleteById createDeleteById() {
        return new DeleteById();
    }

    /**
     * Create an instance of {@link DeleteByIdResponse }
     * 
     */
    public DeleteByIdResponse createDeleteByIdResponse() {
        return new DeleteByIdResponse();
    }

    /**
     * Create an instance of {@link DeletePersonFromPhoto }
     * 
     */
    public DeletePersonFromPhoto createDeletePersonFromPhoto() {
        return new DeletePersonFromPhoto();
    }

    /**
     * Create an instance of {@link DeletePersonFromPhotoResponse }
     * 
     */
    public DeletePersonFromPhotoResponse createDeletePersonFromPhotoResponse() {
        return new DeletePersonFromPhotoResponse();
    }

    /**
     * Create an instance of {@link DeleteResponse }
     * 
     */
    public DeleteResponse createDeleteResponse() {
        return new DeleteResponse();
    }

    /**
     * Create an instance of {@link GetAsList }
     * 
     */
    public GetAsList createGetAsList() {
        return new GetAsList();
    }

    /**
     * Create an instance of {@link GetAsListResponse }
     * 
     */
    public GetAsListResponse createGetAsListResponse() {
        return new GetAsListResponse();
    }

    /**
     * Create an instance of {@link GetById }
     * 
     */
    public GetById createGetById() {
        return new GetById();
    }

    /**
     * Create an instance of {@link GetByIdResponse }
     * 
     */
    public GetByIdResponse createGetByIdResponse() {
        return new GetByIdResponse();
    }

    /**
     * Create an instance of {@link Person }
     * 
     */
    public Person createPerson() {
        return new Person();
    }

    /**
     * Create an instance of {@link Update }
     * 
     */
    public Update createUpdate() {
        return new Update();
    }

    /**
     * Create an instance of {@link UpdateResponse }
     * 
     */
    public UpdateResponse createUpdateResponse() {
        return new UpdateResponse();
    }

    /**
     * Create an instance of {@link Filter.FilterMap.Entry }
     * 
     */
    public Filter.FilterMap.Entry createFilterFilterMapEntry() {
        return new Filter.FilterMap.Entry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Add }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "add")
    public JAXBElement<Add> createAdd(Add value) {
        return new JAXBElement<Add>(_Add_QNAME, Add.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPersonOnPhoto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "addPersonOnPhoto")
    public JAXBElement<AddPersonOnPhoto> createAddPersonOnPhoto(AddPersonOnPhoto value) {
        return new JAXBElement<AddPersonOnPhoto>(_AddPersonOnPhoto_QNAME, AddPersonOnPhoto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPersonOnPhotoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "addPersonOnPhotoResponse")
    public JAXBElement<AddPersonOnPhotoResponse> createAddPersonOnPhotoResponse(AddPersonOnPhotoResponse value) {
        return new JAXBElement<AddPersonOnPhotoResponse>(_AddPersonOnPhotoResponse_QNAME, AddPersonOnPhotoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "addResponse")
    public JAXBElement<AddResponse> createAddResponse(AddResponse value) {
        return new JAXBElement<AddResponse>(_AddResponse_QNAME, AddResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Delete }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "delete")
    public JAXBElement<Delete> createDelete(Delete value) {
        return new JAXBElement<Delete>(_Delete_QNAME, Delete.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "deleteById")
    public JAXBElement<DeleteById> createDeleteById(DeleteById value) {
        return new JAXBElement<DeleteById>(_DeleteById_QNAME, DeleteById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "deleteByIdResponse")
    public JAXBElement<DeleteByIdResponse> createDeleteByIdResponse(DeleteByIdResponse value) {
        return new JAXBElement<DeleteByIdResponse>(_DeleteByIdResponse_QNAME, DeleteByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePersonFromPhoto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "deletePersonFromPhoto")
    public JAXBElement<DeletePersonFromPhoto> createDeletePersonFromPhoto(DeletePersonFromPhoto value) {
        return new JAXBElement<DeletePersonFromPhoto>(_DeletePersonFromPhoto_QNAME, DeletePersonFromPhoto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePersonFromPhotoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "deletePersonFromPhotoResponse")
    public JAXBElement<DeletePersonFromPhotoResponse> createDeletePersonFromPhotoResponse(DeletePersonFromPhotoResponse value) {
        return new JAXBElement<DeletePersonFromPhotoResponse>(_DeletePersonFromPhotoResponse_QNAME, DeletePersonFromPhotoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "deleteResponse")
    public JAXBElement<DeleteResponse> createDeleteResponse(DeleteResponse value) {
        return new JAXBElement<DeleteResponse>(_DeleteResponse_QNAME, DeleteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Filter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "filter")
    public JAXBElement<Filter> createFilter(Filter value) {
        return new JAXBElement<Filter>(_Filter_QNAME, Filter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAsList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "getAsList")
    public JAXBElement<GetAsList> createGetAsList(GetAsList value) {
        return new JAXBElement<GetAsList>(_GetAsList_QNAME, GetAsList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAsListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "getAsListResponse")
    public JAXBElement<GetAsListResponse> createGetAsListResponse(GetAsListResponse value) {
        return new JAXBElement<GetAsListResponse>(_GetAsListResponse_QNAME, GetAsListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "getById")
    public JAXBElement<GetById> createGetById(GetById value) {
        return new JAXBElement<GetById>(_GetById_QNAME, GetById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "getByIdResponse")
    public JAXBElement<GetByIdResponse> createGetByIdResponse(GetByIdResponse value) {
        return new JAXBElement<GetByIdResponse>(_GetByIdResponse_QNAME, GetByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Person }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "person")
    public JAXBElement<Person> createPerson(Person value) {
        return new JAXBElement<Person>(_Person_QNAME, Person.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Update }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "update")
    public JAXBElement<Update> createUpdate(Update value) {
        return new JAXBElement<Update>(_Update_QNAME, Update.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dao.w09.homework.jss.javajoy.net/", name = "updateResponse")
    public JAXBElement<UpdateResponse> createUpdateResponse(UpdateResponse value) {
        return new JAXBElement<UpdateResponse>(_UpdateResponse_QNAME, UpdateResponse.class, null, value);
    }

}
