
package jss.w09.personDao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteByIdResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteByIdResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deletebByIdSuccess" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteByIdResponse", propOrder = {
    "deletebByIdSuccess"
})
public class DeleteByIdResponse {

    protected boolean deletebByIdSuccess;

    /**
     * Gets the value of the deletebByIdSuccess property.
     * 
     */
    public boolean isDeletebByIdSuccess() {
        return deletebByIdSuccess;
    }

    /**
     * Sets the value of the deletebByIdSuccess property.
     * 
     */
    public void setDeletebByIdSuccess(boolean value) {
        this.deletebByIdSuccess = value;
    }

}
