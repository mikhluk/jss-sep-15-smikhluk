
package jss.w09.personDao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="updateSuccess" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateResponse", propOrder = {
    "updateSuccess"
})
public class UpdateResponse {

    protected boolean updateSuccess;

    /**
     * Gets the value of the updateSuccess property.
     * 
     */
    public boolean isUpdateSuccess() {
        return updateSuccess;
    }

    /**
     * Sets the value of the updateSuccess property.
     * 
     */
    public void setUpdateSuccess(boolean value) {
        this.updateSuccess = value;
    }

}
