
package jss.w09.personDao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addPersonOnPhotoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addPersonOnPhotoResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="addedPersonOnPhotoId" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addPersonOnPhotoResponse", propOrder = {
    "addedPersonOnPhotoId"
})
public class AddPersonOnPhotoResponse {

    protected long addedPersonOnPhotoId;

    /**
     * Gets the value of the addedPersonOnPhotoId property.
     * 
     */
    public long getAddedPersonOnPhotoId() {
        return addedPersonOnPhotoId;
    }

    /**
     * Sets the value of the addedPersonOnPhotoId property.
     * 
     */
    public void setAddedPersonOnPhotoId(long value) {
        this.addedPersonOnPhotoId = value;
    }

}
