package jss.w08_test.converter.web;

import jss.w08_test.converter.ConverterBean;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * @author Sergey Mikhluk.
 */
@WebServlet(name = "ConverterServlet", urlPatterns = {"/converter"})
public class ConverterServlet extends HttpServlet {
//    @EJB
//    ConverterBean  converter;


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ConverterBean converter = null;
        try {
            Context initialContext = new InitialContext();
            converter = (ConverterBean) initialContext.lookup("java:module/ConverterEJB");
        } catch (NamingException e) {
            e.printStackTrace();
        }

        try {
            response.getWriter().print("ConverterServlet:\n");
            String amount = request.getParameter("amount");
            if (amount == null || amount.isEmpty()) {
                amount = "20";
            }
            BigDecimal yen = converter.dollarToYen(new BigDecimal(amount));
            BigDecimal euro = converter.yenToEuro(yen);
            response.getWriter().format("%s dollars are %s yen or %s euro %n", amount, yen, euro);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
