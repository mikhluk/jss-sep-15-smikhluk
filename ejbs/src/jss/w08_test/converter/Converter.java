package jss.w08_test.converter;

import javax.ejb.EJBObject;
import java.math.BigDecimal;
import java.rmi.RemoteException;

/**
 * @author Sergey Mikhluk.
 */
public interface Converter extends EJBObject {

    BigDecimal dollarToYen(BigDecimal dollars) throws RemoteException;

    BigDecimal yenToEuro(BigDecimal yen) throws RemoteException;

    void setYenRate(BigDecimal yenRate) throws  RemoteException;
    void setEuroRate(BigDecimal euroRate) throws  RemoteException;



}
