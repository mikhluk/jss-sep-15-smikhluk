package jss.w08_test.converter;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;

/**
 * @author Sergey Mikhluk.
 */
public interface ConverterHome extends EJBHome {
    Converter create() throws RemoteException, CreateException;
}
