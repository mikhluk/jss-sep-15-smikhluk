package jss.w08_test.converter;

import javax.ejb.*;
import java.math.BigDecimal;

/**
 * @author Sergey Mikhluk.
 */
@Stateful(name = "ConverterEJB")
public class ConverterBean {
    BigDecimal euroRate = new BigDecimal("0.0077");
    BigDecimal yenRate = new BigDecimal("121.6000");


    public BigDecimal dollarToYen(BigDecimal dollars) {
        BigDecimal result = dollars.multiply(yenRate);
        return result.setScale(2, BigDecimal.ROUND_UP);
    }


    public BigDecimal yenToEuro(BigDecimal yen) {
        BigDecimal result = yen.multiply(euroRate);
        return result.setScale(2, BigDecimal.ROUND_UP);
    }


    public ConverterBean() {
    }

    public void ejbCreate() throws CreateException {

    }


    public void setEuroRate(BigDecimal euroRate) {

    }


    public void setYenRate(BigDecimal yenRate) {

    }

    @Remove
    public void remove(){
    }

}