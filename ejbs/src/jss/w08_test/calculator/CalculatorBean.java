package jss.w08_test.calculator;

import javax.ejb.*;

/**
 * @author Sergey Mikhluk.
 */
@Stateful(name = "CalculatorEJB")
public class CalculatorBean {


    public CalculatorBean() {
    }

    public double calculate(double a, double b){
        return a + b;
    }


    public void setSessionContext(SessionContext sessionContext)  {
    }

    public void ejbRemove()  {
    }

    public void ejbActivate()  {
    }

    public void ejbPassivate()  {
    }

    @Remove
    public void remove(){

    }


    public void ejbCreate() throws CreateException {

    }
}