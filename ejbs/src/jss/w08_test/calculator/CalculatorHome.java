package jss.w08_test.calculator;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;

/**
 * @author Sergey Mikhluk.
 */
public interface CalculatorHome extends EJBHome {
    jss.w08_test.calculator.Calculator create() throws RemoteException, CreateException;
}
