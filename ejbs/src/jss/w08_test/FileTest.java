package jss.w08_test;

import java.io.*;
import java.util.Properties;

/**
 * @author Sergey Mikhluk.
 */
public class FileTest {
    public static final String CONFIG_PATH = "resources\\w08\\db.properties";
    public static void main(String[] args) {


        Properties config = null;

//        config = new Properties();
//
//
//        String filePath = "property.ini";
//        FileOutputStream fileOutputStream = null;
//
//        try {
//            fileOutputStream = new FileOutputStream(filePath);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
////        try {
////            config.store(fileOutputStream, "no comment");
////            new File("property.ini").createNewFile();
////            fileOutputStream.close();
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
//
//
//        try {
//            config.load(new InputStreamReader(new FileInputStream("resources\\w08\\db.properties"), "UTF-8"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        //System.out.println(new File(".").getAbstactFileName);
//        System.out.println("home_photo_db_name: " + config.getProperty("home_photo_db_name"));



        config = new Properties();

        try {
            FileInputStream fileInputStream = new FileInputStream(CONFIG_PATH);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
            config.load(inputStreamReader);
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("dbName: " + config.getProperty("dbName"));  //debug
        System.out.println("dbUser: " + config.getProperty("dbUser"));  //debug
        System.out.println("dbPass: " + config.getProperty("dbPass"));  //debug


    }
}
