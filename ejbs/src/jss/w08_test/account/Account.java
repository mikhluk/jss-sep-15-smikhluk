package jss.w08_test.account;

import javax.ejb.EJBObject;
import java.rmi.RemoteException;

/**
 * @author Sergey Mikhluk.
 */
public interface Account extends EJBObject{
    boolean withdraw(int amount) throws RemoteException;
    void deposit(int amount) throws RemoteException;
    int getBalance() throws RemoteException;
}
