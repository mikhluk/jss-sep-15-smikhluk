package jss.w08_test.account;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;

/**
 * @author Sergey Mikhluk.
 */
public interface AccountHome extends EJBHome {
    jss.w08_test.account.Account create() throws RemoteException, CreateException;

}
