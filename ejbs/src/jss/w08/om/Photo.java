package jss.w08.om;

import java.io.*;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @author Sergey Mikhluk.
 */
public class Photo implements Serializable {
    private long id = -1;
    private String path = "";
    private Date date = null;
    //private long regionID = 0;
    private Region region = null;
    private int views = 0;
    private byte imageData[];

    public void setDataFromInputStream(InputStream imgInputStream) throws IOException {
        if (imgInputStream == null) {
            imageData = null;
            return;
        }

        byte[] buf = new byte[1024 * 4];
        ByteArrayOutputStream bos = new ByteArrayOutputStream(1024 * 1024);
        while (imgInputStream.read(buf) > 0) {
            bos.write(buf);
        }
        imageData = bos.toByteArray();
    }

    public InputStream getDataInputStream() {
        return new ByteArrayInputStream(imageData);
    }

    public Photo(long id, String path, Date date, Region region, int views, InputStream imgInputStream) { //конструктор используется при чтениии базу
        this.id = id;
        this.path = path;
        this.date = date;
        //  this.regionID = regionID;
        this.region = region;
        this.views = views;

        try {
            setDataFromInputStream(imgInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Photo(String tId, String tPath, String tDate, String tRegion, String tViews, byte[] imageData) { //конструктор используется при записи в базу
        try {
            this.id = new Long(tId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        this.path = tPath;

        try {
            java.util.Date date_utl = new SimpleDateFormat("yyyy-MM-dd").parse(tDate);
            Date dat_sql = new Date(date_utl.getTime());
            this.date = dat_sql;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.region = new Region(tRegion);

        try {
            if (tViews == null || tViews.equals("")) {
                this.views = 0;
            } else {
                this.views = Integer.valueOf(tViews);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        this.imageData = imageData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }

    public byte[] getImageData() {
        return imageData;
    }
}
