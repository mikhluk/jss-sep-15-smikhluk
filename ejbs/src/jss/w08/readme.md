Webinar 8 - Home Task
1. Модифицируйте созданное на предыдущих занятиях web-приложение, 
    вынеся всю бизнес-логику в EJB-компоненты.

2 (*). Сделайте ваше приложение распределенным. 
    Для этого поместите EJB-компоненты на одном сервере (например, TomEE), 
    а jsp-страницы на другом (например, Tomcat).


Последнее изменение: Суббота, 12 Декабрь 2015, 18:25