package jss.w08.bean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * @author Sergey Mikhluk.
 */
public class DatabaseConnection {
    // JDBC driver name and database URL
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:3306/";
    private static final String DB_UNICODE = "?useUnicode=true&characterEncoding=utf8";
    // Database credentials
    private static final String USER = "user";
    private static final String PASS = "pass";

    private static DatabaseConnection instance = null;
    private static Connection conn = null;

    private DatabaseConnection(String dbName, String user, String pass) {
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL + dbName + DB_UNICODE, user, pass);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection(String dbName) {

        if (instance == null) {
            instance = new DatabaseConnection(dbName, USER, PASS);
        }
        return instance.conn;
    }

    public static Connection getConnection(Properties conf) {
        if (instance == null) {
            instance = new DatabaseConnection(conf.getProperty("dbName"),
                    conf.getProperty("dbUser"), conf.getProperty("dbPass"));
        }
        return instance.conn;
    }

    public static Connection getConnection(String dbName, String user, String pass) {
        if (instance == null) {
            instance = new DatabaseConnection(dbName, user, pass);
        }
        return instance.conn;
    }
}
