package jss.w08.bean;

import javax.annotation.PostConstruct;
import javax.ejb.CreateException;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.io.Serializable;
import java.util.Properties;

/**
 * @author Sergey Mikhluk.
 */

@Singleton(name = "AppConfigEJB")
@Startup
public class AppConfigBean implements Serializable {
    private Properties config;

    @PostConstruct
    public void init() {
        config = ConfigSingleton.getInstance().getConfig();
    }

    public Properties getConfig() {
        return config;
    }

    public void ejbCreate() throws CreateException {
    }
}