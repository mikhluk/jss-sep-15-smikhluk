package jss.w08.bean;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;

/**
 * @author Sergey Mikhluk.
 */
public interface AppConfigHome extends EJBHome {
    jss.w08.bean.AppConfig create() throws RemoteException, CreateException;
}
