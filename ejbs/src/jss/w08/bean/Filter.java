package jss.w08.bean;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author Sergey Mikhluk.
 */
public class Filter implements Serializable {

    public enum FilterConstant {
        PHOTO_ID("photo_id"),
        PHOTO_PATH("photo_path"),
        PHOTO_REGION_NAME("photo_region_name"),
        PERSON_ID("person_id"),
        PERSON_NAME("person_name");

        private String filterConstant;

        FilterConstant(String fConstant) {
            filterConstant = fConstant;
        }

        @Override
        public String toString() {
            return filterConstant;
        }
    }

    public Filter() {
    }


    private HashMap<String, String> data = new HashMap<>(100);

    public HashMap<String, String> getFilter() {
        return data;
    }

    public String getValue(String key) {
        String value = (data.get(key) == null) ? "" : data.get(key);
        return value;
    }

    public String getValue(FilterConstant key) {
        return getValue(key.toString());
    }

    public String getConstantValue(String key) {
        return FilterConstant.valueOf(key).toString();
    }

    public FilterConstant getConstant(String key) {
        return FilterConstant.valueOf(key);
    }

    public void setValueByKey(String key, String value) {
        data.put(key, value);
    }

    public void setValue(String key, String value) {
        data.put(getConstantValue(key), value);
    }

    public void setValue(FilterConstant key, String value) {
        data.put(key.toString(), value);
    }

}


