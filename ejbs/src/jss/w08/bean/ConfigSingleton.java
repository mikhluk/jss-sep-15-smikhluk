package jss.w08.bean;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

/**
 * @author Sergey Mikhluk.
 */
public class ConfigSingleton implements Serializable{
    public static final String CONFIG_PATH = "db.properties";
    private Properties config = null;
    private static ConfigSingleton instance;

    private ConfigSingleton() {
        try {
            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(CONFIG_PATH);
            config = new Properties();
            config.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //System.out.println("dbName: " + config.getProperty("dbName"));  //debug
    }

    public static ConfigSingleton getInstance() {
        if (instance == null) {
            instance = new ConfigSingleton();
        }
        return instance;
    }

    public Properties getConfig() {
        return config;
    }
}