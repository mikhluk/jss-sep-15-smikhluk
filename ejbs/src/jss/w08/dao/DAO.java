package jss.w08.dao;

import jss.w08.bean.Filter;

import java.rmi.RemoteException;
import java.util.List;

/**
 * @author Sergey Mikhluk.
 */
public interface DAO<T> {
    T getById(long id) throws RemoteException;
    List<T> getAsList(Filter filter) throws RemoteException;

    long add(T element) throws RemoteException;
    boolean update(T element) throws RemoteException;
    boolean delete(long id) throws RemoteException;
    boolean delete(T element) throws RemoteException;
}
