package jss.w08.dao;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;

/**
 * @author Sergey Mikhluk.
 */
public interface PersonDAOHome extends EJBHome {
    PersonDAO create() throws RemoteException, CreateException;
}
