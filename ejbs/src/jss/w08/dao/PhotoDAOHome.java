package jss.w08.dao;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;
import java.io.Serializable;
import java.rmi.RemoteException;

/**
 * @author Sergey Mikhluk.
 */
public interface PhotoDAOHome extends Serializable, EJBHome {
    jss.w08.dao.PhotoDAO create() throws RemoteException, CreateException;
}
