package jss.w08.dao;

import jdk.internal.org.objectweb.asm.Type;
import jss.w08.bean.ConfigSingleton;
import jss.w08.bean.DatabaseConnection;
import jss.w08.bean.Filter;
import jss.w08.om.Person;

import javax.ejb.CreateException;
import javax.ejb.Stateless;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author Sergey Mikhluk.
 */
@Stateless(name = "PersonDAOEJB")

public class PersonDAOBean implements Serializable, DAO<Person> {
    private Connection conn = null;

    public PersonDAOBean() {
        Properties conf = ConfigSingleton.getInstance().getConfig();
        conn = DatabaseConnection.getConnection(conf);
    }

    public List<Person> getAsList(Filter filter) {
        //System.out.println("PersonDAO.java getPersonAsList"); //debug

        if (filter.getValue(Filter.FilterConstant.PHOTO_ID.toString()) == null ||
                filter.getValue(Filter.FilterConstant.PHOTO_ID.toString()).equals("")) {
            return getPersonsAsList(false, filter);
        } else {
            return getPersonsAsList(true, filter);
        }
    }

    public List<Person> getPersonsAsList(boolean photoFilterFlag, Filter filter) {
        int listCapacity = 0;
        String sql;

        if (photoFilterFlag) {
            sql = "SELECT COUNT(*) AS quantity FROM view_person_photo WHERE name LIKE ?  AND photo_id = ?";
        } else {
            sql = "SELECT COUNT(*) AS quantity FROM person WHERE name LIKE ?";
        }

        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            stmt.setString(1, "%" + filter.getValue(Filter.FilterConstant.PERSON_NAME) + "%");

            if (photoFilterFlag) {
                stmt.setString(2, filter.getValue(Filter.FilterConstant.PHOTO_ID));
            }

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                listCapacity = rs.getInt("quantity");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println(listCapacity);
        List<Person> list = new ArrayList(listCapacity);


        if (photoFilterFlag) {
            sql = "SELECT person_id, name FROM view_person_photo WHERE name LIKE ? AND photo_id = ?";
        } else {
            sql = "SELECT * FROM person WHERE name LIKE ?";
        }

        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {

            stmt.setString(1, "%" + filter.getValue(Filter.FilterConstant.PERSON_NAME) + "%");
            if (photoFilterFlag) {
                stmt.setString(2, filter.getValue(Filter.FilterConstant.PHOTO_ID));
            }
            ResultSet rs = stmt.executeQuery();

            String personId = photoFilterFlag ? "person_id" : "id";
            while (rs.next()) {
                Person person = new Person(
                        rs.getLong(personId),
                        rs.getString("name"));
                list.add(person);
            }
            System.out.println(stmt.toString());

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Person getById(long id) {
        String sql = "SELECT * FROM person WHERE id = ?";
        String sqlExecuted;

        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            stmt.setString(1, Long.toString(id));

            ResultSet rs = stmt.executeQuery();
            sqlExecuted = stmt.toString();
            System.out.println(sqlExecuted);

            if (rs.next()) {
                Person person = new Person(
                        rs.getLong("id"),
                        rs.getString("name"));
                return person;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public long add(Person person) {
        return 0;
    }

    public long addPersonOnPhoto(Long photoID, Person person) {
        //System.out.println("PersonPhotoDAO.java addPerson");  debug
        Long id = -1L;

        String sql = "call insertPersonOnPhoto(?,?,?)";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setString(1, person.getName());
            stmt.setString(2, String.valueOf(photoID));
            stmt.registerOutParameter(3, Type.LONG);

            stmt.execute();
            id = stmt.getLong(3);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public boolean update(Person person) {
        return false;
    }

    public boolean deletePersonFromPhoto(long photoId, long personId) {
        boolean success = false;

        String sql = "call deletePerson(?,?,?)";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setLong(1, photoId);
            stmt.setLong(2, personId);
            stmt.registerOutParameter(3, Type.LONG);

            stmt.execute();
            success = stmt.getBoolean(3);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }

    public boolean delete(Person person) {
        return delete(person.getId());
    }

    public boolean delete(long id) {
        return false;
    }

    public void ejbCreate() throws CreateException {
    }

}
