package jss.w08.dao;

import jss.w08.bean.Filter;
import jss.w08.om.Photo;

import javax.ejb.EJBObject;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.List;

/**
 * @author Sergey Mikhluk.
 */
public interface PhotoDAO extends Serializable, EJBObject, DAO<Photo> {
    List<Photo> getPhotosAsList(boolean personFilterFlag, Filter filter) throws RemoteException;

    long add(Photo photo) throws RemoteException;
    boolean update(Photo photo) throws RemoteException;
    boolean delete(Photo photo) throws RemoteException;

    List<Photo> getAsList(Filter filter) throws RemoteException;
}
