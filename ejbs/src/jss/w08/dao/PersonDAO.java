package jss.w08.dao;
import jss.w08.bean.Filter;
import jss.w08.om.Person;

import javax.ejb.EJBObject;

import java.rmi.RemoteException;
import java.util.List;

/**
 * @author Sergey Mikhluk.
 */
public interface PersonDAO extends EJBObject, DAO<Person> {
    List<Person> getPersonsAsList(boolean photoFilterFlag, Filter filter) throws RemoteException;
    long addPersonOnPhoto(Long photoID, Person person)throws RemoteException;
    boolean deletePersonFromPhoto(long photoId, long personId)throws RemoteException;

    //long add(Person person) throws RemoteException;
    boolean update(Person person) throws RemoteException;
    boolean delete(Person person) throws RemoteException;
}
