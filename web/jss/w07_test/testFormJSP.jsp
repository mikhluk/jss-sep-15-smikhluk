<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 12.01.2016
  Time: 17:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Test Form JSP</title>
</head>
<body>

<% request.setCharacterEncoding("UTF-8");
    String strVar = request.getParameter("var");

%>

<form method="post" accept-charset="UTF-8">
    <input type="text" name="var" value="">
    <input type="submit" value="set variable">
    <input type="reset" value="reset">

    <%
    out.print("<br>");
    out.print("hello world" + strVar);
    %>
</body>
</html>
