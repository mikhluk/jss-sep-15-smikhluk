<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="misctags" uri="http://javajoy.net/jss/miscTags" %>
<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 10.01.2016
  Time: 22:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User's Album</title>
</head>
<body>
    <jsp:useBean id="photoDAO" class="net.javajoy.jss.homework.w07_test.beans.PhotoDAO" scope="session"/>
    ${photoDAO.addToSession(pageContext.request.session)}
    <h3> Photos:</h3>
<table>
    <tr>
        <c:forEach var="i" begin="0" end="${photoDAO.size}">
            <td>
                <misctags:photoTag id="${i}"/>
            </td>
        </c:forEach>
    </tr>
    <tr>
        <c:forEach var="name" items ="${photoDAO.namesAsList}">
            <td>
                ${photoDAO.getPhotoName(i)}
            </td>
        </c:forEach>
    </tr>
    <tr>
        <c:forEach var="i" begin="0" end="${photoDAO.size}">
            <td>
                    <a href="/removePhotoServlet?id=${i}"> Remove</a>
            </td>
        </c:forEach>
    </tr>



</table>

<form action="/jss-sep-15-smikhluk/uploadPhoto" method="post" enctype="multipart/form-data">
    <input type="file" name="file" accept="image/jpeg"><br>
    <input type="submit" value="upload">
</form>



</body>
</html>
