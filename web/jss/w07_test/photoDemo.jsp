<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 16.01.2016
  Time: 18:51
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="misctags" uri="http://javajoy.net/jss/miscTags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>photo jstl demo</title>
</head>

<body>
<form method="get" accept-charset="UTF-8">
    <table>
        <tr>
            <td>Path Name</td>
            <td><input type="text" name="filterName" value="${param.filterName}" placeholder="Enter filter"></td>
        </tr>
        <tr>
            <td>Region</td>
            <td><input type="text" name="filterRegion" value="${param.filterRegion}"></td>
        </tr>
    </table>
    <p/>
    <input type="submit" value="Set filters">
    <input type="reset" value="Cancel" onclick="dosmth()"> <%--MS:TODO как сделать чтобы по reset очищались парамерты? --%>
</form>

<jsp:useBean id="personPhotoDAO" class="net.javajoy.jss.homework.w07.dao.PersonPhotoDAO" scope="page"/>
${personPhotoDAO.setFilter(0, param.filterName)}
${personPhotoDAO.setFilter(1, param.filterRegion)}
${personPhotoDAO.refreshPhoto()}
${personPhotoDAO.size}
<br>

<table border="1" cellspacing="0" width="700">
    <tr>
        <th>photo_id</th>
        <th>path</th>
        <th>region</th>
        <th>date</th>
        <th>views</th>
        <th>delete</th>
    </tr>

    <c:forEach var="photo" items="${personPhotoDAO.getPhotoAsList()}">

        <tr>
            <td>${photo.getId()} </td>
            <td>${photo.getPath()} </td>
            <td>${photo.getRegion().getName()}</td>
            <td>${photo.getDate()} </td>
            <td>${photo.getViews()} </td>
            <td><a href="/jss-sep-15-smikhluk/photoDBAction?action=delete&photo_id=${photo.getId()}"> Remove</a></td>
                <%--MS:TODO вызывается сервлет из него опять создается объект personPhotoDAO это долго, как удалить сразу отсюда? --%>

        </tr>
    </c:forEach>
</table>

</body>
</html>
