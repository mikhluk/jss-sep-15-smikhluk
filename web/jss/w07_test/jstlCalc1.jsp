<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 28.12.2015
  Time: 22:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<link rel="stylesheet" type="text/css" href="styles.css"/>
<html>
<head>
    <title>JSTL Calc</title>
</head>

<h2 class="header"> Calculator</h2>

<form method="get">
    <input type="text" name="a" size="10" value="${param.a}">
    <select name="operation" value="${param.operation}">
        <option value="+">Add</option>
        <option value="-">Subtract</option>
        <option value="*">Multiply</option>
        <option value="/">Divide</option>
    </select>
    <input type="text" name="b" size="10" value="${param.b}">

    <p></p>
    <input type="submit" value="Submit">
    <input type="reset" value="Reset">
</form>

<c:catch var="exception">
    <c:if test="${not empty param.a and not empty param.b and not empty param.operation}" var="testEmptyParam">
        <c:set var="a">
            <fmt:parseNumber value=""/>
        </c:set>
        <c:set var="result" value="0"/>
        <c:choose>
            <c:when test="${param.operation =='+'}">
                <c:set var="result" value="${0 + param.a + param.b}"/>
            </c:when>
            <c:when test="${param.operation =='-'}">
                <c:set var="result" value="${0 + param.a - param.b}"/>
            </c:when>
            <c:when test="${param.operation =='*'}">
                <c:set var="result" value="${0 + param.a * param.b}"/>
            </c:when>
            <c:when test="${param.operation =='/'}">
                <c:set var="result" value="${0 + param.a / param.b}"/>
            </c:when>
        </c:choose>
        <h2 class="result">Result: ${param.a} ${param.operation} ${param.b} = ${result}</h2>
    </c:if>
    <c:if test="${!testEmptyParam}">
        <h2> Eneter all data! </h2>
    </c:if>
</c:catch>
<c:if test="${not empty exception}">
    <h2> Error: ${exception}</h2>
</c:if>
</body>
</html>
