<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="misc" uri="http://javajoy.net/jss/miscFunctions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 08.01.2016
  Time: 17:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title> Functions </title>
</head>
<body>
    <h3> Standard </h3>

    <c:set var="str" value="abcdEF"/>
    ${fn:replace(fn:toLowerCase(str),"de","XXXXX")}
    <h3> Custom </h3>

    ${misc:concat("Abc","def")}
    <p></p>
    ${misc:map2String(header, "<br>", " : ")}
    <p></p>

    <fmt:setLocale value="ru_RU" scope ="session"/>
    <c:set var="newVar" value="abdc" scope="session"/>
        ${misc:map2String(sessionScope, "<br>", " : ")}


</body>
</html>
