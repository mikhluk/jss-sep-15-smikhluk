<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 09.01.2016
  Time: 21:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <title>Beans Test</title>
</head>
<body>


    <h3> Calculator </h3>
    <jsp:useBean id="calc" class="net.javajoy.jss.homework.w07_test.beans.CalculatorBean" scope="session"/>
    <jsp:setProperty name="calc" property="a" value="15"/>
    <jsp:setProperty name="calc" property="b" value="3"/>
    <jsp:setProperty name="calc" property="operation" value="-"/>
    ${calc.a} ${calc.operation} ${calc.b} = ${calc.result} <br>
    ${calc.reset()}
    ${calc.result} <br>


    <h3> JavaBeen demo </h3>
    <jsp:useBean id="dateValue" class="java.util.Date"/>
    <jsp:setProperty name="dateValue" property="time" value="${pageContext.session.creationTime}"/>
    <fmt:formatDate value="${dateValue}" pattern="dd/MM/yyyy HH:mm:ss"/>
    <br>

    <jsp:setProperty name="dateValue" property="time" value="${pageContext.session.lastAccessedTime}"/>
    <fmt:formatDate value="${dateValue}" pattern="dd/MM/yyyy HH:mm:ss"/>

</body>
</html>
