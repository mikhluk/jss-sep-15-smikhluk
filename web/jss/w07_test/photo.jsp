<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 10.01.2016
  Time: 21:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="net.javajoy.jss.homework.w07_test.beans.PhotoDAO" %>
<%@ page import="java.io.OutputStream" %>
<%@ page contentType="image/jpeg" %>

<%
    int id = Integer.parseInt(request.getParameter("id"));
    OutputStream binaryOut = response.getOutputStream();
    PhotoDAO dao = PhotoDAO.getPhotoDAO(request.getSession());
    if (dao != null && id < dao.getSize()) {
        byte data[] = dao.getPhotoData(id);
        binaryOut.write(data, 0, data.length);
    } else {
        // error image
    }

%>