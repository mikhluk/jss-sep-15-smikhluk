<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 28.12.2015
  Time: 10:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
    <title>JSTL</title>
</head>
<body>
<c:forEach var="i" begin="0" end="9">
    ${0 + i + 2} &nbsp; &nbsp;
</c:forEach>


<c:set var="clientID" scope="page" value="11234"/>

<c:if test="${not empty param['clientID']}">
    <c:set var="clientID" scope="session">
        ${param["clientID"]}
    </c:set>
    set client id
</c:if>
${ empty param["clientID"]? 33:1}

Client ID (page) = ${pageScope.clientID}
Client ID (session) = ${sessionScope.clientID}

<c:remove var="clientID" scope="page"/>

Client ID (page) = ${pageScope.clientID}
Client ID (session) = ${sessionScope.clientID}


<%--Session--%>
<br>

<h3>Session</h3>
Is new session (v1): ${pageContext.session.isNew()}<br>
Is new session (v2): ${pageContext.session.isNew()}<br>
Attribute : ${pageContext.session.setAttribute("a","abc")}
${pageContext.session.getAttribute("a")} <br>
Parameter (v1): ${pageContext.request.getParameter("clientID")}<br>
Parameter (v2): ${param.clientID}

<h3>Headers</h3>
<%--<c:set var="headerNames" value="${header.keySet()}"/>--%>
<c:set var="headerNames" value="${pageContext.request.headerNames}"/>
<c:forEach var="name" items="${headerNames}">
    ${name} = ${header[name]}<br>

</c:forEach>

<br>

<h3>ForEach</h3>
<c:set var="items" value="2,3,6,99,adsfdf,ieirtum, kkkk, ttt, 22, fkjfdk, 44,4,5,1"/>
<c:forEach var="elem" items="${items}" begin="2" end="8" step="2" varStatus="status">
    ${elem}${status.last? "<br>":","}
</c:forEach>

<br>


</body>
</html>
