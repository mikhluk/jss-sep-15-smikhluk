<%@ page import="net.javajoy.jss.homework.w06.DatabaseConnection" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="net.javajoy.jss.homework.w06.AppConfig" %>
<%@ page import="java.util.Properties" %>

<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 15.01.2016
  Time: 20:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>photo Person View</title>
</head>
<body>

<h3> Person photo view </h3>

<table border="1" cellspacing="0" width="700px">
    <tr>
        <th>pfid</th>
        <th>photo_id</th>
        <th>path</th>
        <th>name</th>
        <th>region</th>
        <th>date</th>
        <th>views</th>
    </tr>

    <%
        Properties conf = new AppConfig().getConfig(application);
        Connection conn = DatabaseConnection.getConnection(conf);

        String filterPhoto = request.getParameter("photo_id") == null ? "" : request.getParameter("photo_id");
        String sql = "SELECT * FROM view_person_photo WHERE photo_id = " + filterPhoto;
        String sqlExecuted = "";

        try (java.sql.PreparedStatement stmt = conn.prepareStatement(sql)) {
            ResultSet rs = stmt.executeQuery();
            sqlExecuted = stmt.toString();
            while (rs.next()) {
                out.println("<tr> <td>" +
                        rs.getLong("pfid") + "</td> <td> " +
                        rs.getLong("photo_id") + "</td> <td> " +
                        rs.getString("path") + "</td> <td>" +
                        rs.getString("name") + "</td> <td>" +
                        rs.getString("region_name") + "</td> <td>" +
                        rs.getDate("date") + "</td> <td>" +
                        rs.getInt("views") + "</td> </tr>");

            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    %>

</table>
<%= sqlExecuted %>
<p/>
<a href='/jss-sep-15-smikhluk/jss/w06/photoView.jsp'>back to photo View</a>


<h3> Add person</h3>


<form action="/jss-sep-15-smikhluk/dbAction" method="post" accept-charset="UTF-8">
    <table border=1 cellspacing=0 width=700px>
        <tr>
            <th>pfid</th>
            <th>photo_id</th>
            <th>name</th>
        <tr>
            <td></td>
            <td><%=request.getParameter("photo_id")%>
            </td>
            <td><input type="text" name="updateName" value="blablabla"></td>

        </tr>
    </table>
    <p/>
    <input type="hidden" name="action" value="addPerson">
    <input type="hidden" name="photo_id" value="<%=request.getParameter("photo_id")%>">
    <input type="hidden" name="pfid" value="-1">
    <input type="submit" value="Add person">
    <input type="reset" value="Cancel">
</form>


<br><br><br>


</body>
</html>
