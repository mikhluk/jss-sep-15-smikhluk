<%@ page import="net.javajoy.jss.homework.w06.DatabaseConnection" %>
<%@ page import="net.javajoy.jss.homework.w06.AppConfig" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.util.Properties" %>

<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 25.12.2015
  Time: 22:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title> Home Photo View</title>
</head>
<body>


<%
    Properties conf = new AppConfig().getConfig(application);
    Connection conn = DatabaseConnection.getConnection(conf);

    request.setCharacterEncoding("UTF-8");
    String filterPath = request.getParameter("filterPath") == null ? "" : request.getParameter("filterPath");
    String filterRegion = request.getParameter("filterRegion") == null ? "" : request.getParameter("filterRegion");
    String updatePath = request.getParameter("updatePath") == null ? "" : request.getParameter("updatePath");
    String updatePhoto_id = request.getParameter("photo_id") == null ? "" : request.getParameter("photo_id");
    int updateViews = request.getParameter("updateViews") == null ? 0 : new Integer(request.getParameter("updateViews"));

    String actionStatus = request.getParameter("actionStatus");

//    String actionStatus = (String) request.getAttribute("actionStatus");
    if (actionStatus == null) {
        actionStatus = "";
} else {%>
<div style="color:#EE0000"> The photo with id = <%=updatePhoto_id %> <%=actionStatus%>
</div>
<%
    }

%>
<h3> Filter </h3>

<form method="post" accept-charset="UTF-8">
    <table>
        <tr>
            <td>Path</td>
            <td><input type="text" name="filterPath" value="<%=filterPath%>"></td>
        </tr>
        <tr>
            <td>Region</td>
            <td><input type="text" name="filterRegion" value="<%=filterRegion%>"></td>
        </tr>
    </table>
    <p/>
    <input type="submit" value="Set Filters">
    <input type="reset" value="Cancel">
</form>

<table border="1" cellspacing="0" width="700px">

    <tr>

        <th>photo_id</th>
        <th>path</th>
        <th>region</th>
        <th>date</th>

        <th>views</th>
        <th>delete</th>
        <th>update</th>
        <th>person</th>
    </tr>

    <%
        //String sql = "SELECT * FROM view_person_photo ";
        String sql = "SELECT * FROM view_photo_region WHERE path LIKE ? and region_name LIKE ?";

        //"SELECT * FROM view_person_photo " +
        //"WHERE (date BETWEEN  ? AND ? ) AND (name LIKE ?) AND (region_name LIKE ?) "
        String sqlExecuted = "";

        try (java.sql.PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, "%" + filterPath + "%");
            stmt.setString(2, "%" + filterRegion + "%");
            ResultSet rs = stmt.executeQuery();
            sqlExecuted = stmt.toString();

            while (rs.next()) {

                String refDelete = String.format("<a href='/jss-sep-15-smikhluk/dbAction?photo_id=%d&action=delete'>Delete</a>", rs.getLong("photo_id"));
//            String refUpdate = String.format("<a href='/jss-sep-15-smikhluk/dbAction?photo_id=%d&action=update" +
//                    "&updatePath=%s&updateViews=%d'>Update</a>", rs.getLong("photo_id"), updatePath, updateViews);
                String refUpdate = String.format("<a href='?photo_id=%d&action=update'>Update</a>", rs.getLong("photo_id"));
                String refPerson = String.format("<a href='/jss-sep-15-smikhluk/jss/w06/personPhotoView.jsp?photo_id=%d'>Person</a>", rs.getLong("photo_id"));

                out.println("<tr> <td>" +
                        rs.getLong("photo_id") + "</td> <td> " +
                        rs.getString("path") + "</td> <td>" +
                        rs.getString("region_name") + "</td> <td>" +
                        rs.getDate("date") + "</td> <td>" +

                        rs.getInt("views") + "</td> <td>" +
                        refDelete + "</td> <td>" +
                        refUpdate + "</td> <td>" +
                        refPerson + "</td> </tr>");
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    %>

</table>

<div style="color: #AAA;"><%=sqlExecuted%>
</div>

<%
    if (request.getParameter("action") != null && request.getParameter("action").equals("update")) {
        out.print("<h3>Update fields</h3>");
        out.print("<form action='/jss-sep-15-smikhluk/dbAction' method='get' accept-charset='UTF-8'>");
%>

<table border=1 cellspacing=0 width=700px>
    <tr>
        <th>photo_id</th>
        <th>path</th>
        <th>views</th>
    <tr>

        <% String sql1 = "SELECT * FROM view_photo_region WHERE photo_id =" + request.getParameter("photo_id");
            Statement stm = conn.createStatement();
            ResultSet rs1 = stm.executeQuery(sql1);

            if (rs1.next()) {
        %>

        <td><input type="hidden" name="action" value="update">
            <input type="hidden" name="photo_id" value="<%=request.getParameter("photo_id")%>">
            <%=rs1.getLong("photo_id")%>
        </td>
        <td><input type="text" name="updatePath" value="<%=rs1.getString("path")%>"></td>
        <td><input type="text" name="updateViews" value="<%=rs1.getInt("views")%>"></td>

        <%
                }
            }
        %>


    </tr>
</table>

<input type="submit" value="Update">
<input type="reset" value="Cancel">

</form>


<h3> Add photo</h3>


<form  action="/jss-sep-15-smikhluk/dbAction" method="post" accept-charset="UTF-8">
    <table border=1 cellspacing=0 width=700px>
        <tr>
            <th>photo_id</th>
            <th>path</th>
            <th>region</th>
            <th>date</th>
            <th>views</th>
        <tr>
            <td></td>
            <td><input type="text" name="updatePath" value="d:\temp"></td>
            <td><input type="text" name="updateRegionName" value="odessa"></td>
            <td><input type="text" name="updateDate" value="2016-01-01"></td>
            <td><input type="text" name="updateViews"></td>

        </tr>
    </table>
    <p/>
    <input type="hidden" name="action" value="addPhoto">
    <input type="hidden" name="photo_id" value="-1">
    <input type="submit" value="Add photo">
    <input type="reset" value="Cancel">
</form>

<br><br><br>


</body>
</html>
