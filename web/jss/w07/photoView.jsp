<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 16.01.2016
  Time: 18:51
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="misctags" uri="http://javajoy.net/jss/miscTags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>photo jstl demo</title>
</head>

<body>

<jsp:useBean id="appConfig" class="net.javajoy.jss.homework.w07.bean.AppConfig" scope="page"/>
<c:set var="conf" value="${appConfig.getConfig(pageContext.servletContext)}" scope="page"/>

<form method="get" accept-charset="UTF-8">
    <table>
        <tr>
            <td>Path Name</td>
            <td><input type="text" name="filterName" value="${param.filterName}" placeholder="Enter path"></td>
        </tr>
        <tr>
            <td>Region</td>
            <td><input type="text" name="filterRegion" value="${param.filterRegion}" placeholder="Enter region"></td>
        </tr>
    </table>
    <p/>
    <input type="submit" value="Set filters">
    <input type="reset" value="Cancel">
</form>

<jsp:useBean id="filter" class="net.javajoy.jss.homework.w07.bean.Filter" scope="page"/>
${filter.setValue("photo_path", param.filterName)}
${filter.setValue("photo_region_name", param.filterRegion)}
${filter.setValue("person_id", param.person_id)}

<jsp:useBean id="photoDAO" class="net.javajoy.jss.homework.w07.dao.PhotoDAO" scope="page"/>
${photoDAO.addToSession(pageContext.request.session)}

<br>

<%--TODO: iteration tag --%>


<table border="1" cellspacing="0" width="800">
    <tr>
        <th>photo_id</th>
        <th>preview</th>
        <th>path</th>
        <th>region</th>
        <th>date</th>
        <th>views</th>
        <th>delete</th>
        <th>update</th>

    </tr>

    <c:forEach var="photo" items="${photoDAO.getAsList(filter)}">
            <misctags:photoCardTag photo="${photo}"/>

    </c:forEach>
</table>

<%--

    <c:photoList var="photo" items="${photoDAO.getAsList(filter)}" filter="...">
            <misctags:photoCardTag photo="${photo}"/>

    </c:forEach>

    // can access photoDAO within tag :
    PhotoDAO dao = (PhotoDAO) getJspContext().getAttribute("photoDAO", PageContext.PAGE_SCOPE);

--%>

<p/>

<h3>Update fields</h3>

<form action='/jss-sep-15-smikhluk/photoDBAction' method='post' accept-charset='UTF-8' enctype="multipart/form-data">
    <table border=1 cellspacing=0 width=700px>
        <tr>
            <th>photo_id</th>
            <th>path</th>
            <th>region</th>
            <th>date</th>
            <th>views</th>
        <tr>

            <c:set var="photo" value="${photoDAO.getById(param.photo_id)}" scope="page"/>
            <td><input type="hidden" name="action" value="update">
                <input type="hidden" name="photo_id" value="${param.photo_id}">
                ${param.photo_id}

            </td>
            <td><input type="text" name="updatePath" value="${photo.getPath()}"></td>
            <td><input type="text" name="updateRegionName" value="${photo.getRegion().getName()}"></td>
            <td><input type="text" name="updateDate" value="${photo.getDate()}"></td>
            <td><input type="text" name="updateViews" value="${photo.getViews()}"></td>

        </tr>
    </table>

    <input type="file" name="file" accept="image/jpeg"><br>
    <input type="submit" value="Update">
    <input type="reset" value="Cancel">

</form>

<h3> Add photo</h3>

<form action="/jss-sep-15-smikhluk/photoDBAction" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
    <table border=1 cellspacing=0 width=700px>
        <tr>
            <th>photo_id</th>
            <th>path</th>
            <th>region</th>
            <th>date</th>
            <th>views</th>
        <tr>
            <td></td>
            <td><input type="text" name="updatePath" value="d:\_ms_2468"></td>
            <td><input type="text" name="updateRegionName" value="odessa"></td>
            <td><input type="text" name="updateDate" value="2016-01-01"></td>
            <td><input type="text" name="updateViews"></td>

        </tr>
    </table>
    <p/>
    <input type="hidden" name="action" value="addPhoto">
    <input type="hidden" name="photo_id" value="-1">

    <input type="file" name="file" accept="image/jpeg"><br>
    <input type="submit" value="Add photo">
    <input type="reset" value="Cancel">
</form>


</body>
</html>
