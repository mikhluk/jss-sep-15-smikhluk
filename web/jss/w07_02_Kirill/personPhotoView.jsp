<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 23.01.2016
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Person photo view 07</title>
</head>
<body>

<a href='/jss-sep-15-smikhluk/jss/w07/photoView.jsp'>back to photo View</a>
<br>


<jsp:useBean id="personPhotoDAO" class="net.javajoy.jss.homework.w07.dao.PersonPhotoDAO" scope="page"/>

${personPhotoDAO.setFilter(0, param.photo_id)}
${personPhotoDAO.setFilter(1, "")}
<%--MS:TODO по хорошему надо бы навести порядок с фильтрами, сейчас в DAO три разных объекта,
и для каждого значение фильтра такруется по разному --%>
${personPhotoDAO.refreshPersonPhoto()}
${personPhotoDAO.refreshPerson()}


<br>

<table border="1" cellspacing="0" width="700">
    <tr>
        <th>pfid</th>
        <th>photo_id</th>
        <th>person_id</th>
        <th>name</th>
    </tr>




    <%--<some:PersonsOnPhoto items="${PhotoDAO}" var="person" filter="${filterMap}}" >--%>
        <%--<some:PersonInfo person="${person}" param="withPhoto" />     --%>
    <%--</some:PersonsOnPhoto>--%>

    <c:forEach var="personPhoto" items="${personPhotoDAO.getDataPersonPhoto()}">
        <tr>
            <td>${personPhoto.getPfID()} </td>
            <td>${personPhoto.getPhotoID()} </td>
            <td>
                <c:set var="personID" value="${personPhoto.getPersonID()}" scope="page"/>
                    ${personID}
            </td>
            <td>
                    ${personPhotoDAO.getPersonByID(personID).getName()}
                        <%--.name--%>
            </td>


        </tr>
    </c:forEach>
</table>

<p/>

<h3> Add person</h3>


<form action="/jss-sep-15-smikhluk/photoDBAction" method="post" accept-charset="UTF-8">
    <table border=1 cellspacing=0 width=700px>
        <tr>
            <th>photo_id</th>
            <th>name</th>
        <tr>
            <td>${param.photo_id}</td>
            <td><input type="text" name="updateName" value="blablabla"></td>

        </tr>
    </table>
    <p/>
    <input type="hidden" name="action" value="addPerson">
    <input type="hidden" name="photo_id" value="${param.photo_id}">
    <input type="hidden" name="pfid" value="-1">
    <input type="submit" value="Add person">
    <input type="reset" value="Cancel">
</form>


<br><br><br>

</body>
</html>
