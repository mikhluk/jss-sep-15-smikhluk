<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.Arrays" %>
<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 15.12.2015
  Time: 8:26
  To change this template use File | Settings | File Templates.
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">



<link rel="stylesheet" type="text/css" href="styles.css"/>

<html>
<head>
    <title>Calculator</title>
    <link rel="stylesheet" href="/jss/w06_test/styles.css" type="text/css">
</head>
<body>

<h2 class="header">Calculator</h2>

<%
    String stringA = request.getParameter("a");
    String stringB = request.getParameter("b");
    String oper = request.getParameter("operation");
%>
<form method="get">
    <input type="text" name="a" size="10" value='<%=stringA!=null? stringA : "" %>'>
    <select name="operation">
        <option value="+">Add</option>
        <option value="-">Substract</option>
        <option value="*">Multiplay</option>
        <option value="/">Divide</option>
    </select>
    <input type="text" name="b" size="10" value='<%=stringA!=null? stringB : "" %>'>

    <p></p>
    <input type="submit" value="Submit">
    <input type="reset" value="Reset">
</form>
<%

    if (stringA != null  && !stringA.equals("") && stringB != null && !stringB.equals("") && oper != null) {
        int a = Integer.parseInt(stringA);
        int b = Integer.parseInt(stringB);
        //int a = Integer.parseInt("1");
        //int b = Integer.parseInt("2");
        String res = a + " " + oper + " " + b + "=";

        switch (oper.charAt(0)) {
            case '+' : res+= a+b; break;
            case '-' : res+= a-b; break;
            case '/' : res+= a/b; break;
            case '*' : res+= a*b; break;
        }

%>

<h2 class='result'>Result: <%=res%>
</h2>
<%
} else {
%>
<h2>Enter 2 operands and select operation</h2>
<%
    }
%>

<div style="font-family: Courier New"> <p> Parameters <p>
<%
        Enumeration<String> prmNames = request.getParameterNames();
        while (prmNames.hasMoreElements()){
        String prmName = prmNames.nextElement();
        String prmValues[] = request.getParameterValues(prmName);
        %><%=prmName%> = <%= Arrays.toString(prmValues)%> <br> <%
        }
%>
</div>
</body>
</html>
