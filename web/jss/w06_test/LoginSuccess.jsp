<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 23.12.2015
  Time: 19:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login success page</title>
</head>
<body>
<form action="/jss-sep-15-smikhluk/LogoutServlet" method="post">
    <input type="submit" value="Logout">
    <%
        String userID = "";
        Cookie[] cookies = request.getCookies();
        if (cookies != null)
        {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("user_id")) {
                    userID = cookie.getValue();
                    break;
                }
            }
        }
        if (!userID.isEmpty()){
            out.println("Welcome, " + userID);
            response.setIntHeader("Expires", 0);
            response.setHeader("Pragma","no-cache");
            response.setHeader("Cache-Control","no-cache, no-store, must-revalidate");

            //response.setIntHeader("Refresh", 2);
        } else {
            response.sendRedirect("/jss-sep-15-smikhluk/jss/w06_test/Login.html");
        }

    %>

</form>

</body>
</html>
