<%@ page import="jss.w08_test.account.Account" %>
<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 14.02.2016
  Time: 22:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Account Operation</title>
</head>
<body>

<%
    //AccountBean remote = (AccountBean) session.getAttribute("remote");
    Account remote = (Account) session.getAttribute("remote");

    String operation = request.getParameter("operation");
    String amount = request.getParameter("amount");

    if (operation != null) {
        if (operation.equals("deposit")) {
            System.out.println("Start deposit!");
            remote.deposit(Integer.parseInt(amount));
            out.print("Amount successfully deposited!");
        } else if (operation.equals("withdraw")) {
            boolean status = remote.withdraw(Integer.parseInt(amount));
            if (status) {
                out.print("Amount successfully withdraw");
            } else {
                out.println("Enter less amount");
            }
        } else {
            out.println("Current Amount: " + remote.getBalance());
        }
    }
%>
<hr/>
<form action="">
    Enter Amount: <input type="text" name="amount"/><br>
    Choose Operation: <br>
    Deposit <input type="radio" name = "operation" value="deposit"><br>
    Withdraw <input type="radio" name = "operation" value="withdraw"><br>
    Check balance <input type="radio" name = "operation" value="checkbalance"><br>

    <input type="submit" name = "ok">
    <input type="reset" name = "cancel">

</form>


</body>
</html>
