<%@ page import="jss.w08_test.converter.Converter" %>
<%@ page import="jss.w08_test.converter.ConverterHome" %>
<%@ page import="javax.ejb.CreateException" %>
<%@ page import="javax.naming.Context" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="javax.rmi.PortableRemoteObject" %>
<%@ page import="java.math.BigDecimal" %>
<%@ page import="java.util.Enumeration" %>
<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 28.01.2016
  Time: 17:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Remote ejb view</title>
</head>
<body>

<%!
    private void traceContextNames(Context rootContext, String contextName) {
        System.out.println("Context: " + contextName);
        try {
            Context context = (Context) rootContext.lookup(contextName);
            Enumeration names = context.list("");
            while (names.hasMoreElements()) {
                System.out.println("\t" + names.nextElement());
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }
%>
<%
    //ConverterBean converter = null;
    Converter converter = null;
    try {
        Context initialContext = new InitialContext();

        traceContextNames(initialContext, "java:module");
        traceContextNames(initialContext, "java:app");
        traceContextNames(initialContext, "java:global");


        //не работает
//        Object objRef = initialContext.lookup("java:module/ConverterEJB");
//        if (objRef == null) {
//            System.out.println("jbjRef is null!!!!");
//        }
//        response.getWriter().print(objRef);

////        converter = (ConverterBean) initialContext.lookup("java:module/ConverterEJB");
//        converter = (ConverterBean) initialContext.lookup("java:module/ConverterEJB!ConverterBean");
//        response.getWriter().print(converter);
//


//        WARNING: Unexpected exception resolving reference
//        java.lang.NullPointerException: Cannot obtain an instance of the stateful session bean with a null session id
//        converter = (ConverterBean) initialContext.lookup("java:module/ConverterEJB!jss.w08.converter.ConverterBean");
//        response.getWriter().print(converter);


//
        //работает с java:module/ConverterEJB!jss.w08.converter.ConverterHome
//        ConverterHome converterHome = (ConverterHome) PortableRemoteObject.
//                narrow(initialContext.lookup("java:module/ConverterEJB!jss.w08.converter.ConverterHome"), ConverterHome.class);


        //работает с java:app/ejbs_ejb/ConverterEJB!jss.w08.converter.ConverterHome
        ConverterHome converterHome = (ConverterHome) PortableRemoteObject.
                narrow(initialContext.lookup("java:app/ejbs_ejb/ConverterEJB!jss.w08_test.converter.ConverterHome"), ConverterHome.class);


        //      response.getWriter().print(converterHome);

//////

        converter = converterHome.create();


        //  response.getWriter().print(converter);


    } catch (NamingException e) {
        e.printStackTrace();
    }

    try {
        response.getWriter().print("ConverterServlet11:");
        System.out.println(converter);
        String amount = request.getParameter("amount");
        if (amount == null || amount.isEmpty()) {
            amount = "20";
        }
        BigDecimal yen = converter.dollarToYen(new BigDecimal(amount));
        BigDecimal euro = converter.yenToEuro(yen);
        response.getWriter().format("%s dollars are %s yen or %s euro %n", amount, yen, euro);

    } catch (Exception e) {
        e.printStackTrace();
    }

%>

</body>
</html>
