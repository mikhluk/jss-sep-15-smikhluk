package jss.w08;

import jss.w08.dao.*;
import jss.w08.om.Person;
import jss.w08.om.Photo;

import javax.ejb.CreateException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;

/**
 * @author Sergey Mikhluk.
 */

@WebServlet(name = "photoDBAction", urlPatterns = {"/photoDBAction"})
@MultipartConfig(location = "d:/temp", maxFileSize = 16 * 1024 * 1024, fileSizeThreshold = 0)
public class DBActionServlet extends HttpServlet {
    private InitialContext initialContext = null;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String action = request.getParameter("action");
        String actionStatus;

        String photoId = request.getParameter("photo_id");
        if (photoId == null || photoId.equals("")) {
            actionStatus = "Wrong parameter photo_id!";

            // CK : Лучше в таких случаях делать перенаправление
            // на специальную страницу error.jsp, которая  actionStatus отобразит еще и пользователю
            // MS: исправил
            String refRedirect = String.format("jss/w08/error.jsp?&actionStatus=%s", actionStatus);
            response.sendRedirect(refRedirect);
            return;
        }
        Long id = new Long(photoId);

        actionStatus = "Wrong parameter action!";
        String refRedirect = String.format("jss/w08/error.jsp?&actionStatus=%s", actionStatus);
        if (action == null) {
            response.sendRedirect(refRedirect);
            return;
        }

        switch (action) {
            case "addPerson":
                actionStatus = actAddPerson(request, actionStatus);
                refRedirect = String.format("/ejbClient_war/openPersonPhoto?photo_id=%d&actionStatus=%s", id, actionStatus);
                break;

            case "deletePerson":
                Long personId = new Long(request.getParameter("person_id"));
                actionStatus = actDeletePerson(actionStatus, id, personId);
                refRedirect = String.format("/ejbClient_war/openPersonPhoto?photo_id=%d&actionStatus=%s", id, actionStatus);
                break;

            case "delete":
                actionStatus = actDelete(actionStatus, id);
                refRedirect = String.format("/ejbClient_war/openPhoto?photo_id=%d&actionStatus=%s", id, actionStatus);
                break;
            case "update":
                actionStatus = actUpdate(request, actionStatus);
                refRedirect = String.format("/ejbClient_war/openPhoto?photo_id=%d&actionStatus=%s", id, actionStatus);
                break;
            case "addPhoto":
                id = actAddPhoto(request);
                if (id > 0) {
                    actionStatus = "added";
                }
                refRedirect = String.format("/ejbClient_war/openPhoto?photo_id=%d&actionStatus=%s", id, actionStatus);
                break;
        }

        response.sendRedirect(refRedirect);
    }

    @Override
    public void init() throws ServletException {
        //super.init();
        // CK : получение контекста лучше вынести в init(), потому что этот метод вызывается многократно
        // MS: исправил
        try {
            initialContext = new InitialContext();
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    private String actUpdate(HttpServletRequest request, String actionStatus) throws IOException, ServletException {
        boolean success;

        Photo photo = new Photo(request.getParameter("photo_id"), request.getParameter("updatePath"),
                request.getParameter("updateDate"), request.getParameter("updateRegionName"),
                request.getParameter("updateViews"), getImageDataFromPart(request));

        PhotoDAO photoDAO = getPhotoDAORef();
        success = photoDAO.update(photo);
        if (success) {
            actionStatus = "updated";
        }
        return actionStatus;
    }

    private PhotoDAO getPhotoDAORef() throws RemoteException {
        PhotoDAO photoDAO = null;
        try {
            // CK : получение контекста лучше вынести в init(), потому что этот метод вызывается многократно
            // MS: исправил
            photoDAO = ((PhotoDAOHome) initialContext.lookup("java:module/PhotoDAOEJB!jss.w08.dao.PhotoDAOHome")).create();
        } catch (NamingException | CreateException e) {
            e.printStackTrace();
        }
        return photoDAO;
    }

    private byte[] getImageDataFromPart(HttpServletRequest request) throws IOException, ServletException {
        Part filePart = request.getPart("file");
        InputStream inputStream = filePart.getInputStream();
        byte[] buf = new byte[1024*4];
        ByteArrayOutputStream bos = new ByteArrayOutputStream(1024*1024);
        while (inputStream.read(buf) > 0) {
            bos.write(buf);
        }
        return bos.toByteArray();
    }

    private String actDelete(String actionStatus, Long id) throws RemoteException{
        boolean success;

        PhotoDAO photoDAO = getPhotoDAORef();
        success = photoDAO.delete(id);
        if (success) {
            actionStatus = "deleted";
        }
        return actionStatus;
    }

    private Long actAddPhoto(HttpServletRequest request) throws ServletException, IOException {
        //System.out.println("DBActionServlet.java case addPhoto"); //debug
        Photo photo = new Photo(request.getParameter("photo_id"), request.getParameter("updatePath"),
                request.getParameter("updateDate"), request.getParameter("updateRegionName"),
                request.getParameter("updateViews"), getImageDataFromPart(request));

        PhotoDAO photoDAO = getPhotoDAORef();
        return photoDAO.add(photo);
    }

    private String actAddPerson(HttpServletRequest request, String actionStatus) {
        //System.out.println("DBActionServlet.java case addPerson"); // debug
        PersonDAOBean personDAO = new PersonDAOBean();
        Long id = personDAO.addPersonOnPhoto(Long.valueOf(request.getParameter("photo_id")), new Person(request.getParameter("updateName")));

        if (id > 0) {
            actionStatus = "added";
        }
        return actionStatus;
    }

    private String actDeletePerson(String actionStatus, Long photoId, Long personId) {
        //System.out.println("DBActionServlet.java case deletePerson");  //debug
        boolean success;

        PersonDAOBean personDAO = new PersonDAOBean();
        success = personDAO.deletePersonFromPhoto(photoId, personId);

        if (success) {
            actionStatus = "deleted";
        }
        return actionStatus;
    }
}
