package jss.w08;

import jss.w08.bean.AppConfigBean;
import jss.w08.bean.Filter;
import jss.w08.dao.PersonDAO;
import jss.w08.dao.PersonDAOHome;
import jss.w08.dao.PhotoDAO;
import jss.w08.dao.PhotoDAOHome;
import jss.w08.om.Photo;

import javax.ejb.CreateException;
import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Sergey Mikhluk.
 */
@WebServlet(name = "OpenPersonPhotoView", urlPatterns = {"/openPersonPhoto"})
public class OpenPersonPhotoView extends HttpServlet {
    @EJB
    AppConfigBean appConfig;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        Context initialContext;
        PersonDAO personDAO = null;
        PhotoDAO photoDAO = null;
        Filter filter = new Filter();
        Filter emptyFilter = new Filter();

        try {
            initialContext = new InitialContext();
            personDAO = ((PersonDAOHome) initialContext.lookup("java:module/PersonDAOEJB!jss.w08.dao.PersonDAOHome")).create();
            photoDAO = ((PhotoDAOHome) initialContext.lookup("java:module/PhotoDAOEJB!jss.w08.dao.PhotoDAOHome")).create(); // было !jss.w08.dao.PhotoDAOBean");

        } catch (NamingException | CreateException e) {
            e.printStackTrace();
        }

        String photoId = request.getParameter("photo_id");
        String personName = request.getParameter("person_name");

        filter.setValue(Filter.FilterConstant.PHOTO_ID, photoId == null ? "" : photoId);
        filter.setValue(Filter.FilterConstant.PERSON_NAME, personName == null ? "" : personName);

        Photo photo = null;
        if (photoId != null && !photoId.equals("")) {
            if (photoDAO != null) {
                photo = photoDAO.getById(Long.valueOf(photoId));
            }
        }

        request.getSession().setAttribute("photoId", photoId);
        request.getSession().setAttribute("photo", photo);
        request.getSession().setAttribute("personDAO", personDAO);
        request.getSession().setAttribute("filter", filter);
        request.getSession().setAttribute("emptyFilter", emptyFilter);

        request.getRequestDispatcher("/jss/w08/personPhotoView.jsp").forward(request, response);
    }
}