package jss.w08;

import jss.w08.bean.AppConfigBean;
import jss.w08.bean.Filter;
import jss.w08.dao.PhotoDAO;
import jss.w08.dao.PhotoDAOHome;

import javax.ejb.CreateException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Sergey Mikhluk.
 */
@WebServlet(name = "OpenPhotoView", urlPatterns = {"/openPhoto"})
public class OpenPhotoView extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        PhotoDAO photoDAO = null;

        Filter filter = new Filter();
        AppConfigBean appConfig;

        try {
            Context initialContext;
            initialContext = new InitialContext();

            photoDAO = ((PhotoDAOHome) initialContext.lookup("java:module/PhotoDAOEJB!jss.w08.dao.PhotoDAOHome")).create();
            appConfig = (AppConfigBean) initialContext.lookup("java:module/AppConfigEJB!jss.w08.bean.AppConfigBean");

            appConfig.getConfig();

        } catch (NamingException | CreateException e) {
            e.printStackTrace();
        }

        request.getSession().setAttribute("photoDAO", photoDAO);
        request.getSession().setAttribute("filter", filter);

        // CK : Здесь RequestDispatcher.forward() лучше, чем sendRedirect(), потому что
        // в случае sendRedirect() при обновлении страницы personPhotoView.jsp этот сервлет повторно вызываться не будет
        // и объекты обновляться не будут.

        request.getRequestDispatcher("/jss/w08/photoView.jsp").forward(request, response);
    }
}
