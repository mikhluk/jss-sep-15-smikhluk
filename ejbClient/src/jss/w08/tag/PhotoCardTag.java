package jss.w08.tag;

import jss.w08.bean.Filter;
import jss.w08.om.Photo;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * @author Sergey Mikhluk.
 */
public class PhotoCardTag extends SimpleTagSupport {
    Long id = 0L;
    Photo photo;
    Filter filter;

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();

        out.print("<tr>");
        out.print("<td>" + photo.getId() + " </td>");
        out.print("<td align='center'> <img src='/ejbClient_war/photoCard?photo_id=" + photo.getId() +"' height=40>");
        out.print("<td> <a href='/ejbClient_war/openPersonPhoto?photo_id=" + photo.getId() + "'>" + photo.getPath() + "</a></td>");
        out.print("<td>" + photo.getRegion().getName() + " </td>");
        out.print("<td>" + photo.getDate() + " </td>");
        out.print("<td>" + photo.getViews() + " </td>");
        out.print("<td> <a href='/ejbClient_war/photoDBAction?action=delete&photo_id=" + photo.getId() + "'> X</a></td>");
        out.print("<td> <a href='?action=update&photo_id=" + photo.getId() + "'> Update</a></td>");

        out.print("</td>");
        out.print("</tr>");
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }
}


