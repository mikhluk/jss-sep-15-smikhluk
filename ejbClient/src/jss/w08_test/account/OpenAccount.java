package jss.w08_test.account;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

/**
 * @author Sergey Mikhluk.
 */
@WebServlet(name = "OpenAccount", urlPatterns = "/openAccount")
public class OpenAccount extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("openAccount");
        try {
            Context initialContext = new InitialContext();
            traceContextNames(initialContext, "java:module");
            traceContextNames(initialContext, "java:app");
            traceContextNames(initialContext, "java:global");
        } catch (NamingException e) {
            e.printStackTrace();
        }

        AccountBean accountBean = null;
        try {
            InitialContext context = new InitialContext();
            //accountBean = (AccountBean) context.lookup("java:module/AccountEJB");
            //accountBean = (AccountBean) context.lookup("java:module/AccountEJB!jss.w08_test.account.AccountBean");

            AccountHome accountHome = (AccountHome) context.lookup("java:module/AccountEJB!jss.w08_test.account.AccountHome");
            Account account = accountHome.create();


            request.getSession().setAttribute("remote", account);
            //request.getRequestDispatcher("/account/operation.jsp").forward(request, response);
            response.sendRedirect("jss/w08_test/account/operation.jsp");
        } catch (Exception e) {
            e.printStackTrace(response.getWriter());
        }

    }

    private void traceContextNames(Context rootContext, String contextName) {
        System.out.println("Context: " + contextName);
        try {
            Context context = (Context) rootContext.lookup(contextName);
            Enumeration names = context.list("");
            while (names.hasMoreElements()) {
                System.out.println("\t" + names.nextElement());
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }


}
