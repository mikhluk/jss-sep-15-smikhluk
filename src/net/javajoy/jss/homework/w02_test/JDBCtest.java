package net.javajoy.jss.homework.w02_test;

import java.sql.*;

/**
 * @author Sergey Mikhluk.
 */
public class JDBCtest {

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/homephoto";
    // Database credentials
    static final String USER = "root";
    static final String PASS = "123456";

    private static Connection connection = null;

    public static void main(String[] args) {
        Statement statement = null;

        try {
            Class.forName(JDBC_DRIVER);

            System.out.println("Connecting to database...");
            connection = DriverManager.getConnection(DB_URL, USER, PASS);

            System.out.println("Creating statement...");

            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("Select * from photo");

            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet rsTables = metaData.getTables(null, null, null, null);

            while (rsTables.next()){
                System.out.println(rsTables.getString("TABLE_NAME"));
            }
            rsTables.close();

            printTableData("person");
            System.out.println();

            printTableData("photo");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void printTableData(String tableName) throws  SQLException {
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM " + tableName);
        ResultSetMetaData meta  = rs.getMetaData();

        int cnt = meta.getColumnCount();
        for(int i=1; i<=cnt; i++) {
            System.out.print(meta.getColumnName(i) + " (" + meta.getColumnType(i) + ") \t");
        }
        System.out.println();

        while (rs.next()) {
            for (int i=1; i<=cnt; i++) {
                System.out.print(rs.getString(i) + " \t");
            }
            System.out.println();
        }
        rs.close();
        stmt.close();

    }



}
