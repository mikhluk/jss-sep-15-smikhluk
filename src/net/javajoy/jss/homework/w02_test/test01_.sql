select orders.id, orders.date, orders.product_id, orders.customer_id, orders.qty, customers.name, customers.adress, products.description, products.price
from orders
join customers on (orders.customer_id = customers.id)
join products on (orders.product_id = products.id)