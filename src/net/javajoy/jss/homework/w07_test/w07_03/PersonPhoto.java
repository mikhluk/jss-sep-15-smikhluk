package net.javajoy.jss.homework.w07_test.w07_03;

/**
 * @author Sergey Mikhluk.
 */
public class PersonPhoto {  // CK : этот класс не нужен
    private long pfID;
    private long photoID;
    private long personID;

    public PersonPhoto(long pfID, long photoID, long personID) {
        this.pfID = pfID;
        this.photoID = photoID;
        this.personID = personID;
    }


    public long getPfID() {
        return pfID;
    }

    public long getPhotoID() {
        return photoID;
    }

    public long getPersonID() {
        return personID;
    }
}
