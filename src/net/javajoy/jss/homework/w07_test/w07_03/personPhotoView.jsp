<%--
  Created by IntelliJ IDEA.
  User: Serg
  Date: 23.01.2016
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Person photo view 07</title>
</head>
<body>

<a href='/jss-sep-15-smikhluk/jss/w07/photoView.jsp'>back to photo View</a>
<br>

<jsp:useBean id="appConfig" class="net.javajoy.jss.homework.w07.bean.AppConfig" scope="page"/>
<c:set var="conf" value="${appConfig.getConfig(pageContext.servletContext)}" scope="page"/>

<jsp:useBean id="photoDAO" class="net.javajoy.jss.homework.w07.dao.PhotoDAO" scope="page"/>
<jsp:useBean id="personDAO" class="net.javajoy.jss.homework.w07.dao.PersonDAO" scope="page"/>

<c:set var="photo" value="${photoDAO.getById(param.photo_id)}" scope="page"/>

<table border=1 cellspacing=0 bordercolor="#CCCCCC" width=500>
    <tr>
        <th width="30">photo id</th>
        <th>path</th>
        <th>views</th>
        <th>region</th>
    </tr>

    <tr>
        <td>${photo.getId()}</td>
        <td>${photo.getPath()}</td>
        <td>${photo.getViews()}</td>
        <td>${photo.getRegion().getName()}</td>
    </tr>
</table>

<br>

<jsp:useBean id="filter" class="net.javajoy.jss.homework.w07.bean.Filter" scope="page"/>
${filter.setValue("photo_id", param.photo_id)}
${filter.setValue("person_name", param.person_name)}

<table border="1" cellspacing="0" width="300">
    <tr>
        <th width="30">person_id</th>
        <th>name</th>
        <th width="30">delete</th>

    </tr>


    <%--TODO: iteration tag--%>
    <%--MS: TODO не знаю как сделать--%>

    <%--<some:PersonsOnPhoto items="${PhotoDAO}" var="person" filter="${filterMap}}" >--%>
    <%--<some:PersonInfo person="${person}" param="withPhoto" />     --%>
    <%--</some:PersonsOnPhoto>--%>

    <c:forEach var="person" items="${personDAO.getAsList(filter)}">
        <tr>
            <td>${person.getId()}</td>
            <td> <font color="red">   ${person.getName()} </font></td>
            <td><a href="/jss-sep-15-smikhluk/photoDBAction?action=deletePerson&photo_id=${photo.getId()}&person_id=${person.getId()}"> X</a></td>
        </tr>
    </c:forEach>
</table>


<p/>

<h3> Add person on photo id = ${param.photo_id}</h3>


<form action="/jss-sep-15-smikhluk/photoDBAction" method="post" accept-charset="UTF-8">
    <table border=1 cellspacing=0 bordercolor="#CCCCCC" width=300px>
        <tr>
            <th>name</th>
        <tr>
            <td><input type="text" name="updateName" value="blablabla"></td>
        </tr>
    </table>
    <p/>
    <input type="hidden" name="action" value="addPerson">
    <input type="hidden" name="photo_id" value="${param.photo_id}">
    <input type="hidden" name="pfid" value="-1">
    <input type="submit" value="Add person">
    <input type="reset" value="Cancel">
</form>

<br>

<img src='/jss-sep-15-smikhluk/photoCard?photo_id=${photo.getId()}'>

<br>

<h3>All existing person:</h3>

<jsp:useBean id="emptyFilter" class="net.javajoy.jss.homework.w07.bean.Filter" scope="page"/>

<table border="1" cellspacing="0" width="300">
    <tr>
        <th width="30">person_id</th>
        <th>name</th>

    </tr>

    <c:forEach var="person" items="${personDAO.getAsList(emptyFilter)}">
        <tr>
            <td >${person.getId()}</td>
            <td > ${person.getName()} </td>
        </tr>
    </c:forEach>
</table>


<br>

</body>
</html>
