Вариант получения списка Person getAsList(), много дублирующегося кода


@Override
    public List<Person> getAsList(Filter filter) {
        //System.out.println("PersonDAO.java getPersonAsList");
        String sql;

        if (filter.getValue("photo_id") == null || filter.getValue("photo_id") == "") {
            return getPersonsAsList(filter);
        } else {
            return getPersonsOnPhotoAsList(filter);
        }
    }


    private List<Person> getPersonsAsList(Filter filter) {
        int listCapacity = 0;
        String sql;
        sql = "SELECT COUNT(*) AS quantity FROM person WHERE name LIKE ?";

        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            stmt.setString(1, "%" + filter.getValue("person_name") + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                listCapacity = rs.getInt("quantity");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(listCapacity);
        List<Person> list = new ArrayList(listCapacity);

        sql = "SELECT * FROM person WHERE name LIKE ?";
        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            stmt.setString(1, "%" + filter.getValue("person_name") + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Person person = new Person(
                        rs.getLong("id"),
                        rs.getString("name"));
                list.add(person);
            }
            System.out.println(stmt.toString());

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    private List<Person> getPersonsOnPhotoAsList(Filter filter) {
        int listCapacity = 0;
        String sql;

        sql = "SELECT COUNT(*) AS quantity FROM view_person_photo WHERE photo_id = ? AND name LIKE ?";
        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            stmt.setString(1, filter.getValue("photo_id"));
            stmt.setString(2, "%" + filter.getValue("person_name") + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                listCapacity = rs.getInt("quantity");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println(listCapacity);
        List<Person> list = new ArrayList(listCapacity);

        sql = "SELECT person_id, name FROM view_person_photo WHERE photo_id = ? AND name LIKE ?";
        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            stmt.setString(1, filter.getValue("photo_id"));
            stmt.setString(2, "%" + filter.getValue("person_name") + "%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Person person = new Person(
                        rs.getLong("person_id"),
                        rs.getString("name"));
                list.add(person);
            }
            System.out.println(stmt.toString());

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
