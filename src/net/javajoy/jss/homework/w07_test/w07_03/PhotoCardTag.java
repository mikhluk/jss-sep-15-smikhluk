package net.javajoy.jss.homework.w07_test.w07_03;

import net.javajoy.jss.homework.w07.om.Photo;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * @author Sergey Mikhluk.
 */
public class PhotoCardTag extends SimpleTagSupport {
    Long id = 0L;
    Photo photo;

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();

        out.print("<tr>");
        out.print("<td>" + photo.getId() + " </td>");
        out.print("<td> <a href='/jss-sep-15-smikhluk/jss/w07/personPhotoView.jsp?photo_id=" + photo.getId() + "'>" + photo.getPath() + "</a></td>");
        out.print("<td>" + photo.getRegion().getName() + " </td>");
        out.print("<td>" + photo.getDate() + " </td>");
        out.print("<td>" + photo.getViews() + " </td>");
        out.print("<td> <a href='/jss-sep-15-smikhluk/photoDBAction?action=delete&photo_id=" + photo.getId() + "'> X</a></td>");
        out.print("<td> <a href='?action=update&photo_id=" + photo.getId() + "'> Update</a></td>");
        //out.print("<a href='/jss-sep-15-smikhluk/photoCard?id=0'>123</a>");
        out.print("<td> <img src='/jss-sep-15-smikhluk/photoCard?photo_id=" + photo.getId() +"' height=40>");
        out.print("</td>");
        out.print("</tr>");
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

}


