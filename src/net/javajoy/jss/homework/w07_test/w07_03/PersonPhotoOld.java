package net.javajoy.jss.homework.w07_test.w07_03;

import net.javajoy.jss.homework.w07.om.Person;
import net.javajoy.jss.homework.w07.om.Photo;

/**
 * @author Sergey Mikhluk.
 */
public class PersonPhotoOld {   // CK : этот класс не нужен

    private long id = -1;
    private Person person = null;
    private Photo photo = null;

    public PersonPhotoOld(Person person, Photo photo) {
        this.id = - 1;
        this.person = person;
        this.photo = photo;
    }
    public PersonPhotoOld(long id, Person person, Photo photo) {
        this.id = id;
        this.person = person;
        this.photo = photo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }
}
