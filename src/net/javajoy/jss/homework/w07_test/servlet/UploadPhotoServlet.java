package net.javajoy.jss.homework.w07_test.servlet;

import net.javajoy.jss.homework.w07_test.beans.PhotoDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Sergey Mikhluk.
 */
@WebServlet(name = "UploadServlet", urlPatterns = "/uploadPhoto")
@MultipartConfig(location = "d:/temp", maxFileSize = 16*1024*1024, fileSizeThreshold = 0)
public class UploadPhotoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println("start uploadPhotoServlet");
        PhotoDAO dao = PhotoDAO.getPhotoDAO(request.getSession());
        Part filePart = request.getPart("file");
        if (filePart == null) {
            return;
        }
        System.out.println("file loaded");

        System.out.println(filePart.getName());
        System.out.println(filePart.getContentType());
        System.out.println(filePart.getSize());

        InputStream inputStream = filePart.getInputStream();
        int size = (int) filePart.getSize();
        byte[] buf = new byte[(int) size];
        inputStream.read(buf, 0, size);
        dao.addPhoto(filePart.getName(), buf);
        inputStream.close();

        response.sendRedirect("/jss-sep-15-smikhluk/jss/w07_test/album.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
