package net.javajoy.jss.homework.w07_test.beans;

/**
 * @author Sergey Mikhluk.
 */
public interface Figure {
    public int getSquare();

}
