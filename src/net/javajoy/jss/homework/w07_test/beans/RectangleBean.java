package net.javajoy.jss.homework.w07_test.beans;

import net.javajoy.jss.homework.w07.dao.DAO;
import net.javajoy.jss.homework.w07.dao.PhotoDAO;
import net.javajoy.jss.homework.w07.om.Photo;

/**
 * @author Sergey Mikhluk.
 */
public class RectangleBean implements Figure{
    private int a = 0;
    private int b = 0;
    private int square1 = 0;


    public RectangleBean() {
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    @Override
    public int getSquare() {
        square1 = a * b;
        System.out.println("rectangle!!");

        DAO<Photo> photoDAO = new PhotoDAO();

        return 1111;
    }

    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }


}
