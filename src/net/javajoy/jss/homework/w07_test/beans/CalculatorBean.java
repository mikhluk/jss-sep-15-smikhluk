package net.javajoy.jss.homework.w07_test.beans;

import java.io.Serializable;

/**
 * @author Sergey Mikhluk.
 */
public class CalculatorBean implements Serializable {
    private double a = 0;
    private double b = 0;
    private String operation = "+";

    public CalculatorBean() {
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public String getOperation() {
        return operation;
    }

    public double getResult(){
        double res = 0;
        switch (operation.charAt(0)) {
            case '+': res = a + b; break;
            case '-': res = a - b; break;
            case '*': res = a * b; break;
            case '/': res = a / b; break;
        }
        return res;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void reset(){
        a = b = 0;
        operation = "+";
    }
}
