package net.javajoy.jss.homework.w07_test.tag;

import javax.servlet.jsp.JspException;
import java.io.IOException;

/**
 * @author Sergey Mikhluk.
 */
public class ProstoTag extends SimpleTag {
    @Override
    public void doTag() throws JspException, IOException {
        getJspContext().getOut().println("The prosto tag");
    }
}
