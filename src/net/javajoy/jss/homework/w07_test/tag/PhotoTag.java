package net.javajoy.jss.homework.w07_test.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * @author Sergey Mikhluk.
 */
public class PhotoTag extends SimpleTagSupport{
    private int width = 100;
    private int height = 100;
    private int id = 0;

    @Override
    public void doTag() throws JspException, IOException {
//        JspWriter out = getJspContext().getOut();
//        out.println("<a href='photo.jsp?id=" + id + "'>");
//        out.println("<img src='photo.jsp?id=" + id + "' alt='photo' height='" + height + "' width='" + width + "'>");
//        out.println("</a>");


        JspWriter out = getJspContext().getOut();
        out.println("<a href='/jss-sep-15-smikhluk/photo?id=" + id + "'>");
        out.println("<img src='/jss-sep-15-smikhluk/photo?id=" + id + "' alt='photo' height='" + height + "' width='" + width + "'>");
        out.println("</a>");


    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setId(int id) {
        this.id = id;
    }
}
