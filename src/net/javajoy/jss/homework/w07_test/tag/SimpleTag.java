package net.javajoy.jss.homework.w07_test.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * @author Sergey Mikhluk.
 */
public class SimpleTag extends SimpleTagSupport{
    @Override
    public void doTag() throws JspException, IOException {
        getJspContext().getOut().println("The simple tag");
    }
}
