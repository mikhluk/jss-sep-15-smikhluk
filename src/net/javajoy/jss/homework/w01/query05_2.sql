/*
удалить контакт (по имени). При этом нужно удалить все записи, связанные с этим контактом 
в таблицах Телефон, E-mail, встреча


*/


# находим @contact_id_for_delete id удаляемого сотрудника по заданному firstName
SELECT @contact_id_for_delete:=id, firstName
FROM contact WHERE firstName = "kolya";

#SELECT @contact_id_for_delete;

# удаляем телефон и е-мейл сотрудника, который будет удален
DELETE FROM phone WHERE contact_id = @contact_id_for_delete;
DELETE FROM email WHERE contact_id = @contact_id_for_delete;

# находим found_meeting_id митинг в котором участвовал удаленный сотрудник 
SELECT @meeting_id_for_delete:=meeting_id
FROM contact_meeting WHERE contact_id = @contact_id_for_delete;


DELETE FROM contact_meeting WHERE contact_id = @contact_id_for_delete;
DELETE FROM contact WHERE id = @contact_id_for_delete;

# удаляем запись из таблицы meeting если в таблице contact_meeting нет ни одной записи с этим митингом
DELETE  from meeting
where meeting.id = @meeting_id_for_delete and not exists
(select * from organizer.contact_meeting where organizer.contact_meeting.meeting_id = @meeting_id_for_delete)



