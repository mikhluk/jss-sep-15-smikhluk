/*
Выбрать все встречи с заданным человеком (задается имя человека)
*/

select contact.firstName, contact.secondName, meeting.id, meeting.meeting_name, meeting.meeting_description, meeting.time
#select * 
from contact_meeting 
inner join contact on contact_meeting.contact_id = contact.id 
inner join meeting on contact_meeting.meeting_id = meeting.id

where contact.firstName = "petya"