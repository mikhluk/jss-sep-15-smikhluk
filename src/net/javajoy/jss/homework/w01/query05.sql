/*
удалить контакт (по имени). При этом нужно удалить все записи, связанные с этим контактом 
в таблицах Телефон, E-mail, встреча


*/


# находим @contact_id_for_delete id удаляемого сотрудника по заданному firstName
SELECT @contact_id_for_delete:=id, firstName
FROM contact WHERE firstName = "kolya";

#SELECT @contact_id_for_delete;

# удаляем телефон и е-мейл сотрудника, который будет удален
DELETE FROM phone WHERE contact_id = @contact_id_for_delete;
DELETE FROM email WHERE contact_id = @contact_id_for_delete;

DELETE FROM contact_meeting WHERE contact_id = @contact_id_for_delete;
DELETE FROM contact WHERE id = @contact_id_for_delete;

# удаляем все митинги по которым нет ни одной записи в таблице contact_meeting 
DELETE from organizer.meeting
where not exists
(select * from organizer.contact_meeting where organizer.contact_meeting.meeting_id = organizer.meeting.id)



