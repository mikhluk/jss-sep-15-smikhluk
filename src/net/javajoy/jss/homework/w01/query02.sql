/*
выбрать напоминания, для которых срок самого напоминания уже истек, но время встречи еще не прошло
*/

select distinct meeting.id as meeting_id, meeting.meeting_name, meeting.meeting_description, meeting.time, reminder.id as reminder_id, reminder.time
#select * 
from contact_meeting 

inner join meeting on contact_meeting.meeting_id = meeting.id
inner join reminder on reminder.meeting_id = meeting.id 

where reminder.time < date(now()) and meeting.time > date(now())