/*
добавить нового человека в список контактов (заданы имя контакта, телефон, e-mail)
*/

INSERT INTO `organizer`.`contact` (`firstName`, `secondName`) VALUES ('kolya', 'nikolaev');
set @new_id = last_insert_id();
INSERT INTO `organizer`.`phone` (`phone_name`, `contact_id`) VALUES ('999-99-99', @new_id);
INSERT INTO `organizer`.`email` (`email_name`, `contact_id`) VALUES ('99@gmail.com', @new_id);