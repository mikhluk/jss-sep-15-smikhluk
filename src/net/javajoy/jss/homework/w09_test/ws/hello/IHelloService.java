package net.javajoy.jss.homework.w09_test.ws.hello;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author Sergey Mikhluk.
 */

@WebService
public interface IHelloService {
    String sayHelloWorldFrom(@WebParam(name="from") String from);
}
