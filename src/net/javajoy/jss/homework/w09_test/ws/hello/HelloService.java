package net.javajoy.jss.homework.w09_test.ws.hello;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

/**
 * @author Sergey Mikhluk.
 */

//@WebService(endpointInterface = "net.javajoy.jss.homework.w09_test.ws.hello.IHelloService", serviceName = "IHelloService")
@WebService(serviceName = "IHelloService")
public class HelloService {
    @WebResult(name="helloString")
    @WebMethod(operationName = "sayHello")
    public String sayHelloWorldFrom(@WebParam(name="from") String from) {
           String result = "Hello, world, from " + from;
           System.out.println(result);
           return result;
      }
}
