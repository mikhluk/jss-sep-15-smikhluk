package net.javajoy.jss.homework.w09_test.ws.validator;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Sergey Mikhluk.
 */
@XmlRootElement
public class CreditCard {
    private String number;
    private int controlNumber;
    private String expiryDate;
    private String type;

    public CreditCard() {
        this.number = "0000 0000 0000 0000";
        controlNumber = 0;
        expiryDate = "";
        type = "Visa";
    }

    public CreditCard(String number, int controlNumber, String expiryDate, String type) {
        this.number = number;
        this.controlNumber = controlNumber;
        this.expiryDate = expiryDate;
        this.type = type;
    }


    @XmlAttribute(required = true)
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @XmlAttribute(required = false)
    public int getControlNumber() {
        return controlNumber;
    }

    public void setControlNumber(int controlNumber) {
        this.controlNumber = controlNumber;
    }

    @XmlAttribute(required = false)
    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    @XmlAttribute(required = false)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
