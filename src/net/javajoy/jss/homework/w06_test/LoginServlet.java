package net.javajoy.jss.homework.w06_test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Sergey Mikhluk.
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    private final String userID = "user1";
    private final String password = "123";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //get request parameters for userID and password
        String user = request.getParameter("user");
        String pwd = request.getParameter("pwd");
        System.out.println("LoginServlet - ok!!");

        if (userID.equals(user) && password.equals(pwd)){
            Cookie cookie = new Cookie("user_id",user);
            cookie.setMaxAge(2*60);
            response.addCookie(cookie);

            response.sendRedirect("/jss-sep-15-smikhluk/jss/w06_test/LoginSuccess.jsp");
        }else{
            //response.sendRedirect("/jss-sep-15-smikhluk/jss/w06_test/Login.html");
            //response.setStatus(300);
            //response.setHeader("Location","/jss-sep-15-smikhluk/jss/w06_test/Login.html");
            //response.setHeader("Location","/jss-sep-15-smikhluk/LogoutServlet");

            ServletContext application = request.getServletContext();
            RequestDispatcher dispatcher = application.getRequestDispatcher("/jss/w06_test/Login.html");
            PrintWriter out = response.getWriter();
            dispatcher.include(request, response);
            out.println("Try again !!!!!<br>");

        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
