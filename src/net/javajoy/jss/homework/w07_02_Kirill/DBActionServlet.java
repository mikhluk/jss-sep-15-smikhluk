//package net.javajoy.jss.homework.w07_02_Kirill;
//
//import net.javajoy.jss.homework.w06.DatabaseConnection;
//import net.javajoy.jss.homework.w07_02_Kirill.dao.DAO;
//import net.javajoy.jss.homework.w07_02_Kirill.dao.PersonPhotoDAO;
//import net.javajoy.jss.homework.w07_02_Kirill.om.Photo;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.sql.Connection;
//
///**
// * @author Sergey Mikhluk.
// */
//
//@WebServlet(name = "photoDBAction", urlPatterns = {"/photoDBAction"})
//public class DBActionServlet extends HttpServlet {
//
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        doGet(request, response);
//    }
//
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        String dbName = getServletContext().getInitParameter("home_photo_db_name");
//        String dbUser = getServletContext().getInitParameter("mysql_user");
//        String dbPass = getServletContext().getInitParameter("mysql_pass");
//
//        String action = request.getParameter("action");
//        String actionStatus = "";
//        Long id = new Long(request.getParameter("photo_id"));
//
//        if (action == null) {
//            actionStatus = "Wrong parameter action!";
//            String refRedirect = String.format("/jss-sep-15-smikhluk/jss/w07/photoView.jsp?photo_id=%d&actionStatus=%s", id, actionStatus);
//            response.sendRedirect(refRedirect);
//            return;
//        }
//
//
//        Connection conn = DatabaseConnection.getConnection(dbName == null ? "homephoto" : dbName, dbUser, dbPass);
//
//        String refRedirect;
//        if (action.equals("addPerson")) {
//            actionStatus = actAddPerson(request, actionStatus, id, conn);
//            refRedirect = String.format("/jss-sep-15-smikhluk/jss/w07/personPhotoView.jsp?photo_id=%d&actionStatus=%s", id, actionStatus);
//
//        } else {
//            switch (action) {
//                case "delete":
//                    actionStatus = actDelete(actionStatus, id, conn);
//                    break;
//                case "update":
//                    actionStatus = actUpdate(request, actionStatus, conn);
//                    break;
//                case "addPhoto":
//                    id = actAddPhoto(request, id, conn);
//                    if (id > 0) {
//                        actionStatus = "added";
//                    }
//                    break;
//            }
//            refRedirect = String.format("/jss-sep-15-smikhluk/jss/w07/photoView.jsp?photo_id=%d&actionStatus=%s", id, actionStatus);
//        }
//
//        response.sendRedirect(refRedirect);
//    }
//
//    private String actUpdate(HttpServletRequest request, String actionStatus, Connection conn) {
//        boolean success;
//        DAO<Photo> personPhotoDAO = new PersonPhotoDAO();
//        success = personPhotoDAO.updatePhoto(request.getParameter("photo_id"), request.getParameter("updatePath"),
//                "2015-01-02", "odessa", request.getParameter("updateViews") );
//
//
//        if (success) {
//            actionStatus = "updated";
//        }
//        return actionStatus;
//    }
//
//    private String actDelete(String actionStatus, Long id, Connection conn) {
//        boolean success;
//
//        DAO<Photo> personPhotoDAO = new PersonPhotoDAO();
//        success = personPhotoDAO.deletePhoto(id);
//
//        if (success) {
//            actionStatus = "deleted";
//        }
//        return actionStatus;
//    }
//
//    private Long actAddPhoto(HttpServletRequest request, Long id, Connection conn) {
//        //System.out.println("DBActionServlet.java case addPhoto");
//
//        DAO<Photo> personPhotoDAO = new PersonPhotoDAO();
//        id = personPhotoDAO.addSample(request.getParameter("updatePath"), request.getParameter("updateDate"),
//                request.getParameter("updateRegionName"), request.getParameter("updateViews"));
//
//        return id;
//    }
//
////    MS:TODO переписать добавление Person через DAO
//    private String actAddPerson(HttpServletRequest request, String actionStatus, Long id, Connection conn) {
//        //System.out.println("DBActionServlet.java case addPerson");
//
//        DAO<Photo> personPhotoDAO = new PersonPhotoDAO();
//        id = personPhotoDAO.addPerson(request.getParameter("updateName"), request.getParameter("photo_id"));
//
//        //new Person(request.getParameter("updateName"), request.getParameter("photo_id"))
//
//        // <jsp:usebean class=Person var="p"/>
//        // ${p.name}
//
//        if (id > 0) {
//            actionStatus = "added";
//        }
//        return actionStatus;
//    }
//}
