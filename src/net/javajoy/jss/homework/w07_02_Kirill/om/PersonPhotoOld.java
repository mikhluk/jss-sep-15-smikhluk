package net.javajoy.jss.homework.w07_02_Kirill.om;

/**
 * @author Sergey Mikhluk.
 */
public class PersonPhotoOld {

    private long id = -1;
    private net.javajoy.jss.homework.w07_02_Kirill.om.Person person = null;
    private net.javajoy.jss.homework.w07_02_Kirill.om.Photo photo = null;

    public PersonPhotoOld(net.javajoy.jss.homework.w07_02_Kirill.om.Person person, net.javajoy.jss.homework.w07_02_Kirill.om.Photo photo) {
        this.id = - 1;
        this.person = person;
        this.photo = photo;
    }
    public PersonPhotoOld(long id, net.javajoy.jss.homework.w07_02_Kirill.om.Person person, net.javajoy.jss.homework.w07_02_Kirill.om.Photo photo) {
        this.id = id;
        this.person = person;
        this.photo = photo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public net.javajoy.jss.homework.w07_02_Kirill.om.Person getPerson() {
        return person;
    }

    public void setPerson(net.javajoy.jss.homework.w07_02_Kirill.om.Person person) {
        this.person = person;
    }

    public net.javajoy.jss.homework.w07_02_Kirill.om.Photo getPhoto() {
        return photo;
    }

    public void setPhoto(net.javajoy.jss.homework.w07_02_Kirill.om.Photo photo) {
        this.photo = photo;
    }
}
