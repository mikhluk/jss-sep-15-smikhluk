package net.javajoy.jss.homework.w07_02_Kirill.om;

import java.sql.Date;

/**
 * @author Sergey Mikhluk.
 */
public class Photo {
    private long id = -1;
    private String path ="";
    private Date date = null;
    private long regionId = 0;
    private net.javajoy.jss.homework.w07_02_Kirill.om.Region region = null;
    private int views = 0;

    public Photo(long id, String path, Date date, long regionId, net.javajoy.jss.homework.w07_02_Kirill.om.Region region, int views) {
        this.id = id;
        this.path = path;
        this.date = date;
        this.regionId = regionId;
        this.region = region;
        this.views = views;
    }

    public Photo(String path, Date date, net.javajoy.jss.homework.w07_02_Kirill.om.Region region, int views) {
        this.id = -1;
        this.path = path;
        this.date = date;
        //this.regionId = regionId;
        this.region = region;
        this.views = views;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getRegionId() {
        return regionId;
    }

    public net.javajoy.jss.homework.w07_02_Kirill.om.Region getRegion() {
        return region;
    }

    public void setRegion(net.javajoy.jss.homework.w07_02_Kirill.om.Region region) {
        this.region = region;
    }
    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }
}
