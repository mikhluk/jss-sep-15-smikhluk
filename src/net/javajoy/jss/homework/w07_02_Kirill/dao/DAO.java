package net.javajoy.jss.homework.w07_02_Kirill.dao;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Mikhluk.
 */
public interface DAO<T> extends Iterable<T>{

    // CK : Р­С‚РѕС‚ РёРЅС‚РµСЂС„РµР№СЃ РІ РїСЂРёРјРµСЂРµ СЏ СЃРґРµР»Р°Р» РєР°Рє Р·Р°РіРѕС‚РѕРІРєСѓ, РґР»СЏ СЃР»СѓС‡Р°СЏ РїСЂРѕРіСЂР°РјРјРЅРѕРіРѕ С„РѕСЂРјРёСЂРѕРІР°РЅРёСЏ СѓСЃР»РѕРІРёСЏ where
    // Р§С‚РѕР±С‹ СЌС‚Рѕ СѓСЃР»РѕРІРёРµ СЃС‚СЂРѕРёР»РѕСЃСЊ РёСЃРєР»СЋС‡РёС‚РµР»СЊРЅРѕ РјРµС‚РѕРґР°РјРё Filter, c СЌРєСЂР°РЅРёСЂРѕРІР°РЅРёРµРј Рё С‚.Рї.
    // Р•СЃР»Рё С‚С‹ РїРѕР»СЊР·СѓРµС€СЊСЃСЏ PreparedStatement, С‚Рѕ СЌС‚РѕС‚ РёРЅС‚РµСЂС„РµР№СЃ С‚РµР±Рµ СѓР¶Рµ РЅРµ РЅСѓР¶РµРЅ

    // Р Р°Р·РІРµ С‡С‚Рѕ РјРѕР¶РЅРѕ РёСЃРїРѕР»СЊР·РѕРІР°С‚СЊ РґР»СЏ РїРµСЂРґСЃС‚Р°РІР»РµРЅРёСЏ РЅР°СЃС‚СЂРѕРµРє РІРјРµСЃС‚Рѕ РјР°СЃСЃРёРІР° String[] filter, РЅР°РїСЂРёРјРµСЂ РјР°Рї (РёРјСЏ РїРѕР»СЏ - Р·РЅР°С‡РµРЅРёРµ С„РёР»СЊС‚СЂР°)
    // С‚РѕРіРґР° РїРѕСЃС‚СЂРѕРµРЅРёРµ Р·Р°РїСЂРѕСЃР° Р±СѓРґРµС‚ Р±РѕР»РµРµ РїСЂРѕР·СЂР°С‡РЅС‹Рј:
    // stmt.setString(filter.get("someFieldVlue"));
    // stmt.setString(filter.get("someFieldVlueGT"))
    interface RequestBuilder{
        Statement getStatement();

    }

    void setFilter(int pos, String filter);


    long addSample(String tPath, String tDate, String tRegion1, String tViews);

    //MS:TODO С‡С‚Рѕ РѕР·РЅР°С‡Р°РµС‚ Р±СѓРєРІР° Рў, РїРѕС‡РµРјСѓ РґР»СЏ getByID T РїРѕРґС…РѕРґРёС‚, РґР»СЏ getPersonByID Р±СѓРґРµС‚ РѕС€РёР±РєР°
    T getByID(long id);
    T getPersonByID(long id);
    T getByPosition(int pos);
    List<T> getPhotoAsList();
    List<net.javajoy.jss.homework.w07_02_Kirill.om.Person> getPersonAsList();
   // List<T> getPhotoAsList(Predicate<T> predicate);
    long addPhoto(T person);
    long addPerson(String tName, String tPhotoID);
    boolean updatePhoto(T person);
    boolean updatePhoto(String id, String tPath, String tDate, String tRegion1, String tViews) ;
    boolean deletePhoto(T person);
    boolean deletePhoto(long id);
    boolean changePhoto(long id, String path, int views);
    void refreshPhoto();
    void refreshPersonPhoto();
    void refreshPerson();
    int getSize();
    ArrayList<net.javajoy.jss.homework.w07_02_Kirill.om.PersonPhoto> getDataPersonPhoto();

}
