//package net.javajoy.jss.homework.w07_02_Kirill.dao;
//
//import jdk.internal.org.objectweb.asm.Type;
//import net.javajoy.jss.homework.w02.DatabaseConnection;
//
//import net.javajoy.jss.homework.w07_02_Kirill.om.Person;
//import net.javajoy.jss.homework.w07_02_Kirill.om.PersonPhoto;
//import net.javajoy.jss.homework.w07_02_Kirill.om.Photo;
//import net.javajoy.jss.homework.w07_02_Kirill.om.Region;
//
//import java.io.Serializable;
//import java.sql.*;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.LinkedHashMap;
//import java.util.List;
//
////import java.sql.Date;
////import java.util.Date;
//
///**
// * @author Sergey Mikhluk.
// */
//public class PersonPhotoDAO implements DAO<Photo>, Serializable {
//
//    private String dbName = "homephoto";
//    public static final int FILTERS_QUANTITY = 4;
//
//    LinkedHashMap<Long, net.javajoy.jss.homework.w07_02_Kirill.om.Photo> dataPhoto = new LinkedHashMap<>(100);  // todo: remove data copy, store all changes directly into DB
//
//    //MS:TODO РѕР±СЏР·Р°С‚РµР»СЊРЅРѕ Р»Рё РґР»СЏ С…СЂР°РЅРµРЅРёСЏ Person РёСЃРїРѕР»СЊР·РѕРІР°С‚СЊ LinkedHashMap? РќРµ РјРѕР¶РµРј Р»Рё РјС‹ РѕР±РѕР№С‚РёСЃСЊ РЅР°РїСЂРёРјРµСЂ СЃРїРёСЃРєРѕРј, Р»РёР±Рѕ РµС‰Рµ РєР°РєРёРј РЅРёР±СѓРґСЊ РІР°СЂРёР°РЅС‚РѕРј?
//    LinkedHashMap<Long, net.javajoy.jss.homework.w07_02_Kirill.om.Person> dataPerson = new LinkedHashMap<>(100);
//
//    //MS:TODO РїСЂР°РІРёР»СЊРЅРѕ Р»Рё Р·Р°РєР»СЋС‡Р°С‚СЊ РІ РѕРґРёРЅ PersonPhotoDAO РІСЃРµ С‚СЂРё РѕР±СЉРµРєС‚Р° Photo, PersonPhoto Рё Person? Р�Р»Рё Р»СѓС‡С€Рµ Р±С‹Р»Рѕ СЃРґРµР»Р°С‚СЊ РЅР° РєР°Р¶РґС‹Р№ РѕР±СЉРµРєС‚ РѕС‚РґРµР»СЊРЅРѕРµ DAO?
//    ArrayList<net.javajoy.jss.homework.w07_02_Kirill.om.PersonPhoto> dataPersonPhoto = new ArrayList<>(100);
//    Connection conn = null;
//    PreparedStatement stmt = null;
//    String[] filter = new String[FILTERS_QUANTITY];
//
//    public PersonPhotoDAO() {
//        conn = DatabaseConnection.getConnection(dbName);
//    }
//
//    @Override
//    public int getSize() {
//        return dataPhoto.size();
//    }
//
//    @Override
//    public net.javajoy.jss.homework.w07_02_Kirill.om.Photo getByID(long id) {
//        return dataPhoto.get(id);
//    }
//
//    @Override
//    public Person getPersonByID(long id) {
//        return dataPerson.get(id);
//    }
//
//    @Override
//    public net.javajoy.jss.homework.w07_02_Kirill.om.Photo getByPosition(int pos) {
//        if (pos >= 0 && pos <= dataPhoto.size()) {
//            return (net.javajoy.jss.homework.w07_02_Kirill.om.Photo) dataPhoto.values().toArray()[pos];
//        } else return null;
//    }
//
//    @Override
//    public List<net.javajoy.jss.homework.w07_02_Kirill.om.Photo> getPhotoAsList() {
//        List list = new ArrayList(dataPhoto.size());
//        dataPhoto.values().forEach(o -> list.add(o));
//        return list;
//    }
//
//    //MS:TODO РґРІР° РїРѕС…РѕР¶РёС… РјРµС‚РѕРґР°, СЃС‚РѕРёС‚ Р»Рё РѕР±СЉРµРґРёРЅРёС‚СЊ?
//    @Override
//    public List<net.javajoy.jss.homework.w07_02_Kirill.om.Person> getPersonAsList() {
//        List list = new ArrayList(dataPerson.size());
//        dataPerson.values().forEach(o -> list.add(o));
//        return list;
//    }
//
////    @Override
////    public List<PersonPhoto> getPhotoAsList(Predicate<PersonPhoto> predicate) {
////        List list = new ArrayList(dataPhoto.size());
////       // dataPhoto.values().stream().filter(predicate).forEach(o -> list.add(o));
////        return list;
////    }
//
//    @Override
//    public long addPhoto(net.javajoy.jss.homework.w07_02_Kirill.om.Photo photo) {
//        Connection conn = DatabaseConnection.getConnection(dbName);
//        long id = -1L;
//        String sql = "call insertPhoto(?,?,?,?,?)";
//        try (CallableStatement stmt = conn.prepareCall(sql)) {
//
//            stmt.setString(1, photo.getPath());
//            stmt.setDate(2, photo.getDate());
//            stmt.setString(3, photo.getRegion().getName());
//            stmt.setInt(4, photo.getViews());
//            stmt.registerOutParameter(5, Type.LONG);
//
//            stmt.execute();
//            id = stmt.getLong(5);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return id;
//    }
//
//    @Override
//    public long addSample(String tPath, String tDate, String tRegion1, String tViews) {
//        System.out.println("add sample");
//        long id = -1L;
//
//        try {
//            //tDate = "2015-10-15";
//            java.util.Date date_utl = new SimpleDateFormat("yyyy-MM-dd").parse(tDate);
//            Date dat_sql = new Date(date_utl.getTime());
//            int views = (tViews == "" ? 0 : new Integer(tViews));
//            net.javajoy.jss.homework.w07_02_Kirill.om.Photo photo = new net.javajoy.jss.homework.w07_02_Kirill.om.Photo(tPath, dat_sql, new net.javajoy.jss.homework.w07_02_Kirill.om.Region(tRegion1), views);
//
//            id = addPhoto(photo);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return id;
//    }
//
//
//
////    MS:TODO Р·Р°РїРёСЃС‹РІР°РµРј СЃСЂР°Р·Сѓ РІ Р±Р°Р·Сѓ Р° РЅРµ СЃРѕР·РґР°РµРј РѕР±СЉРµРєС‚ Person РІ DAO, СЌС‚Рѕ РЅРµ РѕС€РёР±РєР°?
//    @Override
//    public long addPerson(/*Person person*/ String tName, String tPhotoID) {
//        System.out.println("PersonPhotoDAO.java addPerson");
//        Long id = -1L;
//
//        Connection conn = DatabaseConnection.getConnection(dbName);
//        String sql = "call insertPersonOnPhoto(?,?,?)";
//        try (CallableStatement stmt = conn.prepareCall(sql)) {
//            stmt.setString(1, tName);
//            stmt.setString(2, tPhotoID);
//            stmt.registerOutParameter(3, Type.LONG);
//
//            stmt.execute();
//            id = stmt.getLong(3);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return id;
//
//    }
//    //MS:TODO РїРѕС‡РµРјСѓ С‚СѓС‚ Photo Р° РІ DAO T person ?
//    @Override
//    public boolean updatePhoto(net.javajoy.jss.homework.w07_02_Kirill.om.Photo photo) {
//        return false;
//    }
//
//    @Override
//    public boolean updatePhoto(/* Photo*/ String tID, String tPath, String tDate, String tRegion1, String tViews) {
//        Connection conn = DatabaseConnection.getConnection(dbName);
//        boolean executeStatus = false;
//        System.out.println("PersonPhotoDAO.java updatePhoto");
//
//        //MS:TODO РЅРµ РЅСѓР¶РЅРѕ Р»Рё Р·РґРµСЃСЊ РєСЂРѕРјРµ Р·Р°РїРёСЃРё РІ Р±Р°Р·Сѓ mysql РµС‰Рµ РґРµР»Р°С‚СЊ РёР·РјРµРЅРµРЅРёСЏ РІ РѕР±СЉРµРєС‚Рµ photo РїСЂРёРЅР°РґР»РµР¶Р°С‰РµРј DAO?
//        //MS:TODO РґРѕРїРёСЃР°С‚СЊ РІ РїСЂРѕС†РµРґСѓСЂСѓ РѕР±РЅРѕРІР»РµРЅРёРµ РґР°С‚С‹ Рё СЂРµРіРёРѕРЅР°
//        //MS:TODO РіРґРµ РґРѕР»Р¶РЅР° РїСЂРѕРёСЃС…РѕРґРёС‚СЊ РїСЂРѕРµРІРµСЂРєР° С‡С‚Рѕ РЅР°Рј РїРµСЂРµРґР°Р»Рё РїСѓСЃС‚РѕРµ Р·РЅР°С‡РµРЅРёРµ tDate? С‚СѓС‚ РёР»Рё, РЅР°РїСЂРёРјРµСЂ, РІ DBACtionServlet.java?
//        try (PreparedStatement stmt = conn.prepareStatement(
//                "UPDATE photo SET path=?, views=? WHERE id=?",
//                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
//
//            java.util.Date date_utl = new SimpleDateFormat("yyyy-MM-dd").parse(tDate);
//            Date dat_sql = new Date(date_utl.getTime());
//            tViews = (tViews == "") ? "0" : tViews;
//
//            stmt.setString(1, tPath);
//            stmt.setString(2, tViews);
//            stmt.setString(3, tID);
//
//            int key = stmt.executeUpdate();
//            if (key > 0) {
//                executeStatus = true;
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//
//        }
//        return executeStatus;
//    }
//
//    @Override
//    public boolean deletePhoto(long id) {
//        Connection conn = DatabaseConnection.getConnection(dbName);
//        boolean success = false;
//
//        String sql = "call deletePhoto(?,?)";
//        try (CallableStatement stmt = conn.prepareCall(sql)) {
//            stmt.setLong(1, id);
//            stmt.registerOutParameter(2, Type.LONG);
//
//            stmt.execute();
//            success = stmt.getBoolean(2);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return success;
//    }
//
//    @Override
//    public boolean deletePhoto(net.javajoy.jss.homework.w07_02_Kirill.om.Photo photo) {
//        return false;
//    }
//
//    @Override
//    public Iterator<net.javajoy.jss.homework.w07_02_Kirill.om.Photo> iterator() {
//        return dataPhoto.values().iterator();
//    }
//
//    @Override
//    public void setFilter(int pos, String filter) {
//        this.filter[pos] = filter;
//    }
//
//    @Override
//    public boolean changePhoto(long id, String path, int views) {
//        Connection conn = DatabaseConnection.getConnection(dbName);
//        boolean success = false;
//
//        //try (Statement stmt = conn.createStatement()){
//        try (PreparedStatement stmt = conn.prepareStatement(
//                "SELECT * FROM photo",
//                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
//
//            ResultSet rs = stmt.executeQuery();
//            while (rs.next()) {
//
//                if (rs.getLong("id") == id) {
//                    rs.updateString("path", path);
//                    rs.updateInt("views", views);
//                    rs.updateRow();
//                    success = true;
//                    break;
//                }
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return success;
//    }
//
//    @Override
//    public void refreshPhoto() {
//        Connection conn = DatabaseConnection.getConnection(dbName);
//
//        String sql = "SELECT * FROM view_photo_region WHERE path LIKE ? and region_name LIKE ?";
//        String sqlExecuted = "";
//
//        try (PreparedStatement stmt = conn.prepareStatement(sql,
//                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
//
//            stmt.setString(1, "%" + filter[0] + "%");
//            stmt.setString(2, "%" + filter[1] + "%");
//
//            ResultSet rs = stmt.executeQuery();
//            sqlExecuted = stmt.toString();
//
//            while (rs.next()) {
//                net.javajoy.jss.homework.w07_02_Kirill.om.Region region = new net.javajoy.jss.homework.w07_02_Kirill.om.Region(rs.getString("region_name"));
//
//                net.javajoy.jss.homework.w07_02_Kirill.om.Photo photo = new net.javajoy.jss.homework.w07_02_Kirill.om.Photo(
//                        rs.getLong("photo_id"),
//                        rs.getString("path"),
//                        rs.getDate("date"),
//                        rs.getLong("region_id"),
//                        region,
//                        rs.getInt("views"));
//
//
//                dataPhoto.put(photo.getId(), photo);
//            }
//            System.out.println(sqlExecuted);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    //MS:TODO С‚СЂРё РїРѕС…РѕР¶РёС… РјРµС‚РѕРґР° refreshPersonPhoto refreshPhoto refreshPerson, РІРѕР·РјРѕР¶РЅРѕ РјРѕР¶РЅРѕ РІС‹РЅРµСЃС‚Рё С‡Р°СЃС‚СЊ РѕР±С‰РµР№ Р»РѕРіРёРєРё
//    @Override
//    public void refreshPersonPhoto() {
//        Connection conn = DatabaseConnection.getConnection(dbName);
//
//        String sql = "SELECT * FROM view_person_photo WHERE photo_id = ?";
//        String sqlExecuted = "";
//
//        try (PreparedStatement stmt = conn.prepareStatement(sql,
//                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
//
//            stmt.setString(1, filter[0]);
//
//            ResultSet rs = stmt.executeQuery();
//            sqlExecuted = stmt.toString();
//
//            while (rs.next()) {
//                net.javajoy.jss.homework.w07_02_Kirill.om.PersonPhoto personPhoto = new net.javajoy.jss.homework.w07_02_Kirill.om.PersonPhoto(
//                        rs.getLong("pfid"),
//                        rs.getLong("photo_id"),
//                        rs.getLong("person_id"));
//                dataPersonPhoto.add(personPhoto);
//            }
//            System.out.println(sqlExecuted);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    @Override
//    public void refreshPerson() {
//        Connection conn = DatabaseConnection.getConnection(dbName);
//
////        MS:TODO РїРѕ С…РѕСЂРѕС€РµРјСѓ С‚СѓС‚ РЅР°РґРѕ РѕРіСЂР°РЅРёС‡РёС‚СЊ Рё РІС‹Р±РёСЂР°С‚СЊ РЅРµ РІСЃРµС… person Р° С‚РѕР»СЊРєРѕ С‚РµС… РєС‚Рѕ РµСЃС‚СЊ РЅР° РЅСѓР¶РЅРѕРј photo_id
//        String sql = "SELECT * FROM person";
//        String sqlExecuted = "";
//
//        try (PreparedStatement stmt = conn.prepareStatement(sql,
//                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
//
//            ResultSet rs = stmt.executeQuery();
//            sqlExecuted = stmt.toString();
//
//            while (rs.next()) {
//
//                net.javajoy.jss.homework.w07_02_Kirill.om.Person person = new net.javajoy.jss.homework.w07_02_Kirill.om.Person(rs.getLong("id"), rs.getString("name"));
//                dataPerson.put(person.getId(), person);
//            }
//            System.out.println(sqlExecuted);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    @Override
//    public ArrayList<net.javajoy.jss.homework.w07_02_Kirill.om.PersonPhoto> getDataPersonPhoto() {
//        return dataPersonPhoto;
//    }
//}
