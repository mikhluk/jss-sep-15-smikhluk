//package net.javajoy.jss.homework.w07_02_Kirill.dao;
//
//import java.util.List;
//
///**
// * @author Cyril Kadomsky
// */
//public class PhotoDAO  implements net.javajoy.jss.homework.w07_02_Kirill.dao.DAO<net.javajoy.jss.homework.w07_02_Kirill.om.Photo> {
//        // implement DAO methods (which work with Person)
//
//        net.javajoy.jss.homework.w07_02_Kirill.om.Photo getById(long id) {}
//
//
//        // May place this methods in PersonPhotoDAO
//        List<net.javajoy.jss.homework.w07_02_Kirill.om.Person> getPersonsAsList(long idPhoto) {}
//        List<net.javajoy.jss.homework.w07_02_Kirill.om.Person> getPersonsAsList(net.javajoy.jss.homework.w07_02_Kirill.om.Photo photo) {}
//
//}
