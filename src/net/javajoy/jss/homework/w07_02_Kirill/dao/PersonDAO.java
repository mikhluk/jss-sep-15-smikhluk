//package net.javajoy.jss.homework.w07_02_Kirill.dao;
//
//import java.util.List;
//
///**
// * @author Cyril Kadomsky
// */
//
//// Work with Person table
//public class PersonDAO implements net.javajoy.jss.homework.w07_02_Kirill.dao.DAO<net.javajoy.jss.homework.w07_02_Kirill.om.Person> {
//
//    // implement DAO methods (which work with Person)
//
//    net.javajoy.jss.homework.w07_02_Kirill.om.Person getById(long id) {}
//
//
//    // May place this methods in PersonPhotoDAO
//    List<net.javajoy.jss.homework.w07_02_Kirill.om.Photo> getPhotosAsList(long idPerson) {}
//    List<net.javajoy.jss.homework.w07_02_Kirill.om.Photo> getPhotosAsList(net.javajoy.jss.homework.w07_02_Kirill.om.Person person) {}
//
//}
