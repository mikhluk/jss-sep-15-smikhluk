package net.javajoy.jss.homework.w07.bean;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author Sergey Mikhluk.
 */

public class Filter implements Serializable {
    //Filter keys:
    //"photo_id"
    //"photo_path"
    //"photo_region_name"
    //"person_id"
    //"person_name"

    private HashMap<String, String> data = new HashMap<>(100);

    public Filter() {
    }

    public HashMap<String, String> getFilter() {
        return data;
    }

    public String getValue(String key){
        String value = (data.get(key) == null) ? "":  data.get(key);
        return value;
    }

    public void setValue(String key, String value ) {
        data.put(key, value);
    }

}


