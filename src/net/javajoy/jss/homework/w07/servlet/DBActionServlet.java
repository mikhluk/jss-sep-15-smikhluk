package net.javajoy.jss.homework.w07.servlet;

import net.javajoy.jss.homework.w07.dao.DAO;
import net.javajoy.jss.homework.w07.dao.PersonDAO;
import net.javajoy.jss.homework.w07.dao.PhotoDAO;
import net.javajoy.jss.homework.w07.om.Person;
import net.javajoy.jss.homework.w07.om.Photo;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Sergey Mikhluk.
 */

@WebServlet(name = "photoDBAction", urlPatterns = {"/photoDBAction"})
@MultipartConfig(location = "d:/temp", maxFileSize = 16 * 1024 * 1024, fileSizeThreshold = 0)
public class DBActionServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        String actionStatus = "";
        Long id = new Long(request.getParameter("photo_id"));

        if (action == null) {
            actionStatus = "Wrong parameter action!";
            String refRedirect = String.format("/jss-sep-15-smikhluk/jss/w07/photoView.jsp?photo_id=%d&actionStatus=%s", id, actionStatus);
            response.sendRedirect(refRedirect);
            return;
        }

        String refRedirect = "";

        switch (action) {
            case "addPerson":

                //MS: зачем actionStatus передавать как параметр
                // CK : Нужно чтобы на странице personPhotoView.jsp как-то отобразилось сообщение о результате выполненного действия
                // Посколтку personPhotoView.jsp вызовется уже в ответ на следующий http-запрос от клиента, то обычные способы передачи не подходят
                // По-дугому принципе передавать это сообщение можно (и это даже лучше) через атрибуты сессии
                actionStatus = actAddPerson(request, actionStatus, id);
                refRedirect = String.format("/jss-sep-15-smikhluk/jss/w07/personPhotoView.jsp?photo_id=%d&actionStatus=%s", id, actionStatus);
                break;

            case "deletePerson":
                Long personId = new Long(request.getParameter("person_id"));
                actionStatus = actDeletePerson(actionStatus, id, personId);
                refRedirect = String.format("/jss-sep-15-smikhluk/jss/w07/personPhotoView.jsp?photo_id=%d&actionStatus=%s", id, actionStatus);
                break;

            case "delete":
                actionStatus = actDelete(actionStatus, id);
                refRedirect = String.format("/jss-sep-15-smikhluk/jss/w07/photoView.jsp?photo_id=%d&actionStatus=%s", id, actionStatus);
                break;
            case "update":
                actionStatus = actUpdate(request, actionStatus);
                refRedirect = String.format("/jss-sep-15-smikhluk/jss/w07/photoView.jsp?photo_id=%d&actionStatus=%s", id, actionStatus);
                break;
            case "addPhoto":
                id = actAddPhoto(request, id);
                if (id > 0) {
                    actionStatus = "added";
                }
                refRedirect = String.format("/jss-sep-15-smikhluk/jss/w07/photoView.jsp?photo_id=%d&actionStatus=%s", id, actionStatus);
                break;
        }

        response.sendRedirect(refRedirect);
    }

    private String actUpdate(HttpServletRequest request, String actionStatus) throws IOException, ServletException {
        boolean success;
        PhotoDAO photoDAO = new PhotoDAO();

        Photo photo = new Photo(request.getParameter("photo_id"), request.getParameter("updatePath"),
                request.getParameter("updateDate"), request.getParameter("updateRegionName"),
                request.getParameter("updateViews"));

        Part filePart = request.getPart("file");
        InputStream inputStream = filePart.getInputStream();
        photo.setImgInputStream(inputStream);   // todo: store data as byte[] inside Photo instance


        success = photoDAO.update(photo);
        if (success) {
            actionStatus = "updated";
        }
        return actionStatus;
    }

    private String actDelete(String actionStatus, Long id) {
        boolean success;

        DAO<Photo> photoDAO = new PhotoDAO();
        success = photoDAO.delete(id);

        if (success) {
            actionStatus = "deleted";
        }
        return actionStatus;
    }

    private Long actAddPhoto(HttpServletRequest request, Long id) throws ServletException, IOException {
        //System.out.println("DBActionServlet.java case addPhoto");
        Photo photo = new Photo(request.getParameter("photo_id"), request.getParameter("updatePath"),
                request.getParameter("updateDate"), request.getParameter("updateRegionName"),
                request.getParameter("updateViews"));

        Part filePart = request.getPart("file");
        InputStream inputStream = filePart.getInputStream();
        photo.setImgInputStream(inputStream);

        DAO<Photo> PhotoDAO = new PhotoDAO();
        id = PhotoDAO.add(photo);
        return id;
    }

    private String actAddPerson(HttpServletRequest request, String actionStatus, Long id) {
        //System.out.println("DBActionServlet.java case addPerson");
        PersonDAO personDAO = new PersonDAO();
        id = personDAO.addPersonOnPhoto(Long.valueOf(request.getParameter("photo_id")), new Person(request.getParameter("updateName")));

        if (id > 0) {
            actionStatus = "added";
        }
        return actionStatus;
    }

    private String actDeletePerson(String actionStatus, Long photoId, Long personId) {
        //System.out.println("DBActionServlet.java case addPerson");
        boolean success;

        PersonDAO personDAO = new PersonDAO();
        success = personDAO.deletePersonFromPhoto(photoId, personId);

        if (success) {
            actionStatus = "deleted";
        }
        return actionStatus;
    }
}
