package net.javajoy.jss.homework.w07.om;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @author Sergey Mikhluk.
 */
public class Photo {
    private long id = -1;
    private String path = "";
    private Date date = null;
    //private long regionID = 0;
    private Region region = null;
    private int views = 0;
    private InputStream imgInputStream = null;

    private byte data[];

    public void setData(InputStream imgInputStream) throws IOException {
        byte[] buf = new byte[1024*4];
        ByteArrayOutputStream bos = new ByteArrayOutputStream(1024*1024);
        while (imgInputStream.read(buf) > 0) {
            bos.write(buf);
        }
        data = bos.toByteArray();
    }

    public byte[] getData() {
        return data;
    }

    public InputStream getDataInputStream() {
        return new ByteArrayInputStream(data);
    }


    public void setImgInputStream(InputStream imgInputStream) {
        this.imgInputStream = imgInputStream;
    }

    public InputStream getImgInputStream() {

        return imgInputStream;
    }



    public Photo(long id, String path, Date date, Region region, int views, InputStream imgInputStream) {
        this.id = id;
        this.path = path;
        this.date = date;
        //  this.regionID = regionID;
        this.region = region;
        this.views = views;
        //this.imgInputStream = imgInputStream;
        try {
            setData(imgInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Photo(String path, Date date, Region region, int views, InputStream imgInputStream) {
        this.id = -1;
        this.path = path;
        this.date = date;
        //this.regionID = regionID;
        this.region = region;
        this.views = views;
        //this.imgInputStream = imgInputStream;
        try {
            setData(imgInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public Photo(String tId, String tPath, String tDate, String tRegion, String tViews) {
        try {
            this.id = new Long(tId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        this.path = tPath;

        try {
            java.util.Date date_utl = new SimpleDateFormat("yyyy-MM-dd").parse(tDate);
            Date dat_sql = new Date(date_utl.getTime());
            this.date = dat_sql;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.region = new Region(tRegion);

        try {
            this.views = (tViews == "" ? 0 : new Integer(tViews));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

//    public long getRegionID() {
//        return regionID;
//    }
//
//    public void setRegionID(int regionID) {
//        this.regionID = regionID;
//    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }
}
