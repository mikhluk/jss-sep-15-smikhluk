package net.javajoy.jss.homework.w07.dao;

import net.javajoy.jss.homework.w07.bean.Filter;

import java.util.List;

/**
 * @author Sergey Mikhluk.
 */
public interface DAO<T> {
    T getById(long id);
    List<T> getAsList(Filter filter);
    long add(T element);
    boolean update(T element);
    boolean delete(long id);
    boolean delete(T element);
}
