package net.javajoy.jss.homework.w07.dao;

import jdk.internal.org.objectweb.asm.Type;
import net.javajoy.jss.homework.w06.DatabaseConnection;
import net.javajoy.jss.homework.w07.bean.AppConfig;
import net.javajoy.jss.homework.w07.bean.Filter;
import net.javajoy.jss.homework.w07.om.Person;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author Sergey Mikhlyuk
 */

public class PersonDAO implements DAO<Person>, Serializable {
    private Connection conn = null;

    public PersonDAO() {
        Properties conf = new AppConfig().getConfig();
        conn = DatabaseConnection.getConnection(conf);
    }

    @Override
    public List<Person> getAsList(Filter filter) {
        //System.out.println("PersonDAO.java getPersonAsList");

        if (filter.getValue("photo_id") == null || filter.getValue("photo_id") == "") {
            // CK : Должно быть два разных метода:
            //  - получить людей, отмеченных на заданном фото -
            //  - получить просто всех людейб возможно с фильтром по полям Person
            // Написанное больше похоже на первый метод, тогда назвать надо getAsListByPhoto(long photoId) ?

            // Получить просто список всех людей этотметод не позволит - это будет отдельно getAsList()
            // В нем лучше будет обращаться не к view_person_photo, а к Person (см. getById() )
            // MS: исправил

            // CK: Очень плохая практика пересылать из БД все данные о фото и людях (select *), если реально здесь используются из них только 2 столбца
            // MS: исправил
            return getPersonsAsList(false, filter);
        } else {
            return getPersonsAsList(true, filter);
        }
    }

    private List<Person> getPersonsAsList(boolean photoFilterFlag, Filter filter) {
        // CK : Можно сначала запросить количество записей из БД (select count(*) from ...)
        // и создавать список сразу нужного  размера
        // MS: исправил

        // CK : Хорошо, но нолько подсчет же тоже должен быть в getPersonsAsList(), getPersonsOnPhotoAsList(), с учетом фильтра
        //  А так у тебя резервируется место под всех пользователей, дажже если через фильтр пройдет только один
        // MS: исправил и избавился от метода getPersonsOnPhotoAsList() поскольку получалось очень много дублирующегося кода
        int listCapacity = 0;
        String sql;

        if (photoFilterFlag) {
            sql = "SELECT COUNT(*) AS quantity FROM view_person_photo WHERE name LIKE ?  AND photo_id = ?";
        }else {
            sql = "SELECT COUNT(*) AS quantity FROM person WHERE name LIKE ?";
        }

        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            stmt.setString(1, "%" + filter.getValue("person_name") + "%");

            if (photoFilterFlag) {
                stmt.setString(2, filter.getValue("photo_id"));
            }

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                listCapacity = rs.getInt("quantity");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println(listCapacity);
        List<Person> list = new ArrayList(listCapacity);


        if (photoFilterFlag) {
            sql = "SELECT person_id, name FROM view_person_photo WHERE name LIKE ? AND photo_id = ?";
        }else {
            sql = "SELECT * FROM person WHERE name LIKE ?";
        }

        //sql = "SELECT person_id, name FROM view_person_photo WHERE photo_id = ? AND name LIKE ?";
        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {

            stmt.setString(1, "%" + filter.getValue("person_name") + "%");
            if (photoFilterFlag) {
                stmt.setString(2, filter.getValue("photo_id"));
            }
            ResultSet rs = stmt.executeQuery();

            String personId = photoFilterFlag ? "person_id" : "id";
            while (rs.next()) {
                Person person = new Person(
                        rs.getLong(personId),
                        rs.getString("name"));
                list.add(person);
            }
            System.out.println(stmt.toString());

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Person getById(long id) {
        String sql = "SELECT * FROM person WHERE id = ?";
        String sqlExecuted = "";

        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
                    // CK : исправил CONCUR_UPDATABLE на CONCUR_READ_ONLY.
                    // Незачем блокировать таблицу на время работы с этим результатом, если он используется только для чтения.
                    // Где-то еще у тебя CONCUR_UPDATABLE видел не к месту использованный ...
                    // MS: исправил везде CONCUR_UPDATABLE на CONCUR_READ_ONLY, везде результаты используются только для чтения
            stmt.setString(1, Long.toString(id));

            ResultSet rs = stmt.executeQuery();
            sqlExecuted = stmt.toString();
            System.out.println(sqlExecuted);

            if (rs.next()) {
                Person person = new Person(
                        rs.getLong("id"),
                        rs.getString("name"));
                return person;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long add(Person person) {   //MS: будем ли добавлять person просто так без фото? Вопрос проектирования
        return 0;
    }

    public long addPersonOnPhoto(Long photoID, Person person) {
        //CK: плохо, что в одни методы id передаются как long, а в другие - как строки. Интерфейс должен быть однообразен.
        //MS: исправил

        System.out.println("PersonPhotoDAO.java addPerson");
        Long id = -1L;

        String sql = "call insertPersonOnPhoto(?,?,?)";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setString(1, person.getName());
            stmt.setString(2, String.valueOf(photoID));
            stmt.registerOutParameter(3, Type.LONG);

            stmt.execute();
            id = stmt.getLong(3);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public boolean update(Person person) {
        return false;
    }

    public boolean deletePersonFromPhoto(long photoId, long personId ) {
        //MS: Может быть два варианта, удалить person из БД полностью, а может быть просто с фото.
        // Если с БД полностью, то надо проверять сначала что человека нет на других фото

        // CK : При удалении "полностью" можно просто принудительно удалять все его отметки на фото.
        // Эти две команды delete лучше объединить в процедуру

        // CK : Удалить человека из одного конкретного фото - это уже будет другой метод
        // MS : Исправил. Вариант удаления "полностью" не делал

        boolean success = false;

        String sql = "call deletePerson(?,?,?)";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setLong(1, photoId);
            stmt.setLong(2, personId);
            stmt.registerOutParameter(3, Type.LONG);

            stmt.execute();
            success = stmt.getBoolean(3);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }

    @Override
    public boolean delete(Person person) {
        return delete(person.getId());
    }

    @Override
    public boolean delete(long id ) {
        return false;
    }

}
