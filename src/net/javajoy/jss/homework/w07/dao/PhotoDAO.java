package net.javajoy.jss.homework.w07.dao;

import jdk.internal.org.objectweb.asm.Type;
import net.javajoy.jss.homework.w06.DatabaseConnection;
import net.javajoy.jss.homework.w07.bean.AppConfig;
import net.javajoy.jss.homework.w07.bean.Filter;
import net.javajoy.jss.homework.w07.om.Photo;
import net.javajoy.jss.homework.w07.om.Region;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author Sergey Mikhluk.
 */
public class PhotoDAO implements DAO<Photo>, Serializable {
    // CK : Те же проблемы, что и в PersonDAO
    // MS: Исправил
    private Connection conn = null;
    private List<byte[]> photoData = new ArrayList<>(10);
    public static final String ATTRIBUTE_NAME = "PhotoDAO";

    public PhotoDAO() {
        Properties conf = new AppConfig().getConfig();
        conn = DatabaseConnection.getConnection(conf);
    }

    @Override
    public Photo getById(long id) {
        String sql = "SELECT * FROM view_photo_region WHERE photo_id = ?";

        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            stmt.setString(1, Long.toString(id));
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Region region = new Region(rs.getLong("region_id"), rs.getString("region_name"));
                Photo photo = new Photo(
                        rs.getLong("photo_id"),
                        rs.getString("path"),
                        rs.getDate("date"),
                        region,
                        rs.getInt("views"),
                        rs.getBinaryStream("image"));
                System.out.println(stmt.toString());
                return photo;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Photo> getAsList(Filter filter) {
        if (filter.getValue("person_id") == null || filter.getValue("person_id") == "") {
            return getPhotosAsList(false, filter);
        } else {
            return getPhotosAsList(true, filter);
        }
    }

    private List<Photo> getPhotosAsList(boolean personFilterFlag, Filter filter) {
        int listCapacity = 0;
        String sql;

        if (personFilterFlag) {
            sql = "SELECT COUNT(*) AS quantity FROM view_person_photo WHERE path LIKE ? and region_name LIKE ? and person_id = ?";

        } else {
            sql = "SELECT COUNT(*) AS quantity FROM view_photo_region WHERE path LIKE ? and region_name LIKE ?";
        }

        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            stmt.setString(1, "%" + filter.getValue("photo_path") + "%");
            stmt.setString(2, "%" + filter.getValue("photo_region_name") + "%");

            if (personFilterFlag) {
                stmt.setString(3, filter.getValue("person_id"));
            }
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                listCapacity = rs.getInt("quantity");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        List<Photo> list = new ArrayList(listCapacity);
        System.out.println(listCapacity);

        if (personFilterFlag) {
            sql = "SELECT * FROM view_person_photo WHERE path LIKE ? and region_name LIKE ? and person_id = ? ";
        } else {
            sql = "SELECT * FROM view_photo_region WHERE path LIKE ? and region_name LIKE ?";
        }

        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {

            stmt.setString(1, "%" + filter.getValue("photo_path") + "%");
            stmt.setString(2, "%" + filter.getValue("photo_region_name") + "%");
            if (personFilterFlag) {
                stmt.setString(3, filter.getValue("person_id"));
            }
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Region region = new Region(rs.getLong("region_id"), rs.getString("region_name"));

                Photo photo = new Photo(
                        rs.getLong("photo_id"),
                        rs.getString("path"),
                        rs.getDate("date"),
                        region,
                        rs.getInt("views"),
                        rs.getBinaryStream("image"));
                list.add(photo);
            }
            System.out.println(stmt.toString());

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public long add(Photo photo) {
        long id = -1L;
        String sql = "call insertPhoto(?,?,?,?,?,?)";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.registerOutParameter(1, Type.LONG);
            stmt.setString(2, photo.getPath());
            stmt.setDate(3, photo.getDate());
            stmt.setString(4, photo.getRegion().getName());
            stmt.setInt(5, photo.getViews());
            //if (inputStream!=null) {
            stmt.setBlob(6, photo.getImgInputStream());
            //}


            stmt.execute();
            id = stmt.getLong(1);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public boolean update(Photo photo) {
        boolean executeStatus = false;
        //System.out.println("PhotoDAO.java updatePhoto");

        String sql = "call updatePhoto(?,?,?,?,?,?,?)";

        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.registerOutParameter(1, Type.LONG);
            stmt.setLong(2, photo.getId());
            stmt.setString(3, photo.getPath());
            stmt.setDate(4, photo.getDate());
            stmt.setString(5, photo.getRegion().getName());
            stmt.setInt(6, photo.getViews());
            stmt.setBlob(7, photo.getImgInputStream());  // byte[] -> ByteArrayInputStream

            stmt.execute();
            executeStatus = stmt.getBoolean(1);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return executeStatus;
    }

    @Override
    public boolean delete(long id) {
        boolean success = false;

        String sql = "call deletePhoto(?,?)";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setLong(1, id);
            stmt.registerOutParameter(2, Type.LONG);

            stmt.execute();
            success = stmt.getBoolean(2);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }

    @Override
    public boolean delete(Photo photo) {
        return delete(photo.getId());
    }

    public byte[] getPhotoData(long index) {
        Photo photo = getById(index);
        String photoPath = photo.getPath();

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            //BufferedImage img =  ImageIO.read(new File("D:\\13.jpg"));
            BufferedImage img = ImageIO.read(new File(photoPath));
            ImageIO.write(img, "jpg", bos);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return bos.toByteArray();
    }

    public static PhotoDAO getPhotoDAO(HttpSession session) {
        PhotoDAO dao = (PhotoDAO) session.getAttribute(ATTRIBUTE_NAME);
        if (dao == null) {
            dao = new PhotoDAO();
            dao.addToSession(session);
        }

        return (PhotoDAO) session.getAttribute(ATTRIBUTE_NAME);

    }

    public void addToSession(HttpSession session) {
        session.setAttribute(ATTRIBUTE_NAME, this);
    }

}
