package net.javajoy.jss.homework.test;

/**
 * @author Sergey Mikhluk.
 */
public class Sequence {
    public static void main(String[] args) {
        for(int i=1; i<100; i++){
            int x = i^(i-1);

            System.out.println(i + "("+ Integer.toBinaryString(i)  + ") ^ "+ (i-1)+ "("+ Integer.toBinaryString(i-1) + ") = " + x);
        }
    }
}
