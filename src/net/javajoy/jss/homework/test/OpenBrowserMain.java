package net.javajoy.jss.homework.test;

import com.sun.jndi.toolkit.url.Uri;

import java.awt.*;
import java.io.IOException;
import java.net.*;

/**
 * @author Sergey Mikhluk.
 */
public class OpenBrowserMain {
    public static void main(String[] args) {




        URL myURL = null;
        try {
            myURL = new URL("http://loveplanet.ru");
            URLConnection myURLConnection = myURL.openConnection();
            myURLConnection.connect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        try {
//            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + myURL);
//            //Runtime.getRuntime().exec("explorer.exe");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        URI url = null;
        Desktop desktop = Desktop.getDesktop();
        try {
            url = new URI("http://bdsmplaneta.com");
            desktop.browse(url);
            url = new URI("http://bdsmpeople.ru");
            desktop.browse(url);
            url = new URI("http://mysw.info");
            desktop.browse(url);
            url = new URI("http://maw.ru");
            desktop.browse(url);
            url = new URI("http://painart.ru");
            desktop.browse(url);
            url = new URI("http://vinslave.com");
            desktop.browse(url);
            url = new URI("http://spaces.ru");
            desktop.browse(url);




        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
