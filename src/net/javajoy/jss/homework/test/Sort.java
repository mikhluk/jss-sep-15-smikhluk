package net.javajoy.jss.homework.test;

import java.util.Arrays;

/**
 * @author Sergey Mikhluk.
 */
public class Sort {


    public static void main(String[] args) {
        int intArray[] = {100, 15, 10, 121, 4, 5, 17, 1, 255,  11, 7, 3, 0};
        int temp;
        for (int j = 0; j < intArray.length -1; j++) {
            for (int i = 0; i < intArray.length - 1 - j; i++) {
                if (intArray[i] > intArray[i + 1]) {
                    temp = intArray[i + 1];
                    intArray[i + 1] = intArray[i];
                    intArray[i] = temp;
                }
            }
            System.out.println(Arrays.toString(intArray));
        }

    }

}
