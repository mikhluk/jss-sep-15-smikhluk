package net.javajoy.jss.homework.w02;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @author Sergey Mikhluk.
 */
public class DatabaseConnection {

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/";
    // Database credentials
    static final String USER = "root";
    static final String PASS = "123456";

    private static DatabaseConnection instance = null;

    private static Connection conn = null;

    private DatabaseConnection(String dbName) {
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL + dbName, USER, PASS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static Connection getConnection(String dbName) {
        if (instance == null) {
            instance = new DatabaseConnection(dbName);
        }
        return instance.conn;
    }
}
