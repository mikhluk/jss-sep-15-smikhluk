CREATE DEFINER=`root`@`localhost` PROCEDURE `insertPhoto`(IN _path varchar(300), IN _date datetime, IN _region_id int(10), 
				IN _views int(10), IN _name varchar(45), OUT inserted_id int(10) )
BEGIN

	insert into photo(path, date, region_id, views)
    values (_path, _date, _region_id, _views);
    set @photo_id = last_insert_id();
    
    if 0 < (select count(*) from person where person.name = _name) then
		set @person_id = (select id from person where person.name = _name limit 1);
    else
		insert into person(name) values(_name);
        set @person_id = last_insert_id();
    
    end if;
    
    insert into person_photo(person_id, photo_id)
    values (@person_id, @photo_id);
    
    set inserted_id = last_insert_id();
    
END