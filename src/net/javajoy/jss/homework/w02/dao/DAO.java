package net.javajoy.jss.homework.w02.dao;
import net.javajoy.jss.homework.w02.om.Person;

import java.util.List;
import java.util.function.Predicate;

/**
 * @author Sergey Mikhluk.
 */
public interface DAO<T> extends Iterable<T>{
    interface Filter{
        String getSQLCondition();

    }

    void setFilter(int pos, String filter);
    void addSample(String tPhotoID, String tPath, String tDate, String tRegion1, String tName1, String tViews);

    T getByID(long id);
    T getByPosition(int pos);
    List<T> getAsList();
    List<T> getAsList(Predicate<T> predicate);
    long add(T person);
    boolean update(T person);
    boolean delete(T person);
    boolean delete(long id);
    boolean change(long id, String path, int views);
    void refresh();

}
