package net.javajoy.jss.homework.w02.dao;

import net.javajoy.jss.homework.w02.DatabaseConnection;
import net.javajoy.jss.homework.w02.om.Person;
import net.javajoy.jss.homework.w02.om.PersonPhoto;
import net.javajoy.jss.homework.w02.om.Photo;
import net.javajoy.jss.homework.w02.om.Region;
import jdk.internal.org.objectweb.asm.Type;

import java.sql.*;
//import java.sql.Date;
//import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author Sergey Mikhluk.
 */
public class PersonPhotoDAO implements DAO<PersonPhoto> {

    static final String DB_NAME = "homephoto";
    public static final int FILTERS_QUANTITY = 4;


    LinkedHashMap<Long, PersonPhoto> data = new LinkedHashMap<>(100);
    Connection conn = null;
    PreparedStatement stmt = null;
    String[] filter = new String[FILTERS_QUANTITY];

    @Override
    public PersonPhoto getByID(long id) {
        return data.get(id);
    }

    @Override
    public PersonPhoto getByPosition(int pos) {
        if (pos >= 0 && pos <= data.size()) {
            return (PersonPhoto) data.values().toArray()[pos];
        } else return null;
    }

    @Override
    public List<PersonPhoto> getAsList() {
        List list = new ArrayList(data.size());
        data.values().forEach(o -> list.add(o));
        return list;
    }

    @Override
    public List<PersonPhoto> getAsList(Predicate<PersonPhoto> predicate) {
        List list = new ArrayList(data.size());
       // data.values().stream().filter(predicate).forEach(o -> list.add(o));
        return list;
    }

    @Override
    public long add(PersonPhoto personPhoto) {
        Connection conn = DatabaseConnection.getConnection(DB_NAME);
        long id = -1;
        String sql = "call insertPhoto(?,?,?,?,?,?)";
        try (CallableStatement stmt = conn.prepareCall(sql)){

            stmt.setString(1, personPhoto.getPhoto().getPath());
            stmt.setDate(2, personPhoto.getPhoto().getDate());
            stmt.setString(3, personPhoto.getPhoto().getRegion().getName());
            stmt.setInt(4, personPhoto.getPhoto().getViews());
            stmt.setString(5, personPhoto.getPerson().getName());
            stmt.registerOutParameter(6, Type.LONG);

            stmt.execute();

            id = stmt.getLong(6);

        }catch (SQLException e){
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public void addSample(String tPhotoID, String tPath, String tDate, String tRegion1, String tName1, String tViews){
        System.out.println("add sample");
        //Photo photo = new Photo("D:\\path\\newname",new Date(1000), 2, new Region("Odessa"), 100);


//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
//        System.out.println("date: " + dateFormat.format( new Date() ) );

        try {
            tDate = "2015-10-15";
            java.util.Date date_utl = new SimpleDateFormat("yyyy-MM-dd").parse(tDate);
            java.sql.Date dat_sql = new java.sql.Date(date_utl.getTime());
            Photo photo = new Photo(tPath, dat_sql, new Region(tRegion1), new Integer(tViews));
            PersonPhoto personPhoto = new PersonPhoto(new Person(tName1), photo);
            add(personPhoto);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //Photo photo = new Photo(tPath,new Date(1000), new Region(tRegion1), new Integer(tViews));


    }

    @Override
    public boolean update(PersonPhoto person) {
        return false;
    }

    @Override
    public boolean delete(long id) {

        Connection conn = DatabaseConnection.getConnection(DB_NAME);
        boolean success  = false;
        String sql = "call deletePhoto(?,?)";
        try (CallableStatement stmt = conn.prepareCall(sql)){

            stmt.setLong(1, id);
            stmt.registerOutParameter(2, Type.LONG);

            stmt.execute();

            success = stmt.getBoolean(2);

        }catch (SQLException e){
            e.printStackTrace();
        }
        return success;

    }

    @Override
    public boolean delete(PersonPhoto person) {
        return false;
    }

    @Override
    public Iterator<PersonPhoto> iterator() {
        return data.values().iterator();
    }

    @Override
    public void setFilter(int pos, String filter) {
        this.filter[pos] = filter;
    }

    @Override
    public boolean change(long id, String path, int views) {
        Connection conn = DatabaseConnection.getConnection(DB_NAME);
        boolean success = false;

        //try (Statement stmt = conn.createStatement()){
        try (PreparedStatement stmt = conn.prepareStatement(
                "SELECT * FROM photo",
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {

                if (rs.getLong("id") == id) {
                    rs.updateString("path", path);
                    rs.updateInt("views", views);
                    rs.updateRow();
                    success = true;
                    break;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;

    }

    @Override
    public void refresh() {
        Connection conn = DatabaseConnection.getConnection(DB_NAME);

        //try (Statement stmt = conn.createStatement()){
        try (PreparedStatement stmt = conn.prepareStatement(
                "SELECT * FROM view_person_photo " +
                        "WHERE (date BETWEEN  ? AND ? ) AND (name LIKE ?) AND (region_name LIKE ?) ",
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
            stmt.setString(1, filter[0]);
            //stmt.setString(1, "*");
            stmt.setString(2, filter[1]);
            stmt.setString(3, filter[2]+"%");
            stmt.setString(4, filter[3]+"%");

            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                Person person = new Person(rs.getString("name"));

                Region region = new Region(rs.getString("region_name"));

                Photo photo = new Photo(
                        rs.getLong("photo_id"),
                        rs.getString("path"),
                        rs.getDate("date"),
                        rs.getLong("region_id"),
                        region,
                        rs.getInt("views"));

                PersonPhoto personPhoto = new PersonPhoto(
                        rs.getLong("pfid"), person, photo);
                data.put(personPhoto.getId(), personPhoto);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }


    }
}
