package net.javajoy.jss.homework.w02;

import net.javajoy.jss.homework.w02.dao.DAO;
import net.javajoy.jss.homework.w02.dao.PersonPhotoDAO;
import net.javajoy.jss.homework.w02.om.PersonPhoto;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

/**
 * @author Sergey Mikhluk.
 */
public class HomePhotoTableView {
    public static final int COLUMNS_QUANTITY = 7;
    private JButton buttonRefresh;
    private JTable tableData;
    private JPanel mainPanel;
    private JTextField tDateFrom;
    private JTextField tDateTo;
    private JTextField tName;
    private JTextField tRegion;
    private JButton buttonAdd;
    private JButton buttonDelete;
    private JTextField tPath;
    private JTextField tDate;
    private JTextField tRegion1;
    private JTextField tName1;
    private JTextField tViews;
    private JTextField tPhotoID;
    private JButton buttonChange;
    private static JFrame frame;
    private DefaultTableModel tableModel = null;

    // JDBC driver name and database URL
    // Database credentials
    static final String INITIAL_DATE_FROM = "1970-01-01";
    static final String INITIAL_DATE_TO = "2015-12-01";

    private static Connection conn = null;

    public HomePhotoTableView() {
        tDateFrom.setText(INITIAL_DATE_FROM);
        tDateTo.setText(INITIAL_DATE_TO);

//        try {
//            conn = DatabaseConnection.getConnection("homephoto");
//        } catch (Exception e) {
//            e.printStackTrace();
//            frame.dispose();
//        }

        buttonRefresh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Refresh pressed!!");
                refreshData();
            }
        });

        buttonAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("button add pressed!!");
                DAO<PersonPhoto> personPhotoDAO = new PersonPhotoDAO();
                personPhotoDAO.addSample(tPhotoID.getText(), tPath.getText(), tDate.getText(),
                        tRegion1.getText(), tName1.getText(), tViews.getText());

            }
        });

        buttonChange.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DAO<PersonPhoto> personPhotoDAO = new PersonPhotoDAO();
                System.out.println("Change pressed!!");

                boolean success = personPhotoDAO.change(new Long(tPhotoID.getText()), tPath.getText(), new Integer(tViews.getText()));

                if (success) {
                    System.out.println("Changed photo with ID = " + tPhotoID.getText());
                } else {
                    System.out.println("Photo with ID = " + tPhotoID.getText() + " doesn't found!");
                }
                ;
            }
        });


        buttonDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DAO<PersonPhoto> personPhotoDAO = new PersonPhotoDAO();
                System.out.println("Delete pressed!!");

                boolean success = personPhotoDAO.delete(new Long(tPhotoID.getText()));

                if (success) {
                    System.out.println("Deleted photo with ID = " + tPhotoID.getText());
                } else {
                    System.out.println("Photo with ID = " + tPhotoID.getText() + " doesn't found!");
                }
                ;
            }
        });

    }


    private void refreshData() {
        DAO<PersonPhoto> personPhotoDAO = new PersonPhotoDAO();

        if (tDateFrom.getText().equals("")) {
            // tDateFrom.setText(INITIAL_DATE_FROM);
        }
        personPhotoDAO.setFilter(0, tDateFrom.getText());

        if (tDateTo.getText().equals("")) {
            // tDateTo.setText(INITIAL_DATE_TO);
        }
        personPhotoDAO.setFilter(1, tDateTo.getText());

        if (!tName.getText().equals("")) {
            personPhotoDAO.setFilter(2, tName.getText());
        } else {
            personPhotoDAO.setFilter(2, "");
        }

        if (!tRegion.getText().equals("")) {
            personPhotoDAO.setFilter(3, tRegion.getText());
        } else {
            personPhotoDAO.setFilter(3, "");
        }

        personPhotoDAO.refresh();
        java.util.List<PersonPhoto> list = personPhotoDAO.getAsList();

        tableModel = new DefaultTableModel(0, 0);
        tableModel.addColumn("pfid");
        tableModel.addColumn("photo_id");
        tableModel.addColumn("path");
        tableModel.addColumn("region");
        tableModel.addColumn("date");
        tableModel.addColumn("person");
        tableModel.addColumn("views");
        String[] row = new String[COLUMNS_QUANTITY];

        for (PersonPhoto o : personPhotoDAO) {
            //System.out.println(o.getId() + " \t" + o.getPhoto().getPath() + " \t" + o.getPerson().getName());
            row[0] = Long.toString(o.getId());
            row[1] = Long.toString(o.getPhoto().getId());
            row[2] = o.getPhoto().getPath();
            //row[2] = Long.toString(o.getPhoto().getRegionId());
            row[3] = o.getPhoto().getRegion().getName();
            row[4] = o.getPhoto().getDate().toString();
            row[5] = o.getPerson().getName();
            row[6] = Integer.toString(o.getPhoto().getViews());

            tableModel.addRow(row);
        }

        tableData.setModel(tableModel);
    }

    static void printResultSet(ResultSet rs) throws SQLException {
        ResultSetMetaData meta = rs.getMetaData();
        int columnCount = meta.getColumnCount();
        String[] row = new String[columnCount];
        for (int i = 1; i <= columnCount; i++) {
            System.out.print(meta.getColumnName(i) + "\t");
        }
        System.out.println();
        while (rs.next()) {
            for (int i = 1; i <= columnCount; i++) {
                System.out.print(rs.getString(i) + "\t");
            }
            System.out.println();
        }
    }


    public static void main(String[] args) {

        frame = new JFrame("JDBCTableView");
        frame.setContentPane(new HomePhotoTableView().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        mainPanel.setMaximumSize(new Dimension(1100, 400));
        mainPanel.setMinimumSize(new Dimension(1100, 400));
        mainPanel.setPreferredSize(new Dimension(1100, 400));
        buttonRefresh = new JButton();
        buttonRefresh.setText("Refresh");
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        mainPanel.add(buttonRefresh, gbc);
        final JPanel spacer1 = new JPanel();
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        mainPanel.add(spacer1, gbc);
        final JScrollPane scrollPane1 = new JScrollPane();
        scrollPane1.setMaximumSize(new Dimension(800, 300));
        scrollPane1.setMinimumSize(new Dimension(800, 300));
        scrollPane1.setPreferredSize(new Dimension(800, 300));
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.fill = GridBagConstraints.BOTH;
        mainPanel.add(scrollPane1, gbc);
        tableData = new JTable();
        tableData.setMaximumSize(new Dimension(800, 300));
        tableData.setMinimumSize(new Dimension(800, 300));
        tableData.setPreferredScrollableViewportSize(new Dimension(800, 400));
        tableData.setPreferredSize(new Dimension(800, 300));
        scrollPane1.setViewportView(tableData);
        final JPanel spacer2 = new JPanel();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.fill = GridBagConstraints.VERTICAL;
        mainPanel.add(spacer2, gbc);
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridBagLayout());
        panel1.setMaximumSize(new Dimension(300, 250));
        panel1.setMinimumSize(new Dimension(300, 350));
        panel1.setPreferredSize(new Dimension(300, 250));
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.insets = new Insets(6, 3, 6, 3);
        mainPanel.add(panel1, gbc);
        panel1.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(-16777216)), null));
        final JPanel spacer3 = new JPanel();
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(spacer3, gbc);
        tDateFrom = new JTextField();
        tDateFrom.setPreferredSize(new Dimension(80, 24));
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(tDateFrom, gbc);
        tDateTo = new JTextField();
        tDateTo.setPreferredSize(new Dimension(80, 24));
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(tDateTo, gbc);
        final JLabel label1 = new JLabel();
        label1.setText("Date From:");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label1, gbc);
        final JLabel label2 = new JLabel();
        label2.setText("Date To:");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label2, gbc);
        final JLabel label3 = new JLabel();
        label3.setText("Name:");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label3, gbc);
        final JLabel label4 = new JLabel();
        label4.setText("Region:");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label4, gbc);
        final JLabel label5 = new JLabel();
        label5.setForeground(new Color(-65536));
        label5.setText("Filters:");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label5, gbc);
        final JLabel label6 = new JLabel();
        label6.setForeground(new Color(-65536));
        label6.setText("Change section:");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(15, 0, 0, 0);
        panel1.add(label6, gbc);
        final JLabel label7 = new JLabel();
        label7.setText("Path:");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 7;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label7, gbc);
        buttonDelete = new JButton();
        buttonDelete.setText("Delete");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 13;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(buttonDelete, gbc);
        tViews = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 12;
        gbc.gridwidth = 3;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(tViews, gbc);
        tName1 = new JTextField();
        tName1.setText("");
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 11;
        gbc.gridwidth = 3;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(tName1, gbc);
        tRegion1 = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 10;
        gbc.gridwidth = 3;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(tRegion1, gbc);
        tDate = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 9;
        gbc.gridwidth = 3;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(tDate, gbc);
        buttonAdd = new JButton();
        buttonAdd.setText("Add");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 13;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(buttonAdd, gbc);
        final JLabel label8 = new JLabel();
        label8.setText("Views:");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 12;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label8, gbc);
        final JLabel label9 = new JLabel();
        label9.setText("Name:");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 11;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label9, gbc);
        final JLabel label10 = new JLabel();
        label10.setText("Region:");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 10;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label10, gbc);
        tPath = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 8;
        gbc.gridwidth = 4;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(tPath, gbc);
        final JLabel label11 = new JLabel();
        label11.setText("Photo id:");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label11, gbc);
        final JLabel label12 = new JLabel();
        label12.setText("Date:");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 9;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label12, gbc);
        tPhotoID = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 6;
        gbc.gridwidth = 3;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(tPhotoID, gbc);
        tName = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.gridwidth = 3;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(tName, gbc);
        tRegion = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.gridwidth = 3;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(tRegion, gbc);
        buttonChange = new JButton();
        buttonChange.setText("Change");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 14;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(buttonChange, gbc);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }
}



