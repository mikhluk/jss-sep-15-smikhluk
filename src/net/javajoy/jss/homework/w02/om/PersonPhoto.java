package net.javajoy.jss.homework.w02.om;

/**
 * @author Sergey Mikhluk.
 */
public class PersonPhoto {

    private long id = -1;
    private Person person = null;
    private Photo photo = null;

    public PersonPhoto(Person person, Photo photo) {
        this.id = - 1;
        this.person = person;
        this.photo = photo;
    }
    public PersonPhoto(long id, Person person, Photo photo) {
        this.id = id;
        this.person = person;
        this.photo = photo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }
}
