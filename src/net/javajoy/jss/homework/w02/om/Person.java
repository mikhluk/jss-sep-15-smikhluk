package net.javajoy.jss.homework.w02.om;

/**
 * @author Sergey Mikhluk.
 */
public class Person {
    private long id = -1;
    private String name = "";

    public Person(String name) {
        this.id = id = -1;
        this.name = name;
    }

    public Person(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
