package net.javajoy.jss.homework.w02;

import net.javajoy.jss.homework.w02.dao.DAO;
import net.javajoy.jss.homework.w02.dao.PersonPhotoDAO;
import net.javajoy.jss.homework.w02.om.PersonPhoto;

import java.util.List;

/**
 * @author Sergey Mikhluk.
 */
public class Demo {
    public static void main(String[] args) {
        DAO<PersonPhoto> personPhotoDAO = new PersonPhotoDAO();
        personPhotoDAO.refresh();
        List<PersonPhoto> list = personPhotoDAO.getAsList();
        for( PersonPhoto o : personPhotoDAO ) {
            System.out.println( o.getId() + " \t" + o.getPhoto().getPath() + " \t" + o.getPerson().getName());
        }
    }
}
