package net.javajoy.jss.homework.w05_test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * @author Sergey Mikhluk.
 */
@WebServlet(name = "CalendarServlet")
public class CalendarServlet extends HttpServlet {
    public static final int MONTH = Calendar.NOVEMBER;
    public static final int YEAR = 2015;
    public ArrayList holidays;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int countVisits =0;

//        try {
//            countVisits =  new Integer(request.getParameter("count_visits"));  //MS:TODO пока что не работает, не придумал как сделать
//            System.out.println(countVisits );
//        }catch (NumberFormatException e) {
//            System.err.println("Неверный формат строки!");
//        }

        // CK : The version of hit counter using session attribute
        // todo : try other storage places of counter : servlet class variable, context attribute, cookie
        HttpSession session = request.getSession(true);
        int hitCounter = 0;
        if (!session.isNew()) {
            hitCounter = (Integer) session.getAttribute("hitCounter");
        }
        session.setAttribute("hitCounter",Integer.valueOf(++hitCounter));
        System.out.format("Session ID = %s; \t Hit count =%d %n", session.getId(), hitCounter);


        PrintWriter out = receivePrintWriter(request, response);

        int intYear = receiveIntYear(request);      // todo: replace  receiveIntYear()  and receiveIntMonth() with a single method: getIntParameter(request,name)
        int intMonth = receiveIntMonth(request);
        Calendar calendar = Calendar.getInstance();
        calendar.set(intYear, intMonth - 1, 1);


        createHolidaysList();



        printTop(intYear, intMonth, out);
        printTable(calendar, out);
        printBottom(out);

        countVisits++;
        request.setAttribute("count_visits", countVisits);   // this request object is destroyed after doGet() finishes
        // it cannot store or transfer data BETWEEN requests

        out.close();
    }

    private void createHolidaysList() {
        holidays = new ArrayList();
        holidays.add("1.1");
        holidays.add("1.7");
        holidays.add("3.8");
        holidays.add("5.1");
        holidays.add("5.2");
        holidays.add("5.9");
        holidays.add("5.31");
        holidays.add("6.28");
        holidays.add("8.24");
        holidays.add("10.14");
    }

    private int receiveIntMonth(HttpServletRequest request) {
        String month = request.getParameter("userMonth");
        int intMonth;
        intMonth = (month != null && month.length() != 0) ? new Integer(month) : MONTH;
        return intMonth;
    }

    private int receiveIntYear(HttpServletRequest request) {
        String year = request.getParameter("userYear");
        int intYear;
        intYear = (year != null && year.length() != 0) ? new Integer(year) : YEAR;
        return intYear;
    }

    private PrintWriter receivePrintWriter(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        return response.getWriter();
    }

    private void printTop(int intYear, int intMonth, PrintWriter out) {
        out.println("<html>" +
                "<head>" +
                "<title> Calendar </title>" +
                "</head>" +
                "<body bgcolor = '#eedddd'>" +
                "<h2> Год: " + intYear + "</h2> <p>" +
                "<h2> Месяц: " + intMonth + "</h2> <p>");
    }

    private void printTable(Calendar calendar, PrintWriter out) {
        out.println("<table width = 300 border = 1 style = border-collapse:collapse >");

        out.println("<tr align = center bgcolor = #ccc>");
        out.println("<td> пн </td> <td>вт</td> <td>ср</td> <td>чт</td> <td>пт</td> <td>сб</td> <td>вс</td>");
        out.println("</tr>");

        int firsDayOfWeek = (calendar.get(Calendar.DAY_OF_WEEK) + 5) % 7 + 1; //день недели первого числа месяца
        int count = 0;

        String printDate = "";
        String colorDate = "";
        for (int i = 1; i <= 6; i++) {
            out.println("<tr align = center>");
            for (int j = 1; j <= 7; j++) {
                count++;
                if ((count - firsDayOfWeek + 1 < 1)
                        ||(count - firsDayOfWeek + 1 > calendar.getActualMaximum(Calendar.DAY_OF_MONTH)))  {
                    printDate = "";
                }
                else {
                    printDate = Integer.toString(count - firsDayOfWeek + 1);
                }

                colorDate = (j>=6 && j<=7) ? "red" : "black";

                String param = (calendar.get(Calendar.MONTH) + 1) + "." + printDate;
                if (holidays.contains(param)) {
                    colorDate = "red";
                }

                out.println("<td> <font color = " + colorDate + ">"+ printDate + "</font></td>");
            }
            out.println("</tr>");
        }
        out.println("</table>");

//        for (int i = 1; i <= calendar.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
//            calendar.set(intYear, intMonth - 1, i);
//            out.println(i + " - " + String.format("%1$tA", calendar) + " , <p>");
//        }
    }

    private void printBottom(PrintWriter out) {
        out.println("</body>" +
                "</html>");
    }

}
