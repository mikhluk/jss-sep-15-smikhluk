package net.javajoy.jss.homework.w05_test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

/**
 * @author Sergey Mikhluk.
 */
@WebServlet(name = "ClockServlet")
public class ClockServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html; charset = UTF-8");
        PrintWriter out = new PrintWriter(response.getWriter());
        out.println(new Date());
        out.close();

    }
}
