package net.javajoy.jss.homework.w05_test;

import java.util.Calendar;

/**
 * @author Sergey Mikhluk.
 */
public class TestCalendar1 {

    public static final int MONTH = Calendar.OCTOBER;
    public static final int YEAR = 2015;

    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        System.out.println("Year: " + YEAR + ", Month: " + MONTH);

        for(int i = 1; i<= calendar.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            calendar.set(YEAR, MONTH, i);

            System.out.println(i + " " + String.format("%1$tA", calendar));
            System.out.println((calendar.get(Calendar.DAY_OF_WEEK) + 5)%7+1);
        }

        //int dow = c.get(Calendar.DAY_OF_WEEK);
        //System.out.println (new SimpleDateFormat( "EEEE" ).format ( new Date() )) ;
    }
}
