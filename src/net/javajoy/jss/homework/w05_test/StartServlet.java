package net.javajoy.jss.homework.w05_test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Sergey Mikhluk.
 */
@WebServlet(name = "StartServlet")
public class StartServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html; charset = UTF-8");
        PrintWriter out = new PrintWriter(response.getWriter());
        // CK : to show this static form servlet is not needed
        out.println("<html>" +                  // todo : for static pages use plain html
                        "<head>" +
                        "<title> Test Calendar start page</title>" +

                        "</head>" +
                        "<body bgcolor = '#00eedd'>" +
                        "<form name = Form1 method = POST action = http://localhost:8080/jss-sep-15-smikhluk/cal>" +
                        "Enter year:      " +
                        "<input type = text name = userYear><p>" +
                        "Enter month:      " +
                        "<input type = text name = userMonth><p>" +
                        "<input type = submit value = \"Send\">" +
                        "</form>"
        );

        out.println("</body>" +
                "</html>");

        out.close();
    }
}
