package net.javajoy.jss.homework.w05_test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Sergey Mikhluk.
 */
@WebServlet(name = "ProcessMessage")
public class ProcessMessage extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");

        String msg = request.getParameter("userData");

       if (msg != null && msg.length() !=0 ) {
            PrintWriter out = response.getWriter();

            out.println("<html>" +
                            "<head>" +
                            "<title> Process Message </title>" +

                            "</head>" +
                            "<body bgcolor = '#eedddd'>" +
                    "<h2> Received Message "+ msg +

                            "</body>" +
                            "</html>");


            out.close();
        }else {
            response.sendRedirect("http://localhost:8080/jss-sep-15-smikhluk/clock");
       }



    }
}
