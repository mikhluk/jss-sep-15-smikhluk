package net.javajoy.jss.homework.w05_test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

/**
 * @author Sergey Mikhluk.
 */
public class TestServlet extends javax.servlet.http.HttpServlet {
    public static final int MONTH = Calendar.NOVEMBER;
    public static final int YEAR = 2015;
    private static  int count = 0;

    @Override
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
    }

    @Override
    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        response.setContentType("text/html; charset = UTF-8");
        PrintWriter  out = new PrintWriter(response.getWriter());
        Calendar calendar = Calendar.getInstance();

        out.println("<html>" +
                "<head>" +
                "<title> Test servlet 11</title>" +

                "</head>" +
                "<body bgcolor = '#ddeedd'>" +
                        "<form name = Form1 method = POST action = http://localhost:8080/jss-sep-15-smikhluk/pm>"+
                        "Enter your message:      "+
                        "<input type = text name = userData><p>"+
                        "<input type = submit value = \"Send\">"+
                        "</form>"+
                        "Year: " + YEAR + ", Month: " + (MONTH +1) +".<br>"
                );



        for(int i = 1; i<= calendar.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            calendar.set(YEAR, MONTH, i);
            out.println(i + " - " + String.format("%1$tA", calendar) +" , <br>");
        }

        out.println("</body>" +
                "</html>");
        count++;
        request.setAttribute("current_count", count);
        //out.println(new SimpleDateFormat("EEEE").format(new Date().toString()));

    }
}
