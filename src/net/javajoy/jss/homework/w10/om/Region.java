package net.javajoy.jss.homework.w10.om;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Sergey Mikhluk.
 */

@XmlRootElement(name = "region")
public class Region {
    private long id  = -1;
    private String name = "";

    public Region() {
    }

    public Region(String name) {
        this.id = -1;
        this.name = name;
    }
    public Region(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @XmlAttribute
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @XmlAttribute
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
