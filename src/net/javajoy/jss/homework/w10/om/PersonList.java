package net.javajoy.jss.homework.w10.om;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Mikhluk.
 */
@XmlRootElement
public class PersonList {
    List<Person> personList;

    public PersonList() {
    }

    public PersonList(List<Person> personList) {
        this.personList = personList;
    }

    @XmlElement(name="person")
    public List<Person> getPersonList() {
        if (personList == null) {
            personList = new ArrayList<Person>(10);
        }
        return personList;
    }

    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }
}
