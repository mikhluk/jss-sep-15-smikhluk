package net.javajoy.jss.homework.w10.om;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Mikhluk.
 */
@XmlRootElement
public class PhotoList {
    List<Photo> photoList;

    public PhotoList() {
    }

    public PhotoList(List<Photo> photoList) {
        this.photoList = photoList;
    }

    @XmlElement(name="photo")
    public List<Photo> getPhotoList() {
        System.out.println("PhotoList.java getPhotoList()");
        if (photoList == null) {
            photoList = new ArrayList<Photo>(10);
        }
        return photoList;
    }

    public void setPhotoList(List<Photo> photoList) {
        this.photoList = photoList;
    }
}
