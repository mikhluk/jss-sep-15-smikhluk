package net.javajoy.jss.homework.w10.om;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Sergey Mikhluk.
 */


@XmlRootElement(name = "photo")
public class Photo {
    private long id = -1;
    private String path = "";
    private Date date = null;
    //private long regionID = 0;
    private Region region = null;
    private int views = 0;
    private byte imageData[];

    private void setDataFromInputStream(InputStream imgInputStream) throws IOException {
        if (imgInputStream == null) {
            imageData = null;
            return;
        }

        byte[] buf = new byte[1024 * 4];
        ByteArrayOutputStream bos = new ByteArrayOutputStream(1024 * 1024);
        while (imgInputStream.read(buf) > 0) {
            bos.write(buf);
        }
        imageData = bos.toByteArray();
    }

    //MS:TODO getDataInputStream используется в PhotoDAO, но в PhotoCardServlet он не доступен
    //как лучше чделать для единообразия? Сейчас получается некое дублирование кода.

    // Полного единообразия не получится, потому что клиент работет не с этим классом, а с прокси-классом, созданным
    // на базе xml-представления фото
    // В этом xml-представлении не может быть атрибута типа InputStream, может быть только типа byte[].
    // В этом плане сейчас сделано правильно.
    // Дальше на стороне клиента можно сделать некий класс-обертку над Photo,
    // который бы добавлял недостающую функциональность типа создания InputStream.
    // Но реально это делается редко, потому что классы @XmlElement изначально не предназначены для реализации какой-либо
    // функциональности, а только лишь для представления данных в xml.

    public InputStream getDataInputStream() {
        return new ByteArrayInputStream(imageData);
    }

    public Photo(){
    }

    public Photo(long id, String path, Date date, Region region, int views, InputStream imgInputStream) { //конструктор используется при чтениии базу
        this.id = id;
        this.path = path;
        this.date = date;
        //  this.regionID = regionID;
        this.region = region;
        this.views = views;

        try {
            setDataFromInputStream(imgInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Photo(String tId, String tPath, String tDate, String tRegion, String tViews, byte[] imageData) { //конструктор используется при записи в базу
        try {
            this.id = new Long(tId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        this.path = tPath;

        try {
            Date date_utl = new SimpleDateFormat("yyyy-MM-dd").parse(tDate);
            Date dat_sql = new Date(date_utl.getTime());
            this.date = dat_sql;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.region = new Region(tRegion);

        try {
            if (tViews == null || tViews.equals("")) {
                this.views = 0;
            } else {
                this.views = Integer.valueOf(tViews);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        this.imageData = imageData;
    }

    @XmlAttribute
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @XmlAttribute
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @XmlAttribute
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @XmlAttribute
    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    //@XmlAttribute
    @XmlElement(name = "region", required = false)
    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @XmlAttribute
    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }


}
