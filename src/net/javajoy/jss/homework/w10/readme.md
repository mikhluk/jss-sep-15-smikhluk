Webinar 10 - Home Task
1. Для созданного на предыдущих занятиях web-приложения реализуйте бизнес-логику
(доступ к основным объектам БД и операции с ними) с помощью служб REST. 
Протестируйте работу служб с помощью REST Client и SoapUI .

2. Создайте простого потребителя этих служб (не обязательно в виде веб-модуля),
который содержит код для обращения к ресурсам сервера:
- получение в виде списка,
- добавление,
- удаление,
- изменение.


http://localhost:8080/jss-sep-15-smikhluk/resources/services
http://localhost:8080/jss-sep-15-smikhluk/resources/rs?_wadl

http://localhost:8080/jss-sep-15-smikhluk/resources/rs/persons   - список всех person
http://localhost:8080/jss-sep-15-smikhluk/resources/rs/persons/90 - список person на фото с id=90
http://localhost:8080/jss-sep-15-smikhluk/resources/rs/person/get/1   - вывод person c id=1

Добавление person проиходит на фото, а не просто так. два параметра photoID, personId
Добавление person проиходит с фото, а не просто так. два параметра photoID, personId


http://localhost:8080/jss-sep-15-smikhluk/resources/rs/photos/  - список всех photo
http://localhost:8080/jss-sep-15-smikhluk/resources/rs/photo/get/90

http://localhost:8080/jss-sep-15-smikhluk/resources/rs/photo/image/90 - изображение



Полезные ссылки

http://www.ibm.com/developerworks/ru/library/wa-jaxrs/
https://docs.oracle.com/javaee/6/tutorial/doc/gilik.html
https://docs.oracle.com/javaee/6/tutorial/doc/gjjxe.html


http://cxf.apache.org/docs/jax-rs-multiparts.html   JAX-RS : Support for Multiparts
http://cxf.apache.org/docs/jax-rs-basics.html       JAX-RS : Understanding the Basics

http://www.javatips.net/blog/2013/05/cxf-rest-file-upload?page=2  CXF REST File Upload

http://www.programcreek.com/java-api-examples/index.php?api=javax.ws.rs.core.Request
https://examples.javacodegeeks.com/enterprise-java/rest/jax-rs-formparam-example/
http://stackoverflow.com/questions/9612194/get-httpservletrequest-in-jax-rs-appfuse-application
http://stackoverflow.com/questions/14065257/how-to-get-only-part-of-url-from-httpservletrequest


Гонсалвес Изучаем java ee

