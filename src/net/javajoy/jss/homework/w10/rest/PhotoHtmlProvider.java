package net.javajoy.jss.homework.w10.rest;

import net.javajoy.jss.homework.w10.bean.Filter;
import net.javajoy.jss.homework.w10.dao.PersonDAO;
import net.javajoy.jss.homework.w10.om.Photo;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * @author Sergey Mikhluk.
 */
@Provider
@Produces("text/html")
public class PhotoHtmlProvider implements MessageBodyWriter<Photo> {
    @Override
    public boolean isWriteable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return true;
    }

    @Override
    public long getSize(Photo photo, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return 0;
    }

    @Override
    public void writeTo(Photo photo, Class<?> aClass, Type type, Annotation[] annotations,
                        MediaType mediaType, MultivaluedMap<String, Object> multivaluedMap,
                        OutputStream outputStream) throws IOException, WebApplicationException {

        System.out.println("PhotoHtmlProvider.writeTo()");

        PersonDAO personDAO = new PersonDAO();
        Filter filter = new Filter();

        if (photo != null) {
            filter.setValue(Filter.FilterConstant.PHOTO_ID, String.valueOf(photo.getId()));
        }

        PrintWriter writer = new PrintWriter(outputStream);
        writer.print("<html>");
        writer.print("<a href='" + WebUtils.CONTEXT_PATH + "/resources/rs/photos/'> back to photo list</a>");
        writer.print("<h2> Photo </h2>");
        WebUtils.printPhoto(photo, writer);

        //MS: Сделал метод для вызова из PhotoHtmlProvider и PersonListHtmlProvider, чтобы не было дублирования
        // CK : это хорошее решение
        WebUtils.printPersonList(personDAO.getAsList(filter), writer, "Person list:");


//MS:TODO попробовать избавиться от захардкордженого  контекста /jss-sep-15-smikhluk
        // CK: относительный путь указать нельзя, но ты можешь не хардкодить имя контекста
        // Например, в сервис-методах ты можешь сделать параметр типа @Context HttpServletRequest,
        // а из него получить getContextPath
        // Сюда же url картинки передать можно как свойство Photo

        writer.print("<h3> Add Person on photo</h3>");
        //writer.print("<form action=\"/jss-sep-15-smikhluk/photoDBAction\" method=\"post\" accept-charset=\"UTF-8\" enctype=\"multipart/form-data\">");
        writer.print("<form action='" + WebUtils.CONTEXT_PATH + "/resources/rs/person/add' method='post' accept-charset='UTF-8'>");
        writer.print("<table border=1 cellspacing=0 width=300px>");
        writer.print("<tr>");
        writer.print("<th>photo_id</th>");
        writer.print("<th>person_name</th>");
        writer.print("<tr>");
        writer.print("<td><input type='text' name='photoId' value=" + photo.getId() +"></td>");
        writer.print("<td><input type='text' name='personName' value='Arnold'></td>");
        writer.print("</tr>");
        writer.print("</table>");
        writer.print("<p/>");
        writer.print("<input type=\"submit\" value=\"Add person\">");
        writer.print("<input type=\"reset\" value=\"Cancel\">");
        writer.print("</form>");

        writer.println("</html>");
        writer.close();   // обязательно закрыть поток
    }
}
