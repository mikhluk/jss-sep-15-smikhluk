package net.javajoy.jss.homework.w10.rest;

import net.javajoy.jss.homework.w10.bean.Filter;
import net.javajoy.jss.homework.w10.dao.PhotoDAO;
import net.javajoy.jss.homework.w10.om.Photo;
import net.javajoy.jss.homework.w10.om.PhotoList;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author Sergey Mikhluk.
 */

@Path("photos")
public class PhotoListResource {

    @GET
    @Produces({"application/xml", "application/json", "text/html"})
    @Consumes("*/*")
    public Response getPhotos() {
        return getPhotosHelper();
    }

    @POST  // форма передает данные обычно через POST
    @Produces({"application/xml", "application/json"})
    @Consumes("application/x-www-form-urlencoded")
    public Response getPersonsForm() {
        return getPhotosHelper();
    }

    protected Response getPhotosHelper() {
        System.out.println("PhotoListResource");
        Filter filter = new Filter();
        List<Photo> photoList = null;
        PhotoDAO photoDAO = new PhotoDAO();

        try {
            photoList = photoDAO.getAsList(filter);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (photoList == null || photoList.isEmpty()) {
            //Construct error respose
            return Response.status(Response.Status.BAD_REQUEST).entity("Empty List").build();

        } else {
            //Construct response with PersonList
            Response.ResponseBuilder rb = Response.ok(new PhotoList(photoList));

            Response response = rb.build();   //вызывает PhotoList.getPhotoList()

            return response;
        }
    }
}
