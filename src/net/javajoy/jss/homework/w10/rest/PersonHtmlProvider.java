package net.javajoy.jss.homework.w10.rest;

import net.javajoy.jss.homework.w10.om.Person;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * @author Sergey Mikhluk.
 */
@Provider
@Produces("text/html")
public class PersonHtmlProvider implements MessageBodyWriter<Person> {
    @Override
    public boolean isWriteable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return true;
    }

    @Override
    public long getSize(Person person, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return 0;
    }

    @Override
    public void writeTo(Person person, Class<?> aClass, Type type, Annotation[] annotations,
                        MediaType mediaType, MultivaluedMap<String, Object> multivaluedMap,
                        OutputStream outputStream) throws IOException, WebApplicationException {

        PrintWriter writer = new PrintWriter(outputStream);
        writer.println("<html><h3> Person </h3> <br>" +
                "ID = " + person.getId() +
                "<br> Name = " + person.getName() + "</html>");
        writer.close();   // обязательно закрыть поток
    }
}
