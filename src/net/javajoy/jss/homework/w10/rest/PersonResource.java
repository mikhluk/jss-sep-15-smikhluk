package net.javajoy.jss.homework.w10.rest;

import net.javajoy.jss.homework.w10.dao.PersonDAO;
import net.javajoy.jss.homework.w10.om.Person;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Date;

/**
 * @author Sergey Mikhluk.
 */
@Path("/person")
public class PersonResource {
    @Context
    private HttpServletRequest servletRequest;

    @GET
    @Path("/get/{id}")
    @Produces({"application/xml", "application/json", "text/html"})
    @Consumes("*/*")
    // MS: а можно ли метод с двумя параметрами вызвать из строки браузера, а не через SoapUI
    //CK: Path-параметров может быть несколько, например @Path("dosmth/{prm1}/{prm2}/fixed/{prm3}")
    public Response getPerson(@PathParam("id") long id) {
        Person person = null;
        PersonDAO personDAO = new PersonDAO();

        System.out.println(servletRequest.getContextPath());

        try {
            person = personDAO.getById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (person == null) {
            return Response.status(Response.Status.NOT_FOUND)
                    .type("text/html")
                    .entity(String.format("<h2>Person not found </h2><br> id=%d", id))
                    .build();
        } else {
            return Response.ok(person).
                    expires(new Date(System.currentTimeMillis() + 3000))
                    .build();
        }
    }

    @POST
    @Path("/add")
    @Produces({"application/xml", "application/json"})
    @Consumes({"application/xml", "application/json", "application/x-www-form-urlencoded"})
    // можем получать данные как из формы, так и программно сконструированные
    public Response addPerson(@FormParam("photoId") long photoId, @FormParam("personName") String personName) {
        Person person = null;
        PersonDAO personDAO = new PersonDAO();
        long id = -1;

        try {
            if (personName == null || personName.isEmpty()) {
                throw new IllegalArgumentException();
            }
            person = new Person(personName);
            id = personDAO.addPersonOnPhoto(photoId, person);   //здесь id в таблице person_photo

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (person == null || id <= 0) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type("text/html")
                    .entity(String.format("<h2>Person not added </h2><br>"))
                    .build();
        } else {

            //MS:TODO таким образом нормально реализовать отсутствие зависимости в ссылках от контекста?
            // CK: Да, можно использовать @Context для полей класса вместо параметров метода
            String contextPath = servletRequest.getContextPath();
            return Response.created(URI.create("person/" + person.getId())).type("text/html").
            entity(String.format("<h2>Person (" + personName + ") added on photo id = " + photoId + "</h2><br>" +
                            "<a href=' " + contextPath + "/resources/rs/photo/get/" + photoId + "'> back to photo</a>"
            )).build();    //создает URI на базе текущего конкекста

            //MS:TODO корректно ли реализовано информирование о добавление person и переброска опять на страницу фото?
            // CK : да, верно, нужно отослать статус CREATED, и вместе с ним можно старницу отправить с пояснениями
        }
    }

    @PUT
    @Produces({"application/xml", "application/json"})
    @Consumes({"application/xml", "application/json", "application/x-www-form-urlencoded"})
    // MS: а как сделать чтобы для update можно было передавать парметры как из формы так и из строки браузера?
    // CK : Из алресной строки можно сделать только GET-запрос
    // все остальные можно создать - только с помощью javascript или с помошью формы
    public Response updatePerson(@FormParam("id") long id, @FormParam("name") String name) {
        boolean success = false;
        Person person = null;
        PersonDAO personDAO = new PersonDAO();

        try {
            if (id <= 0 || name == null || name.isEmpty()) {
                throw new IllegalArgumentException();
            }
            person = personDAO.getById(id);
            //MS: но будет false т.к. для Person апдейт в DAO пока не реализован, реализовано только для Photo
            success = personDAO.update(person);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!success) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .type("text/html")
                    .entity(String.format("<h2>Person not updated </h2><br>"))
                    .build();
        } else {
            return Response.ok(person).build();    //создает URI на базе текущего конкекста
        }
    }

    @DELETE
    @Produces({"application/xml", "application/json"})
    @Consumes("*/*")
    public Response deletePerson(@FormParam("photoId") long photoId, @FormParam("personId") long personId) {
        boolean success = false;
        PersonDAO personDAO = new PersonDAO();
        try {
            success = personDAO.deletePersonFromPhoto(photoId, personId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!success) {
            return Response.notModified().build();
        } else {
            return Response.ok().entity("Person (id="+personId +") deleted successfully from photo (" + photoId+")").build();
        }
    }

}

