package net.javajoy.jss.homework.w10.rest;

import net.javajoy.jss.homework.w10.dao.PhotoDAO;
import net.javajoy.jss.homework.w10.om.Photo;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.*;
import java.net.URI;
import java.util.Date;

/**
 * @author Sergey Mikhluk.
 */
@Path("/photo")
public class PhotoResource {
    @Context
    private HttpServletRequest servletRequest;

    public static final int BUF_SIZE = 1024;

    @GET
    @Path("/get/{id}")
    @Produces({"application/xml", "application/json", "text/html"})
    @Consumes("*/*")
    public Response getPhoto(@PathParam("id") long id) {

        System.out.println("PhotoResource.getPhoto()");
        Photo photo = null;
        PhotoDAO photoDAO = new PhotoDAO();

        try {
            photo = photoDAO.getById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (photo == null) {
            return Response.status(Response.Status.NOT_FOUND)
                    .type("text/html")
                    .entity(String.format("<h2>Photo not found </h2><br> id=%d", id))
                    .build();
        } else {
            return Response.ok(photo).
                    expires(new Date(System.currentTimeMillis() + 3000))
                    .build();
        }
    }

// MS: пример вывода изображения из файла на диске
//    @GET
//    @Path("/images")
//    @Produces("image/*")
//    public Response getImage() {
//        String image = "d:/13.jpg";
//        File f = new File(image);
//
//        if (!f.exists()) {
//            throw new WebApplicationException(404);
//        }
//
//        String mt = new MimetypesFileTypeMap().getContentType(f);
//        return Response.ok(f, mt).build();
//    }


    @GET
    @Path("/image/{id}")
    @Produces({"image/*"})
    public Response getUserImage(@PathParam("id") long id) {
        Photo photo = null;
        PhotoDAO photoDAO = new PhotoDAO();

        try {
            photo = photoDAO.getById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        byte[] image = photo.getImageData();

        //MS:TODO а как сделать чтобы и описание фото и фото выводилось на одной странице?
        // CK: также, как мы и в сервлетах картинку в страницу встраивали:
        // один ресурс выдает html со ссылкой <img href=....> на другой ресурс

        //один вариант реализовал, возможно есть другие лучше?
        // CK: Я именно так и имел в виду, получается что ресурс "фото" максимально независим от того, где и как он будет отображаться
        return Response.ok().entity(new StreamingOutput() {
            @Override
            public void write(OutputStream output)
                    throws IOException, WebApplicationException {
                output.write(image);
                output.flush();
            }
        }).build();
    }


    //MS:TODO раскомментировать и реализоывать CRUD



    @POST
    @Path("/add")
    @Produces({"application/xml", "application/json"})
    @Consumes({"application/xml", "application/json", "application/x-www-form-urlencoded"})

    // можем получать данные как из формы, так и программно сконструированные
    public Response addPhoto(@FormParam("updatePath") String updatePath, @FormParam("updateRegionName") String updateRegionName,
                             @FormParam("updateDate") String updateDate, @FormParam("updateViews") String updateViews) {
        //MS:TODO должны реализовывать логику добавления фото здесь? или можно использовать старый DBActionServlet?
        // проверял, он работает если вызвать из PhotoListHtmlProvider.java

        // CK: Твое приложение может содержать одноврменно и сервлеты и сервисы, и они будут работать в tomcat одновременно
        // НО(!) Если у тебя есть ресурс "фото" значит все crud-операции с ним должны быть реализованы через rest.
        // В результате в данном приложении все функциональность переходит в rest, и сервлеты становятся не нужны.

        Photo photo = null;
        PhotoDAO photoDAO = new PhotoDAO();
        long id = -1;

        try {
            if (updateRegionName == null || updateRegionName.isEmpty()) {
                throw new IllegalArgumentException();
            }

            photo = new Photo(String.valueOf(id), updatePath, updateDate, updateRegionName,
                            updateViews, getImageDataFromPart());


            id = photoDAO.add(photo);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (photo == null || id <= 0) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type("text/html")
                    .entity(String.format("<h2>Photo not added </h2><br>"))
                    .build();
        } else {
            String contextPath = servletRequest.getContextPath();
            return Response.created(URI.create("photo/get/" + id)).
                    type("text/html").entity(String.format("<h2>Photo added id =" + id + ", path = " + photo.getPath() + "</h2><br>" +
                    "<a href='" + contextPath + "/resources/rs/photos'> back to photo list</a>")).
                            build();    //создает URI на базе текущего конкекста

        }

    }


    private byte[] getImageDataFromPart() throws IOException {

        File file = new File("d:/13.jpg");
        //FileInputStream inputStream = new FileInputStream(file);
        InputStream inputStream  = null;

        inputStream = new BufferedInputStream(new FileInputStream(file));

        //ImageIO.read(inputStream);

        //Part filePart = request.getPart("file");
        //InputStream inputStream = filePart.getInputStream();
        byte[] buf = new byte[1024*4];
        ByteArrayOutputStream bos = new ByteArrayOutputStream(1024*1024);
        while (inputStream.read(buf) > 0) {
            bos.write(buf);
        }
        return bos.toByteArray();
    }


//    @PUT
//    @Path("/edit/{id}")
//    @Produces({"application/xml", "application/json"})
//    @Consumes({"application/xml", "application/json", "application/x-www-form-urlencoded"})
//    // MS:TODO а ак сделать чтобы можно было передавать парметри как из формы так и из строки браузера?
//    public Response updateBook(@FormParam("id") long id, @FormParam("name") String name) {
//        boolean success = false;
//        Person person = null;
//        PersonDAO personDAO = new PersonDAO();
//
//        try {
//            if (id <= 0 || name == null || name.isEmpty()) {
//                throw new IllegalArgumentException();
//            }
//            person = personDAO.getById(id);
//            //MS:TODO для Person апдейт в DAO пока не реализован, реализовано только для Photo
//            success = personDAO.update(person);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        if (!success) {
//            return Response.status(Response.Status.BAD_REQUEST)
//                    .type("text/html")
//                    .entity(String.format("<h2>Person not updated </h2><br>"))
//                    .build();
//        } else {
//            return Response.ok(person).build();    //создает URI на базе текущего конкекста
//        }
//    }


    //MS:TODO а как вызвать метод delete из формы PhotoListHtmlProvider?  для метода Post получается вызывать
    //а при попытке вызвать метод Delete выдает ошибку 405
    @DELETE
    @Path("/del")
    @Produces({"application/xml", "application/json"})
    @Consumes("*/*")
    public Response deletePhoto(@FormParam("id") long id) {
        boolean success = false;
        PhotoDAO photoDAO = new PhotoDAO();

//        try {
//            success = photoDAO.delete(id);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        if (!success) {
            return Response.notModified().build();
        } else {
            return Response.ok().entity("Photo (id=" + id + ") deleted successfully").build();
        }
    }


}

