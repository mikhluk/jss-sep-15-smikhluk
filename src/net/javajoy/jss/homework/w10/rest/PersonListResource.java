package net.javajoy.jss.homework.w10.rest;

import net.javajoy.jss.homework.w10.dao.PersonDAO;
import net.javajoy.jss.homework.w10.bean.Filter;
import net.javajoy.jss.homework.w10.om.Person;
import net.javajoy.jss.homework.w10.om.PersonList;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author Sergey Mikhluk.
 */

@Path("persons")
public class PersonListResource {

    @GET
    @Produces({"application/xml", "application/json", "text/html"})
    @Consumes("*/*")
    public Response getAllPersons() {
        return getPersonsHelper(null);
    }

    // CK: GET запрос удобно делать, если параметров фильтра мало (один-два)
    @GET
    @Path("/{photoId}")
    @Produces({"application/xml", "application/json", "text/html"})
    @Consumes("*/*")
    public Response getPersons(@PathParam("photoId") Long photoId) {
        return getPersonsHelper(photoId);
    }

    // CK: TODO Здесь можно предусмотреть произвольное количество параметров для фильтра в виде MultivaluedMap<String, String> formParams
    @POST  // форма передает данные обычно через POST
    @Produces({"application/xml", "application/json"})
    @Consumes("application/x-www-form-urlencoded")
    public Response getPersonsForm(@FormParam("photoId") Long photoId) {
        return getPersonsHelper(photoId);
    }

    protected Response getPersonsHelper(Long photoId) {
        Filter filter = new Filter();

        //MS: корректно ли реализован вывод с учетом фильтра?
        // CK: правильно, поскольку в результате все равно список людей, значит это относится к PersonListResource
        // если бы параметром был personId - это уже в PersonResource

        // CK: TODO поскольку семантика getAllPersons() и getPersonsForm() разная, то логичнее в них создавать готовый фильтр и сюда передавать
        if (photoId != null) {
            filter.setValue(Filter.FilterConstant.PHOTO_ID, String.valueOf(photoId));
        }

        List<Person> personList = null;
        PersonDAO personDAO = new PersonDAO();

        try {
            personList = personDAO.getAsList(filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (personList == null || personList.isEmpty()) {
            //Construct error respose
            return Response.status(Response.Status.BAD_REQUEST).entity("Empty List").build();

        } else {
            //Construct response with PersonList
            Response.ResponseBuilder rb = Response.ok(new PersonList(personList));
            Response response = rb.build();

            return response;
        }
    }
}
