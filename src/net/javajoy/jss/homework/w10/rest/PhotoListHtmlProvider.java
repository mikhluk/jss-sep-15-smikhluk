package net.javajoy.jss.homework.w10.rest;

import net.javajoy.jss.homework.w10.om.PhotoList;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * @author Sergey Mikhluk.
 */
@Provider
@Produces("text/html")
public class PhotoListHtmlProvider implements MessageBodyWriter<PhotoList> {
    @Override
    public boolean isWriteable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return true;
    }

    @Override
    public long getSize(PhotoList photoList, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return 0;
    }

    @Override
    public void writeTo(PhotoList photoList, Class<?> aClass, Type type, Annotation[] annotations,
                        MediaType mediaType, MultivaluedMap<String, Object> multivaluedMap,
                        OutputStream outputStream) throws IOException, WebApplicationException {



        PrintWriter writer = new PrintWriter(outputStream);
        writer.print("<html>");
        //MS:TODO нужно ли релазизовать форму для ввода фильтр на странице, как это было в jsp?
        // CK: не обязательно, это уже больше ответственность клиентской стороны
        // По хорошему rest-сервис должен только дать html-провайдеры для отображения ресурсов, а формы это уже не его ответственность
        writer.print("<h3> Photo list </h3>");

        WebUtils.printPhotoList(photoList.getPhotoList(), writer);

        //writer.print("<form action'/jss-sep-15-smikhluk/resources/rs/photo/del' method='delete' accept-charset='UTF-8'>");
//        writer.print("<form action='/jss-sep-15-smikhluk/resources/rs/photo/del' method='post'>");
//        writer.print("<input type='number' name='id'>");
//        writer.print("<input type='submit' value='Submit'>");
//        writer.print("<input type='reset' value='Cancel'>");
//        writer.print("</form>");


        writer.print("<h3> Add photo</h3>");
        //writer.print("<form action=\"/jss-sep-15-smikhluk/photoDBAction\" method=\"post\" accept-charset=\"UTF-8\" enctype=\"multipart/form-data\">");
        writer.print("<form action='" + WebUtils.CONTEXT_PATH + "/resources/rs/photo/add' method='post' accept-charset='UTF-8' >");
        writer.print("<table border=1 cellspacing=0 width=700px>");
        writer.print("<tr>");
        writer.print("<th>photo_id</th>");
        writer.print("<th>path</th>");
        writer.print("<th>region</th>");
        writer.print("<th>date</th>");
        writer.print("<th>views</th>");
        writer.print("<tr>");
        writer.print("<td></td>");
        writer.print("<td><input type=\"text\" name=\"updatePath\" value=\"d:\\_ms_2468\"></td>");
        writer.print("<td><input type=\"text\" name=\"updateRegionName\" value=\"odessa\"></td>");
        writer.print("<td><input type=\"text\" name=\"updateDate\" value=\"2016-01-01\"></td>");
        writer.print("<td><input type=\"text\" name=\"updateViews\"></td>");
        writer.print("</tr>");
        writer.print("</table>");
        writer.print("<p/>");
        writer.print("<input type=\"hidden\" name=\"action\" value=\"addPhoto\">");
        writer.print("<input type=\"hidden\" name=\"photo_id\" value=\"-1\">");
        //writer.print("<input type=\"file\" name=\"file\" accept=\"image/jpeg\"><br>");
        writer.print("<input type=\"submit\" value=\"Add photo\">");
        writer.print("<input type=\"reset\" value=\"Cancel\">");
        writer.print("</form>");
        writer.print("");


        writer.print("</html>");
        writer.close();   // обязательно закрыть поток
    }
}
