package net.javajoy.jss.homework.w10.rest;

import net.javajoy.jss.homework.w10.om.Person;
import net.javajoy.jss.homework.w10.om.Photo;

import java.io.PrintWriter;
import java.util.List;


/**
 * @author Sergey Mikhluk.
 */
public class WebUtils {

    public static final String CONTEXT_PATH = "/smikhluk";

    public static void printPersonList(List<Person> personList, PrintWriter writer, String head) {
        writer.println("<h3>" + head + "</h3>");

        writer.print("<table border=1 bordercolor='LightBlue' cellspacing=0 width=300>" +
                "<tr>" +
                "<th>id</th>" +
                "<th>name</th>" +
                "</tr>");

        for (Person person : personList) {
            writer.print("<tr>");
            writer.print("<td>" + person.getId() + " </td>");
            writer.print("<td>" + person.getName() + " </td>");
            writer.print("</td>");
            writer.print("</tr>");
        }
        writer.print("</table>");
    }

    private static void printTableHead(PrintWriter writer, boolean preview) {
        //MS: вывод таблицы с фото оставляем так или есть возможность сделать редирект на jsp?
        // CK: использовать jsp для формирования ответа можно, но это требует дополнительной настройки
        // так что пока так давай оставим

        writer.print("<table border=1 bordercolor='LimeGreen' cellspacing=0 width=800>" +
                "<tr>" +
                "<th>id</th>" +
                (preview ? "<th>preview</th>" : "") +
                "<th>name</th>" +
                "<th>date</th>" +
                "<th>views</th>" +
                "<th>region</th>" +
                "<th>delete</th>" +
//                "<th>update</th>" +

                "</tr>");
    }

    private static void printPhotoRow(Photo photo, PrintWriter writer, boolean preview) {
        writer.print("<tr>");
        writer.print("<td>" + photo.getId() + " </td>");

        if (preview) {
            writer.print("<td align='center'> <img src='" + CONTEXT_PATH + "/resources/rs/photo/image/" + photo.getId() + "' height=40>");
        }
        writer.print("<td> <a href='" + CONTEXT_PATH + "/resources/rs/photo/get/" + photo.getId() + "'>" + photo.getPath() + "</a></td>");
        writer.print("<td>" + photo.getDate() + " </td>");
        writer.print("<td>" + photo.getViews() + " </td>");
        writer.print("<td>" + photo.getRegion().getName() + " </td>");

        //MS:TODO доделывать возможность добавления, изменения фото по ссылкам или не надо?
        // CK : Давай хотя бы что-то одно сделаем

        // CK : Здесь мы должны обходиться без сервлетов, реализуя все CRUD-опрации через REST
        // Правда, сделать запрос DELETE так просто не получится, его можно только с помощью javascript создать
        // Поэтому пока это оставляем
        writer.print("<td> <a href='/jss-wsClient/photoDBAction?action=delete&photo_id=" + photo.getId() + "'> X</a></td>");
        //writer.print("<td> <a href='?action=update&photo_id=" + photo.getId() + "'> Update</a></td>");

        writer.print("</td>");
        writer.print("</tr>");
    }

    public static void printPhotoList(List<Photo> photoList, PrintWriter writer) {
        printTableHead(writer, true);

        for (Photo photo: photoList) {
            printPhotoRow(photo, writer, true);
        }

        writer.print("</table>");
    }


    public static void printPhoto(Photo photo, PrintWriter writer) {
        printTableHead(writer, false);
        printPhotoRow(photo, writer, false);

        writer.print("</table>");
        writer.print("<img src='" + CONTEXT_PATH + "/resources/rs/photo/image/" + photo.getId() + "'/>");


        //MS:TODO как указать относительный путь без контекста?
        // CK: относительный путь указать нельзя, но ты можешь не хардкодить имя контекста
        // Например, в сервис-методах ты можешь сделать параметр типа @Context HttpServletRequest,
        // а из него получить getContextPath
        // Сюда же url картинки передать можно как свойство Photo


    }


}
