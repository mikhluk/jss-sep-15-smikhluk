package net.javajoy.jss.homework.w10.dao;

import jdk.internal.org.objectweb.asm.Type;
import net.javajoy.jss.homework.w06.DatabaseConnection;
import net.javajoy.jss.homework.w10.bean.ConfigSingleton;
import net.javajoy.jss.homework.w10.bean.Filter;
import net.javajoy.jss.homework.w10.om.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author Sergey Mikhlyuk
 */
public class PersonDAO implements DAO<Person> {
    private Connection conn = null;

    public PersonDAO() {
        Properties conf = ConfigSingleton.getInstance().getConfig();
        conn = DatabaseConnection.getConnection(conf);
    }

    @Override
    public Person getById(long id) {
        String sql = "SELECT * FROM person WHERE id = ?";
        String sqlExecuted = "";

        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            stmt.setString(1, Long.toString(id));

            ResultSet rs = stmt.executeQuery();
            sqlExecuted = stmt.toString();
            System.out.println(sqlExecuted);

            if (rs.next()) {
                Person person = new Person(
                        rs.getLong("id"),
                        rs.getString("name"));
                return person;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Person> getAsList(Filter filter) {
        //System.out.println("PersonDAO.java getPersonAsList");

        if (filter.getValue("photo_id") == null || filter.getValue("photo_id") == "") {
            return getPersonsAsList(false, filter);
        } else {
            return getPersonsAsList(true, filter);
        }
    }

    private List<Person> getPersonsAsList(boolean photoFilterFlag, Filter filter) {
        int listCapacity = 0;
        String sql;

        if (photoFilterFlag) {
            sql = "SELECT COUNT(*) AS quantity FROM view_person_photo WHERE name LIKE ?  AND photo_id = ?";
        } else {
            sql = "SELECT COUNT(*) AS quantity FROM person WHERE name LIKE ?";
        }

        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            stmt.setString(1, "%" + filter.getValue(Filter.FilterConstant.PERSON_NAME) + "%");

            if (photoFilterFlag) {
                stmt.setString(2, filter.getValue(Filter.FilterConstant.PHOTO_ID));
            }

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                listCapacity = rs.getInt("quantity");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println(listCapacity); //debug
        List<Person> list = new ArrayList(listCapacity);

        if (photoFilterFlag) {
            sql = "SELECT person_id, name FROM view_person_photo WHERE name LIKE ? AND photo_id = ?";
        } else {
            sql = "SELECT * FROM person WHERE name LIKE ?";
        }

        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {

            stmt.setString(1, "%" + filter.getValue(Filter.FilterConstant.PERSON_NAME) + "%");
            if (photoFilterFlag) {
                stmt.setString(2, filter.getValue(Filter.FilterConstant.PHOTO_ID));
            }
            ResultSet rs = stmt.executeQuery();

            String personId = photoFilterFlag ? "person_id" : "id";
            while (rs.next()) {
                Person person = new Person(
                        rs.getLong(personId),
                        rs.getString("name"));
                list.add(person);
            }
            System.out.println("PersonDAO.java getPersonsAsList(): " + stmt.toString());

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public long add(Person person) {
        return 0;
    }

    public long addPersonOnPhoto(long photoID, Person person) {
        System.out.println("PersonPhotoDAO.java addPerson");  //debug
        Long id = -1L;

        String sql = "call insertPersonOnPhoto(?,?,?)";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setString(1, person.getName());
            stmt.setString(2, String.valueOf(photoID));
            stmt.registerOutParameter(3, Type.LONG);

            stmt.execute();
            id = stmt.getLong(3);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public boolean update(Person person) {
        return false;
    }

    public boolean deletePersonFromPhoto(long photoId, long personId) {
        boolean success = false;

        String sql = "call deletePerson(?,?,?)";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setLong(1, photoId);
            stmt.setLong(2, personId);
            stmt.registerOutParameter(3, Type.LONG);

            stmt.execute();
            success = stmt.getBoolean(3);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }

    @Override
    public boolean delete(Person person) {
        return delete(person.getId());
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

}
