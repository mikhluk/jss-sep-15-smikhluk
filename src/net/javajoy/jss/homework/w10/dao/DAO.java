package net.javajoy.jss.homework.w10.dao;

import java.util.List;

/**
 * @author Sergey Mikhluk.
 */
public interface DAO<T> {

    T getById(long id);

    List<T> getAsList(net.javajoy.jss.homework.w10.bean.Filter filter);

    long add(T element);

    boolean update(T element);

    boolean delete(long id);

    boolean delete(T element);
}
