package net.javajoy.jss.homework.w10.dao;

import jdk.internal.org.objectweb.asm.Type;
import net.javajoy.jss.homework.w06.DatabaseConnection;
import net.javajoy.jss.homework.w10.bean.ConfigSingleton;
import net.javajoy.jss.homework.w10.bean.Filter;
import net.javajoy.jss.homework.w10.om.Photo;
import net.javajoy.jss.homework.w10.om.Region;

import javax.imageio.ImageIO;
import javax.jws.WebService;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author Sergey Mikhluk.
 */

public class PhotoDAO implements DAO<Photo> {
    private Connection conn = null;

    public PhotoDAO() {
        Properties conf = ConfigSingleton.getInstance().getConfig();
        conn = DatabaseConnection.getConnection(conf);
        //System.out.println("PhotoDaoBean constructor"); //debug
    }

    @Override
    public Photo getById(long id) {  //MS:TODO почему идея подчеркивает?
        String sql = "SELECT * FROM view_photo_region WHERE photo_id = ?";

        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            stmt.setString(1, Long.toString(id));
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Region region = new Region(rs.getLong("region_id"), rs.getString("region_name"));
                Photo photo = new Photo(
                        rs.getLong("photo_id"),
                        rs.getString("path"),
                        rs.getDate("date"),
                        region,
                        rs.getInt("views"),
                        rs.getBinaryStream("image")
                );
                System.out.println(stmt.toString());
                return photo;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Photo> getAsList(Filter filter) {
        //System.out.println("PhotoDAOBean getAsList conn:"+  conn); //debug
        //System.out.println("person_id:"+  filter.getValue("person_id")); //debug
        if (filter.getValue("person_id") == null || filter.getValue("person_id").equals("")) {
            return getPhotosAsList(false, filter);
        } else {
            return getPhotosAsList(true, filter);
        }
    }

    public List<Photo> getPhotosAsList(boolean personFilterFlag, Filter filter) {
        int listCapacity = 0;
        String sql;

        if (personFilterFlag) {
            sql = "SELECT COUNT(*) AS quantity FROM view_person_photo WHERE path LIKE ? and region_name LIKE ? and person_id = ?";

        } else {
            sql = "SELECT COUNT(*) AS quantity FROM view_photo_region WHERE path LIKE ? and region_name LIKE ?";
        }

        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            stmt.setString(1, "%" + filter.getValue(Filter.FilterConstant.PHOTO_PATH) + "%");
            stmt.setString(2, "%" + filter.getValue(Filter.FilterConstant.PHOTO_REGION_NAME) + "%");

            if (personFilterFlag) {
                stmt.setString(3, filter.getValue("person_id"));
            }
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                listCapacity = rs.getInt("quantity");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        List<Photo> list = new ArrayList(listCapacity);
        System.out.println(listCapacity);

        if (personFilterFlag) {
            sql = "SELECT * FROM view_person_photo WHERE path LIKE ? and region_name LIKE ? and person_id = ? ";
        } else {
            sql = "SELECT * FROM view_photo_region WHERE path LIKE ? and region_name LIKE ?";
        }

        try (PreparedStatement stmt = conn.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {

            stmt.setString(1, "%" + filter.getValue(Filter.FilterConstant.PHOTO_PATH) + "%");
            stmt.setString(2, "%" + filter.getValue(Filter.FilterConstant.PHOTO_REGION_NAME) + "%");
            if (personFilterFlag) {
                stmt.setString(3, filter.getValue(Filter.FilterConstant.PERSON_ID));
            }
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Region region = new Region(rs.getLong("region_id"), rs.getString("region_name"));

                Photo photo = new Photo(
                        rs.getLong("photo_id"),
                        rs.getString("path"),
                        rs.getDate("date"),
                        region,
                        rs.getInt("views"),
                        rs.getBinaryStream("image")
                );
                list.add(photo);
            }
            System.out.println(stmt.toString());

        } catch (SQLException e) {
            e.printStackTrace();
        }

//        for(Photo photo:list) {    //debug
//            System.out.println(photo.getId() + " " + photo.getPath());
//        }
        return list;
    }

    @Override
    public long add(Photo photo) {  //MS:TODO идея подчеркивает
        long id = -1L;
        String sql = "call insertPhoto(?,?,?,?,?,?)";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.registerOutParameter(1, Type.LONG);
            stmt.setString(2, photo.getPath());
            stmt.setDate(3, new Date(photo.getDate().getTime()));
            stmt.setString(4, photo.getRegion().getName());
            stmt.setInt(5, photo.getViews());
            stmt.setBlob(6, photo.getDataInputStream());

            stmt.execute();
            id = stmt.getLong(1);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public boolean update(Photo photo) {  //MS:TODO идея подчеркивает
        boolean executeStatus = false;
        //System.out.println("PhotoDAO.java updatePhoto"); //debug

        String sql = "call updatePhoto(?,?,?,?,?,?,?)";

        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.registerOutParameter(1, Type.LONG);
            stmt.setLong(2, photo.getId());
            stmt.setString(3, photo.getPath());
            stmt.setDate(4, new Date(photo.getDate().getTime()));
            stmt.setString(5, photo.getRegion().getName());
            stmt.setInt(6, photo.getViews());
            stmt.setBlob(7, photo.getDataInputStream());

            stmt.execute();
            executeStatus = stmt.getBoolean(1);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return executeStatus;
    }

    @Override
    public boolean delete(long id) {
        boolean success = false;

        String sql = "call deletePhoto(?,?)";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setLong(1, id);
            stmt.registerOutParameter(2, Type.LONG);

            stmt.execute();
            success = stmt.getBoolean(2);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }

    @Override
    public boolean delete(Photo photo) {   //MS:TODO идея подчеркивает
        return delete(photo.getId());
    }

    public byte[] getPhotoData(long index) {
        Photo photo = getById(index);
        String photoPath = photo.getPath();

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            BufferedImage img = ImageIO.read(new File(photoPath));
            ImageIO.write(img, "jpg", bos);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return bos.toByteArray();
    }
}
