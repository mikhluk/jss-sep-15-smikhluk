package net.javajoy.jss.homework.w06;

import jdk.internal.org.objectweb.asm.Type;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;

/**
 * @author Sergey Mikhluk.
 */

@WebServlet(name = "dbAction", urlPatterns = {"/dbAction"})
public class DBActionServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        String actionStatus = "";
        Long id = new Long(request.getParameter("photo_id"));
        //System.out.println("DBActionServlet.java action =" + action);
        //System.out.println("DBActionServlet.java id =" + id);

        if (action == null) {
            actionStatus = "Wrong parameter action!";
            String refRedirect = String.format("/jss-sep-15-smikhluk/jss/w06/photoView.jsp?photo_id=%d&actionStatus=%s", id, actionStatus);
            response.sendRedirect(refRedirect);
            return;
        }

        Properties conf = new AppConfig().getConfig(getServletContext());
        Connection conn = DatabaseConnection.getConnection(conf);

        String refRedirect;
        if (action.equals("addPerson")){
            actionStatus = actAddPerson(request, actionStatus, id, conn);
            refRedirect = String.format("/jss-sep-15-smikhluk/jss/w06/personPhotoView.jsp?photo_id=%d&actionStatus=%s", id, actionStatus);

        }else {
            switch (action) {
                case "delete":
                    actionStatus = actDelete(actionStatus, id, conn);
                    break;
                case "update":
                    actionStatus = actUpdate(request, actionStatus, conn);
                    break;
                case "addPhoto":
                    id = actAddPhoto(request, id, conn);
                    if (id > 0) {
                        actionStatus = "added";
                    }
                    break;
            }
            refRedirect = String.format("/jss-sep-15-smikhluk/jss/w06/photoView.jsp?photo_id=%d&actionStatus=%s", id, actionStatus);
        }

        response.sendRedirect(refRedirect);
        //request.setAttribute("actionStatus", message);
        //getServletContext().getRequestDispatcher("/jss/w06/photoView.jsp").forward(request, response);
    }

    private String actUpdate(HttpServletRequest request, String actionStatus, Connection conn) {
        try (PreparedStatement stmt = conn.prepareStatement(
                "UPDATE photo SET path=?, views=? WHERE id=?",
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {

            stmt.setString(1, request.getParameter("updatePath"));
            stmt.setString(2, request.getParameter("updateViews"));
            stmt.setString(3, request.getParameter("photo_id"));

            int key = stmt.executeUpdate();
            if (key > 0) {
                actionStatus = "updated";
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actionStatus;
    }

    private String actDelete(String actionStatus, Long id, Connection conn) {
        boolean success;
        String sql = "call deletePhoto(?,?)";
        try (CallableStatement stmt = conn.prepareCall(sql)) {

            stmt.setLong(1, id);
            stmt.registerOutParameter(2, Type.LONG);
            stmt.execute();

            success = stmt.getBoolean(2);
            if (success) {
                actionStatus = "deleted";
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actionStatus;
    }

    private Long actAddPhoto(HttpServletRequest request, Long id, Connection conn) {
        //System.out.println("DBActionServlet.java case addPhoto");
        id = -1L;
        String sql = "call insertPhoto(?,?,?,?,?)";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setString(1, request.getParameter("updatePath"));

            java.util.Date date_utl = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("updateDate"));
            java.sql.Date dat_sql = new java.sql.Date(date_utl.getTime());
            stmt.setDate(2, dat_sql);
            stmt.setString(3, request.getParameter("updateRegionName"));

            int updateViews = request.getParameter("updateViews") == "" ? 0 : new Integer(request.getParameter("updateViews"));
            stmt.setInt(4, updateViews);
            stmt.registerOutParameter(5, Type.LONG);

            stmt.execute();
            id = stmt.getLong(5);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return id;
    }

    private String actAddPerson(HttpServletRequest request, String actionStatus, Long id, Connection conn) {
        //System.out.println("DBActionServlet.java case addPerson");
        id = -1L;
        String sql = "call insertPersonOnPhoto(?,?,?)";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setString(1, request.getParameter("updateName"));
            stmt.setString(2, request.getParameter("photo_id"));
            stmt.registerOutParameter(3, Type.LONG);

            stmt.execute();
            id = stmt.getLong(3);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (id > 0) {
            actionStatus = "added";
        }
        return actionStatus;
    }
}
