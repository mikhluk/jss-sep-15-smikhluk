package net.javajoy.jss.homework.w06;

import javax.servlet.ServletContext;
import java.util.Properties;

/**
 * @author Sergey Mikhluk.
 */

public class AppConfig {
    private static Properties config = null;

    public AppConfig() {
    }

    public Properties getConfig(ServletContext application) {
        if (config == null) {
            config = new Properties();

            config.setProperty("dbName", application.getInitParameter("home_photo_db_name"));
            config.setProperty("dbUser", application.getInitParameter("mysql_user"));
            config.setProperty("dbPass", application.getInitParameter("mysql_pass"));
        }
        return config;
    }
}


