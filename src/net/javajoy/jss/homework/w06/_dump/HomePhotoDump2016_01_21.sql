-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: homephoto
-- ------------------------------------------------------
-- Server version	5.6.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,'ivanov'),(2,'petrov'),(3,'sidorov'),(5,'vadik'),(6,'serg'),(7,'vadik1'),(8,'blablabla'),(9,'komarov'),(10,'111'),(11,'44444'),(12,'sergey'),(13,'andrey'),(14,''),(15,'blablabla1');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person_photo`
--

DROP TABLE IF EXISTS `person_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_photo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned DEFAULT NULL,
  `photo_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_person_idx` (`person_id`),
  KEY `fk_photo_idx` (`photo_id`),
  CONSTRAINT `fk_person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_photo` FOREIGN KEY (`photo_id`) REFERENCES `photo` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person_photo`
--

LOCK TABLES `person_photo` WRITE;
/*!40000 ALTER TABLE `person_photo` DISABLE KEYS */;
INSERT INTO `person_photo` VALUES (1,1,1),(2,1,2),(3,2,2),(4,3,3),(5,8,14),(6,1,15),(14,11,23),(45,1,54),(46,1,55),(58,8,67),(69,8,2),(71,8,78);
/*!40000 ALTER TABLE `person_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photo`
--

DROP TABLE IF EXISTS `photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(300) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `region_id` int(10) unsigned DEFAULT NULL,
  `views` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fkRegion_idx` (`region_id`),
  CONSTRAINT `fkRegion` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photo`
--

LOCK TABLES `photo` WRITE;
/*!40000 ALTER TABLE `photo` DISABLE KEYS */;
INSERT INTO `photo` VALUES (1,'d:\\myphoto','2015-10-10 00:00:00',1,333),(2,'c:\\downloads','2015-10-11 00:00:00',1,111),(3,'D:\\photo\\sidorov.jpg','2015-10-12 00:00:00',2,5),(9,'sidorov_petrov','2015-11-08 00:00:00',2,10),(13,'durik_foto','2015-10-11 00:00:00',3,13),(14,'D:\\path\\newname','1970-01-01 00:00:00',2,100),(15,'D:\\path\\newname','1970-01-01 00:00:00',2,100),(23,'1234','2015-10-15 00:00:00',6,0),(54,'d:\\temp','1970-01-01 00:00:00',1,234),(55,'d:\\temp11111','1970-01-01 00:00:00',1,112233),(67,'d:\\temp66','2016-01-01 00:00:00',6,66),(76,'d:\\temp','2016-01-01 00:00:00',6,0),(78,'d:\\temp','2016-01-01 00:00:00',6,0);
/*!40000 ALTER TABLE `photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` VALUES (1,'kiev'),(2,'kharkov'),(3,'lvov'),(4,'zhitomir'),(6,'odessa'),(7,'111'),(8,'lviv'),(9,'');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `view_all_photo`
--

DROP TABLE IF EXISTS `view_all_photo`;
/*!50001 DROP VIEW IF EXISTS `view_all_photo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_all_photo` AS SELECT 
 1 AS `id`,
 1 AS `path`,
 1 AS `date`,
 1 AS `region_id`,
 1 AS `views`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_person_photo`
--

DROP TABLE IF EXISTS `view_person_photo`;
/*!50001 DROP VIEW IF EXISTS `view_person_photo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_person_photo` AS SELECT 
 1 AS `pfid`,
 1 AS `photo_id`,
 1 AS `path`,
 1 AS `date`,
 1 AS `region_id`,
 1 AS `region_name`,
 1 AS `views`,
 1 AS `person_id`,
 1 AS `name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_photo_region`
--

DROP TABLE IF EXISTS `view_photo_region`;
/*!50001 DROP VIEW IF EXISTS `view_photo_region`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_photo_region` AS SELECT 
 1 AS `photo_id`,
 1 AS `path`,
 1 AS `date`,
 1 AS `region_id`,
 1 AS `views`,
 1 AS `region_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'homephoto'
--
/*!50003 DROP PROCEDURE IF EXISTS `deletePhoto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deletePhoto`(IN _id int(10), OUT success boolean)
BEGIN

	if 0 < (select count(*) from photo where photo.id = _id) then
    
		delete from person_photo where person_photo.photo_id = _id;
		delete from photo where photo.id = _id;
		set success = true;
    else
		set success = false;	
    end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertPersonOnPhoto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertPersonOnPhoto`(IN _name varchar(45), IN _photo_id int(10),
					OUT inserted_id int(10))
BEGIN

if 0 < (select count(*) from person where name = _name) then
		set @person_id = (select id from person where name = _name limit 1);
    else
		insert into person(name) values(_name);
        set @person_id = last_insert_id();
    
    end if;
    
    
    if 0 >= (select count(*) from person_photo where photo_id = _photo_id and  person_id = @person_id) then
    insert into person_photo(person_id, photo_id)
    values(@person_id, _photo_id);
    
    set inserted_id = last_insert_id();
    end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertPhoto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertPhoto`(IN _path varchar(300), IN _date datetime, IN _region_name varchar(45), 
				IN _views int(10), OUT photo_id int(10) )
BEGIN

	if 0< (select count(*) from region where region.name = _region_name) then
		set @region_id = (select id from region where region.name = _region_name limit 1);
    else
		insert into region(name) values(_region_name);
        set @region_id = last_insert_id();
	end if;
    
    insert into photo(path, date, region_id, views)
    values (_path, _date, @region_id, _views);
    set @photo_id = last_insert_id();
    
    set photo_id = @photo_id;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `view_all_photo`
--

/*!50001 DROP VIEW IF EXISTS `view_all_photo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_all_photo` AS select `photo`.`id` AS `id`,`photo`.`path` AS `path`,`photo`.`date` AS `date`,`photo`.`region_id` AS `region_id`,`photo`.`views` AS `views` from `photo` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_person_photo`
--

/*!50001 DROP VIEW IF EXISTS `view_person_photo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_person_photo` AS select `person_photo`.`id` AS `pfid`,`photo`.`id` AS `photo_id`,`photo`.`path` AS `path`,`photo`.`date` AS `date`,`photo`.`region_id` AS `region_id`,`region`.`name` AS `region_name`,`photo`.`views` AS `views`,`person`.`id` AS `person_id`,`person`.`name` AS `name` from ((`person` join (`photo` join `person_photo` on((`person_photo`.`photo_id` = `photo`.`id`))) on((`person_photo`.`person_id` = `person`.`id`))) join `region` on((`photo`.`region_id` = `region`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_photo_region`
--

/*!50001 DROP VIEW IF EXISTS `view_photo_region`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_photo_region` AS select `photo`.`id` AS `photo_id`,`photo`.`path` AS `path`,`photo`.`date` AS `date`,`photo`.`region_id` AS `region_id`,`photo`.`views` AS `views`,`region`.`name` AS `region_name` from (`photo` join `region` on((`photo`.`region_id` = `region`.`id`))) order by `photo`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-21 14:20:58
