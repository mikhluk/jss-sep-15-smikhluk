//package net.javajoy.jss.homework.w09.dao;
//
//import net.javajoy.jss.homework.w09.bean.Filter;
//
//import javax.jws.WebMethod;
//import javax.jws.WebResult;
//import javax.jws.WebService;
//import java.util.List;
//
///**
// * @author Sergey Mikhluk.
// */
//@WebService
//public interface DAO<T> {
//    //MS: если расскомментировать то при деплое выдается сделующая ошибка, не понятно, потому что для других методов в DAO такая ошибка не возникает
//    //org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'IPhotoDAO': Invocation of init method failed; nested exception is javax.xml.ws.WebServiceException: java.lang.IllegalArgumentException: An operation with name [{http://dao.w09.homework.jss.javajoy.net/}getById] already exists in this service
//
//    // CK : Я раскоментировал и сделал немного по-другому
//    // Ошибка была, потому что у тебя в IPhotoDAO по сути повторно объявлялся тот же метод, формально с такой же сигнатурой
//    // Теперь здесь все настройки WS, кроме новых методов, которые добавляются в IPhotoDAO
//
//    @WebResult(name = "item")
//    @WebMethod(operationName = "getById")
//    T getById(long id);
//
//    @WebResult(name = "itemsList")
//    @WebMethod(operationName = "getAsList")
//    List<T> getAsList(Filter filter);
//
//    @WebResult(name = "addedItemId")
//    @WebMethod(operationName = "add")
//    long add(T element);
//
//    @WebResult(name = "updateSuccess")
//    @WebMethod(operationName = "update")
//    boolean update(T element);
//
//    @WebResult(name = "deletedItemId")
//    @WebMethod(operationName = "deleteById")
//    boolean delete(long id);
//
//    @WebResult(name = "deleteSuccess")
//    @WebMethod(operationName = "delete")
//    boolean delete(T element);
//}
