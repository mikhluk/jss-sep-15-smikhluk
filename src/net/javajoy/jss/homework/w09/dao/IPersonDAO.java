package net.javajoy.jss.homework.w09.dao;

import net.javajoy.jss.homework.w09.bean.Filter;
import net.javajoy.jss.homework.w09.om.Person;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

/**
 * @author Sergey Mikhluk.
 */

@WebService
public interface IPersonDAO  {

    // CK: Эти методы уже есть в DAO<Person>, повторно объявлять их не нужно
    //MS: отказались от DAO<T> поскольку, на клиенте исчез класс Photo
    //т.к. DAO<T> является endpoint-интерфейсом, в wsdl теперь вместо типа photo xs:anytype
    //т.е. на клиенте просто придется работать со ссылками на Object.

    @WebMethod(operationName = "getById")
    Person getById(long id);

    @WebResult(name = "personsList")
    @WebMethod(operationName = "getAsList")
    List<Person> getAsList(Filter filter);

    @WebResult(name = "addedPersonId")
    @WebMethod(operationName = "add")
    long add(Person person);

    @WebResult(name = "updateSuccess")
    @WebMethod(operationName = "update")
    boolean update(Person person);

    @WebResult(name = "deleteSuccess")
    @WebMethod(operationName = "delete")
    boolean delete(Person person);

    @WebResult(name = "deletebByIdSuccess")
    @WebMethod(operationName = "deleteById")
    //MS:пришлось переименовать "delete", иначе ошибка при деплое из-за дубля, а можно ли не переименовывать?
    //теряется преимущество перегруженных методов

    // CK: с точки зрения java, методы и сейчас перегруженные
        // но "delete" и "deleteById" - это не имена методов. Это названия операций в слабо связзанном WS-интерфйесе,
        // а там уже каждая операция должна иметь уникально имя, и там уже мы полностью абстрагируемся от того,
        // какими java-методами каждая опреация реализуется (и java ли вообще используется)
        // Поэтому на уровне WSDL понятя перегрузки нет
    boolean delete(long id);

    // CK : Этих методов в DAO<Person> нет, поэтому описать их нужно здесь

    @WebResult(name = "addedPersonOnPhotoId")
    @WebMethod(operationName = "addPersonOnPhoto")
    long addPersonOnPhoto(long photoID, Person person);

    @WebResult(name = "deletedPersonFromPhotoSuccess")
    @WebMethod(operationName = "deletePersonFromPhoto")
    boolean deletePersonFromPhoto(long photoId, long personId);

}
