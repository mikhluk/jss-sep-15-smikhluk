package net.javajoy.jss.homework.w09.dao;

import net.javajoy.jss.homework.w09.bean.Filter;
import net.javajoy.jss.homework.w09.om.Photo;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

/**
 * @author Sergey Mikhluk.
 */

@WebService
public interface IPhotoDAO {

    // CK : Новых методов по сравнению с DAO<Photo> нет, ничего повторно не объявляем
    // Но интерфейс этот не лишний. В WSDL будет у PhotoDAO сервиса просто будет одна endpoint IPhotoDAO,
    // и за ней будет скрыты все детали о том, какие методы реально взяты из какого интерфейса
    //MS: отказались от DAO<T> поскольку, на клиенте исчез класс Photo
    //т.к. DAO<T> является endpoint-интерфейсом, в wsdl теперь вместо типа photo xs:anytype
    //т.е. на клиенте просто придется работать со ссылками на Object.


    @WebResult(name = "photo")
    @WebMethod(operationName = "getById")
    Photo getById(long id);

    @WebResult(name = "photosList")
    @WebMethod(operationName = "getAsList")
    List<Photo> getAsList(Filter filter);

    @WebResult(name = "addedPhotoId")
    @WebMethod(operationName = "add")
    long add(Photo photo);

    @WebResult(name = "updateSuccess")
    @WebMethod(operationName = "update")
    boolean update(Photo photo);

    @WebResult(name = "deleteSuccess")
    @WebMethod(operationName = "delete")
    boolean delete(Photo photo);

    @WebResult(name = "deleteByIdSuccess")
    @WebMethod(operationName = "deleteById")
    boolean delete(long id);

}
