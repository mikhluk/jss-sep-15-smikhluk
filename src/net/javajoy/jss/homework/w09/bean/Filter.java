package net.javajoy.jss.homework.w09.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;

/**
 * @author Sergey Mikhluk.
 */

@XmlRootElement(name = "filter")
public class Filter {

    public enum FilterConstant {
        PHOTO_ID("photo_id"),
        PHOTO_PATH("photo_path"),
        PHOTO_REGION_NAME("photo_region_name"),
        PERSON_ID("person_id"),
        PERSON_NAME("person_name");

        private String filterConstant;

        FilterConstant(String fConstant) {
            filterConstant = fConstant;
        }

        @Override
        public String toString() {
            return filterConstant;
        }
    }

    private HashMap<String, String> data; // = new HashMap<>(100);

    public Filter() {
        data = new HashMap<>(100);
    }

    @XmlElement(name = "filterMap", required = false)
    public HashMap<String, String> getFilterMap() {
        return data;
    }

    public void setFilterMap(HashMap<String, String> data) {
        this.data = data;
    }

//    @WebResult(name = "getValueByString")   //MS:TODO опять приходится делать другое название операции иначе ошибка
//    //org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'IFilter': Invocation of init method failed;
//    // nested exception is javax.xml.ws.WebServiceException: java.lang.IllegalArgumentException: An operation with name
//    // [{http://bean.w09.homework.jss.javajoy.net/}getValue] already exists in this service
//    @WebMethod(operationName = "getValueByString")

    public String getValue(String key) {
        String value = (data.get(key) == null) ? "" : data.get(key);
        return value;
    }

    public String getValue(FilterConstant key) {
        return getValue(key.toString());
    }

    //
//    public String getConstantValue(String key) {
//        return FilterConstant.valueOf(key).toString();
//    }
//
//    //@XmlAttribute(required = false)
//    public FilterConstant getConstant(String key) {
//        return FilterConstant.valueOf(key);
//    }
//
    public void setValueByKey(String key, String value) {
        data.put(key, value);
    }
//
//    @WebResult(name = "setValueByString")
//    @WebMethod(operationName = "setValueByString")
//    public void setValue(String key, String value) {
//        data.put(getConstantValue(key), value);
//    }
//
//    public void setValue(FilterConstant key, String value) {
//        data.put(key.toString(), value);
//    }

}



