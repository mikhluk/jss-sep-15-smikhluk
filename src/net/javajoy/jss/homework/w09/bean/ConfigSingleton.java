package net.javajoy.jss.homework.w09.bean;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

/**
 * @author Sergey Mikhluk.
 */
public class ConfigSingleton implements Serializable{
    //MS:TODO куда правильнее положить db.properties?
    // сейчас файл лежит в папке src
    // попробовал положить в папку resources, но похоже что classpath видится только от папки src и подняться на уровень выше не получилось

    // CK : Если бы сборка была с помощью maven, то нужно было бы класть в /resources
    // А пока пусть так будет

    public static final String CONFIG_PATH = "/db.properties";
    private Properties config = null;
    private static ConfigSingleton instance;

    private ConfigSingleton() {
        try {
            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(CONFIG_PATH);
            config = new Properties();
            config.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //System.out.println("dbName: " + config.getProperty("dbName"));  //debug
    }

    public static ConfigSingleton getInstance() {
        if (instance == null) {
            instance = new ConfigSingleton();
        }
        return instance;
    }

    public Properties getConfig() {
        return config;
    }
}