package net.javajoy.jss.homework.w05;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * @author Sergey Mikhluk.
 */
@WebServlet(name = "CalendarServlet")
public class CalendarServlet extends HttpServlet {
    public static final int MONTH = Calendar.NOVEMBER;
    public static final int YEAR = 2015;
    public ArrayList holidays;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // CK: The version of hit counter using session attribute
        // todo: try other storage places of counter : servlet class variable, context attribute, cookie
        HttpSession session = request.getSession(true);
        int hitCounter = 0;
        if (!session.isNew()) {
            hitCounter = (Integer) session.getAttribute("hitCounter");
        }
        session.setAttribute("hitCounter", Integer.valueOf(++hitCounter));
        System.out.format("Session ID = %s; \t Hit count =%d %n", session.getId(), hitCounter);

        PrintWriter out = receivePrintWriter(request, response);

        int intYear = getIntParameter(request, "userYear");
        int intMonth = getIntParameter(request, "userMonth");
        Calendar calendar = Calendar.getInstance();
        calendar.set(intYear, intMonth - 1, 1);

        printTop(intYear, intMonth, out);
        printTable(calendar, out);
        printBottom(out);

        out.close();
    }

    private void createHolidaysList() {
        holidays = new ArrayList();
        holidays.add("1.1");
        holidays.add("1.7");
        holidays.add("3.8");
        holidays.add("5.1");
        holidays.add("5.2");
        holidays.add("5.9");
        holidays.add("5.31");
        holidays.add("6.28");
        holidays.add("8.24");
        holidays.add("10.14");
    }

    private int getIntParameter(HttpServletRequest request, String userParameter) {
        int intParameter = 0;
        String strParameter = request.getParameter(userParameter);

        if (userParameter.equals("userYear")) {
            intParameter = (strParameter != null && strParameter.length() != 0) ? new Integer(strParameter) : YEAR;
        } else if (userParameter.equals("userMonth")) {
            intParameter = (strParameter != null && strParameter.length() != 0) ? new Integer(strParameter) : MONTH;
        }

        return intParameter;
    }

    private PrintWriter receivePrintWriter(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        return response.getWriter();
    }

    private void printTop(int intYear, int intMonth, PrintWriter out) {
        out.println("<html>" +
                "<head>" +
                "<title> Calendar </title>" +
                "</head>" +
                "<body bgcolor = '#eedddd'>" +
                "<h2> Год: " + intYear + "</h2> <p>" +
                "<h2> Месяц: " + intMonth + "</h2> <p>");
    }

    private void printTable(Calendar calendar, PrintWriter out) {
        out.println("<table width = 300 border = 1 style = border-collapse:collapse >");

        out.println("<tr align = center bgcolor = #ccc>");
        out.println("<td> пн </td> <td>вт</td> <td>ср</td> <td>чт</td> <td>пт</td> <td>сб</td> <td>вс</td>");
        out.println("</tr>");

        createHolidaysList();

        int firsDayOfWeek = (calendar.get(Calendar.DAY_OF_WEEK) + 5) % 7 + 1; //день недели первого числа месяца
        int count = 0;

        String printDate = "";
        String colorDate = "";
        for (int i = 1; i <= 6; i++) {
            out.println("<tr align = center>");
            for (int j = 1; j <= 7; j++) {
                count++;
                if ((count - firsDayOfWeek + 1 < 1)
                        || (count - firsDayOfWeek + 1 > calendar.getActualMaximum(Calendar.DAY_OF_MONTH))) {
                    printDate = "";
                } else {
                    printDate = Integer.toString(count - firsDayOfWeek + 1);
                }

                colorDate = (j >= 6 && j <= 7) ? "red" : "black";

                String param = (calendar.get(Calendar.MONTH) + 1) + "." + printDate;
                if (holidays.contains(param)) {
                    colorDate = "red";
                }

                out.println("<td> <font color = " + colorDate + ">" + printDate + "</font></td>");
            }
            out.println("</tr>");
        }
        out.println("</table>");
    }

    private void printBottom(PrintWriter out) {
        out.println("</body>" +
                "</html>");
    }
}
