package net.javajoy.jss.homework.w10_test.bookstore;

import net.javajoy.jss.homework.w10_test.bookstore.dao.BookDAO;
import net.javajoy.jss.homework.w10_test.bookstore.vo.Book;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Date;

/**
 * @author Sergey Mikhluk.
 */
//@Path("/book/{id}")   // лучше не так потому что будет метод add в котором id не указывается
@Path("/book")
public class BookResource {
    @GET
    @Path("/get/{id}")
    @Produces({"application/xml", "application/json","text/html"})
    //какого типа представление ресурса будет в результате работы метода (в каком виде выдадутся данные)
    @Consumes("*/*")
    //какого представление параметров в качестве входных он принимает, например может принимать параметры пришедшие из формы
    // *.* означает что любой контент тайп входящего запроса принимается
    public Response getBook(@PathParam("id") long id) {
        Book book = null;

        try {
            book = BookDAO.getInstance().get(id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (book == null) {
            return Response.status(Response.Status.NOT_FOUND)
                    .type("text/html")
                    .entity(String.format("<h2>Book not found </h2><br> id=%d", id))
                    .build();
        } else {
            return Response.ok(book).
                    expires(new Date(System.currentTimeMillis() + 3000))
                    .build();
        }
    }


    @POST
    @Path("/add")
    @Produces({"application/xml", "application/json"})
    @Consumes({"application/xml", "application/json", "application/x-www-form-urlencoded"})
    // можем получать данные как из формы, так и программно сконструированные
    public Response addBook(@FormParam("name") String name, @FormParam("author") String author) {
        Book book = null;
        try {
            if (name == null || name.isEmpty() || author == null || author.isEmpty()) {
                throw new IllegalArgumentException();
            }
            book = new Book(name, author);
            book = BookDAO.getInstance().add(book);

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (book == null || book.getId() <= 0) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .type("text/html")
                    .entity(String.format("<h2>Book not created </h2><br>"))
                    .build();
        } else {
            return Response.created(URI.create("book/" + book.getId())).build();    //создает URI на базе текущего конкекста

        }
    }

    @PUT
    @Path("/{id}/edit")
    @Produces({"application/xml", "application/json"})
    @Consumes({"application/xml", "application/json", "application/x-www-form-urlencoded"})
    // можем получать данные как из формы, так и программно сконструированные
    public Response updateBook(@FormParam("id") long id, @FormParam("name") String name, @FormParam("author") String author) {
        Book book = null;
        try {
            if (id <= 0 || name == null || name.isEmpty() || author == null || author.isEmpty()) {
                throw new IllegalArgumentException();
            }
            book = new Book(id, name, author);
            book = BookDAO.getInstance().update(book);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (book == null || book.getId() <= 0) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .type("text/html")
                    .entity(String.format("<h2>Book not updated </h2><br>"))
                    .build();
        } else {
            return Response.ok(book).build();    //создает URI на базе текущего конкекста
        }
    }

    @DELETE
    @Path("/{id}/del")
    @Produces({"application/xml", "application/json"})
    @Consumes("*/*")  // не важно в какой форме получаем параметры т.к. кроме id нам ничего не нужно
    public Response deleteBook(@PathParam("id") long id) {
        boolean success = false;
        try {
            success = BookDAO.getInstance().remove(id);
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (!success) {
            return Response.notModified().build();
        } else {
            return Response.ok().entity("Deleted successfully").build();
        }
    }


}



