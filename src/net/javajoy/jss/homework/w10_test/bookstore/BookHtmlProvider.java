package net.javajoy.jss.homework.w10_test.bookstore;

import net.javajoy.jss.homework.w10_test.bookstore.vo.Book;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * @author Sergey Mikhluk.
 */
@Provider
@Produces("text/html")
public class BookHtmlProvider implements MessageBodyWriter<Book> {
    @Override
    public boolean isWriteable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return true;   // внимательно если false будет ошибка
    }

    @Override
    public long getSize(Book book, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return 0;
    }

    @Override
    public void writeTo(Book book, Class<?> aClass, Type type, Annotation[] annotations,
                        MediaType mediaType, MultivaluedMap<String, Object> multivaluedMap,
                        OutputStream outputStream) throws IOException, WebApplicationException {

        PrintWriter writer = new PrintWriter(outputStream);
        writer.println("<html><h2> Book </h2> <br>" +
                "Name = " + book.getName() +
                "<br> Author = " + book.getAuthor() + "</html>");
        writer.close();   // обязательно закрыть поток
    }
}
