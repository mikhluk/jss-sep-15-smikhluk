package net.javajoy.jss.homework.w10_test.bookstore.vo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Sergey Mikhluk.
 */
//JAXB:  <book id="10" name="asdf" author="gdfsad"/> преобразование в тег
@XmlRootElement(name = "book")   // один объект Book может являться корневым элементом для некой xml схемы, т.е. может встраиваться в запромы и ответы веб сервисов
public class Book implements Cloneable{
    private long id = -1;
    private String name = null;
    private String author = null;

    public Book() {
    }

    public Book(long id, String name, String author) {
        this.id = id;
        this.name = name;
        this.author = author;
    }

    public Book(String name, String author) {
        this.id = -1;
        this.name = name;
        this.author = author;
    }


    @XmlAttribute
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @XmlAttribute
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlAttribute
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public Object clone() {
        return new Book(id, name, author);
    }

    @Override
    public String toString() {
        return "book{ name = " + getName() + ", author = " + getAuthor() + "}";
    }
}
