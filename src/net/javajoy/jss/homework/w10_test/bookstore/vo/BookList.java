package net.javajoy.jss.homework.w10_test.bookstore.vo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Mikhluk.
 */
//JAXB: <bookList>
    // <book id = "111" .../>
    // <book id = "111" .../>
    // <book id = "111" .../>
// </bookList>
@XmlRootElement(name = "bookList")
public class BookList {
    private List<Book> bookList;

    public BookList() {
    }

    public BookList(List<Book> bookList) {
        this.bookList = bookList;
    }

    @XmlElement(name="book")
    public List<Book> getBookList() {
        if (bookList == null) {
            bookList = new ArrayList<Book>(10);
        }
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }
}
